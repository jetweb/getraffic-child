<?php
/**
 * Template Name: 404
 *  *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-lg-12 container-404">
                <img class="image-404" src="<?php echo img('404-bg.png'); ?>">
                <div class="text-404">
                    <p class="oops-404">!oops</p>
                    <p class="sub-404">העמוד שחיפשת כנראה הלך לאיבוד במחסן...</p>
                    <p class="sub-404">אנחנו נמשיך לחפש, אבל בינתיים אולי תוכלי להיעזר באחד העמודים הבאים:</p>
                    <div><a href="/" class="button-404">עמוד הבית</a></div>
                    <div><a href="<?php echo site_url('/questions/'); ?>" class="button-404">שאלות נפוצות</a></div>
                    <div><a href="<?php echo site_url('/יצירת-קשר/'); ?>" class="button-404">דברי איתנו</a></div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>


<style>
    .container-404{
        display: flex;
        justify-content: center;
        margin: 50px 0;
    }
    .text-404{
        margin: 30px -25px 0 20px;    
        text-align: center;    
    }
    .oops-404{
        font-size: 47px;
        letter-spacing: 3px;
        line-height: 56px;
    }
    .image-404{
        width: 37%;
    }
    .sub-404{
        font-size: 20px;
        margin-top: -10px;
    }
    .button-404{
    /*   padding: 11px 23px; */
    /* font-size: 13px; */
    /* font-weight: bold; */
    /* border: 1px solid #5c5c5c; */
    /* background: #f6f0ed; */
    text-decoration: underline;
    line-height: 32px;
    }
    
    
    
    @media screen and (max-width: 768px) {
        .image-404{
        display: none;
    }
        .text-404{
        margin: 0;  
    }
    }
</style>