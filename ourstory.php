<?php
/**
 * Template Name: Our Story
 *  *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>
<div class="container ourstory">
    <div class="content">
        <div class="row">
            <div class="col-lg-6 col-sm-12 ourstory-right">
                <img class="right-top-image" src="<?php echo gt_get_field('image_our_story')['url']; ?>">
                <div class="ourstory-content">    
                    <?php
                    if (have_posts()) {
                        while (have_posts()) : the_post();
                            ?>                
                            <?php the_content(); ?>
                        <?php
                        endwhile;
                    }
                    ?>
                </div>
            </div>
            <div class="col-lg-6 ourstory-left">
                <img class="left-top-image" src="<?php echo gt_get_field('left_top_image')['url']; ?>" />                
                <img class="left-bottom-image" src="<?php echo gt_get_field('left_bottom_image')['url']; ?>">
                <img class="left-gold-frame" src="<?php echo img('ourstory-gold-frame.png'); ?>">                
            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>