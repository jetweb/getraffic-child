<?php
/**
 * Template Name: Archive
 * The template for displaying archive pages.
 *
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package storefront
 */

get_header(); ?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php 
            $jobsargs = array('post_type' => 'jobs');
            $jobsQuery = new WP_Query($jobsargs);
            $jobsCat_args = array(
                'hide_empty' => 0,
                'style'      => 'none',
                'taxonomy'   => 'jobs_category' );             
            $categories = get_categories( $jobsCat_args ); 
        ?> 
        <div class="container">
            <div class="row">                    
                <div class="jobs">
                    <div class="jobs-left">
                    <?php echo do_shortcode('[contact-form-7 id="75392" title="Jobs"]') ?>
                    </div>
                    <div class="jobs-right">
                        <p class="jobs_categories_title">המשרות השוות שלנו</p>
                        <div class="jobs_categories">
                            <?php if( $categories ){
                                foreach( $categories as $key => $cat ){ 
                                    $cat_id = str_replace(' ','_', $cat->name); ?>
                                    <div class="category" id="<?php echo $cat_id; ?>"><?php echo $cat->name; ?></div>
                            <?php }
                            } ?> 
                        </div>

                        <?php if ($jobsQuery->have_posts()) : 
                            while ($jobsQuery->have_posts()) : 
                                $jobsQuery->the_post();
                                $job_cat = get_the_terms( $post->ID, 'jobs_category')[0] -> name;
                                $job_data_target = str_replace(' ','_', $job_cat);
                        ?>
                                <div class="job" data-target="<?= $job_data_target ?>">
                                    <div class="job-title"><?php echo $job_cat; ?> | מס. משרה <?php echo gt_get_field('job_number', $post->ID); ?></div>
                                    <div class="job-content"><?php echo the_content(); ?></div>
                                </div>  
                                <?php 
                            endwhile;  
                            wp_reset_postdata(); 
                        endif; ?>                             
                    </div>
                </div>
                <div class="jobs-thankyou">
                    <div class="jobs-thankyou-top">
                        <p>תודה שפנית אלינו!<br> נחזור אלייך בהקדם</p>
                    </div>
                    <div class="jobs-thankyou-bottom"></div>
                </div>
            </div>
        </div>
    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer(); ?>

<script>
    jQuery(document).ready(function(){
        let $ = jQuery;        
        $('.job').hide();        
        $('.category').click(function(){        
            var selectedTopic = $(this).attr('id');           
            
            $('.category.selected').removeClass('selected');
            $(this).addClass('selected');
            $('.job').hide();
            $('.job[data-target=' + selectedTopic + ']').show();        
        });
    });
    document.addEventListener('wpcf7mailsent', function (event) {
        if ('450' == event.detail.contactFormId) {
            $('.jobs').hide();        
            $('.jobs-thankyou').show();        
        }
    }, false);
</script> 

