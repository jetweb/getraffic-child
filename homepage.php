<?php
/**
 * Template Name: עמוד הבית
 *
 * The home page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>

<?php get_template_part('templates/homepage/new-flats'); ?>
<?php get_template_part('templates/homepage/banners'); ?>
<?php get_template_part('templates/homepage/best-sellers'); ?>
<?php get_template_part('templates/homepage/pink-banners'); ?>
<?php get_template_part('templates/homepage/our-story'); ?>
<?php if(layer_is_office_ip()) get_template_part('templates/homepage/instagram-feed'); ?>


<?php get_footer(); ?>
