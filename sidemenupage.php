<?php
/**
 * Template Name: Side Menu Page
 *
 * Side Menu Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>
<div class="sidemenu">
<div class="sidemenu-menu">
    <?php wp_nav_menu( [	'menu'=> 'side-menu'] );?>
</div>
<div class="container sidemenu-content" id="primary">
    <div class="content">
        <?php
        if (have_posts()) {
            while (have_posts()) : the_post();
                ?>
                
                <?php the_content(); ?>
            <?php
            endwhile;
        }
        ?>
    </div>
</div>
</div>
<?php get_footer(); ?>
