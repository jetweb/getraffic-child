<?php
require_once 'includes/BUnique_Plugin_Base.php';

global $bunique_blugin_base;
$bunique_blugin_base = new Bunique_Plugin_Base();

// Enqueue all scripts in footer
BUnique_Scripts_To_Footer::getInstance();


add_action('after_setup_theme', 'bunique_remove_admin_bar');
function bunique_remove_admin_bar() {
    if ( !is_admin() ) {
        //show_admin_bar(false);
    }
}

// Remove wp emoji lib
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

function bunique_enqueue_styles_in_footer() {

}

add_action( 'get_footer', 'bunique_enqueue_styles_in_footer' );

add_filter( 'script_loader_tag', 'bunique_add_defer_to_scripts', 15, 2 );
function bunique_add_defer_to_scripts( $tag, $handle ) {
    if( !is_admin() ) {
        $load_defer = array(
            /*'prettyPhoto',
            'jquery-selectBox',
            'jquery-yith-wcwl',
            'contact-form-7',
            'flot',
            'flot-pie',
            'wc_sa-frontend',
            'jquery-blockui',
            'wc-add-to-cart',
            'js-cookie',
            'woocommerce',
            'wc-cart-fragments',
            'wc_additional_variation_images_script',
            'prettyPhoto',
            'mailchimp-woocommerce',
            'underscore',
            'wp-util',
            'wc-add-to-cart-variation',
            'chunk-vendors',
            'scripts',
            'image_zoooom',
            'image_zoooom-init',
            'woo-variation-swatches',
            'woo-variation-swatches-pro',
            'berocket_lmp_js',
            'gtm-getraffic',*/
        );
        if( in_array($handle, $load_defer) ) {
            return str_replace( ' src', " id='".$handle."' defer src", $tag );
        }

        return str_replace( ' src', " id='".$handle."' src", $tag );
    }
    return $tag;
}


add_action( 'wp_enqueue_scripts',         'bunique_deregister_styles', 9999 );
add_action( 'wp_head',                    'bunique_deregister_styles', 9999 );
function bunique_deregister_styles() {
    $deregister_styles = array(
        'wp-block-library',
        'wp-block-library-rtl',
        //'admin-bar',
        'yoast-seo-adminbar',
        'wordfenceAJAXcss',
        'yith-wcwl-font-awesome',
       // 'font-awesome',
        'googleFontsOpenSans', //@see wp-content\plugins\yith-woocommerce-badge-management-premium\class.yith-wcbm-frontend.php
        'animate.css', //@see wp-content\plugins\everest-comment-rating-lite\everest-comment-rating-lite.php
        'wc_sa-font-icons'
    );
    if( ! is_admin() && count( $deregister_styles )   ) {
        foreach ( $deregister_styles as $handle ) {
            wp_dequeue_style($handle);
            wp_deregister_style($handle);
        }
    }
}

add_action( 'wp_enqueue_scripts',         'bunique_deregister_scripts', 9999 );
add_action( 'wp_head',                    'bunique_deregister_scripts', 9999 );
function bunique_deregister_scripts() {
    $deregister_scripts = array(
        //'admin-bar',
        'wp-embed',
        'wordfenceAJAXjs'
    );
    if( ! is_admin() && count( $deregister_scripts ) ) {
        foreach ( $deregister_scripts as $handle ) {
            wp_dequeue_script($handle);
            wp_deregister_script($handle);
        }
    }
}

add_image_size('product-thumb', 500, 500, true);
set_post_thumbnail_size(500, 500, true);


function jobs_create_post_type()
{
    $labels = array(
        'name' => __('Jobs'),
        'singular_name' => __('Job'),
        'add_new' => __('New Job'),
        'add_new_item' => __('Add New Job'),
        'edit_item' => __('Edit Job'),
        'new_item' => __('New Job'),
        'view_item' => __('View Job'),
        'search_items' => __('Search Jobs'),
        'not_found' => __('No Jobs Found'),
        'not_found_in_trash' => __('No Jobs found in Trash'),
    );
    $args = array(
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'hierarchical' => false,
        'menu_position' => 10,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'custom-fields',
            'thumbnail'
        ),
    );
    register_post_type('jobs', $args);
}

add_action('init', 'jobs_create_post_type');

function jobs_register_taxonomy()
{
    register_taxonomy('jobs_category', 'jobs',
        array(
            'labels' => array(
                'name' => 'Job Categories',
                'singular_name' => 'Job Category',
                'search_items' => 'Search Job Categories',
                'all_items' => 'All Job Categories',
                'edit_item' => 'Edit Job Categories',
                'update_item' => 'Update Job Category',
                'add_new_item' => 'Add New Job Category',
                'new_item_name' => 'New Job Category Name',
                'menu_name' => 'Jobs Categories',
            ),
            'hierarchical' => true,
            'sort' => true,
            'args' => array('orderby' => 'term_order'),
            'show_admin_column' => true
        )
    );
}

add_action('init', 'jobs_register_taxonomy');

function add_jobs_list_to_contact_form($job, $unused)
{
    if ($job['name'] != 'job-list') {
        return $job;
    }
    $args = array('post_type' => 'jobs');
    $posts = get_posts($args);
    foreach ($posts as $post) {
        $job_number = gt_get_field('job_number', $post->ID);
        $job_cat = get_the_terms($post->ID, 'jobs_category')[0]->name;
        $job['raw_values'][] = $job_cat . ' | ' . $job_number;
        $job['values'][] = $job_cat . ' | ' . $job_number;
        $job['labels'][] = $job_cat . ' | ' . $job_number;
    }
    return $job;
}

add_filter('wpcf7_form_tag', 'add_jobs_list_to_contact_form', 10, 2);

function my_wpcf7_validate_text($result, $tag)
{

    $type = $tag['type'];
    $name = $tag['name'];
    $value = $_POST[$name];

    if (strpos($name, 'name') !== false || strpos($name, 'city') !== false) {
        $regex = '/^[\p{L} .-]+$/u';
        $Valid = preg_match($regex, $value, $matches);
        if ($Valid > 0) {
        } else {
            $result->invalidate($tag, wpcf7_get_message('invalid_name'));
        }
    }
    return $result;
}

add_filter('wpcf7_validate_text*', 'my_wpcf7_validate_text', 10, 2);

add_filter('wpcf7_messages', 'mywpcf7_text_messages');
function mywpcf7_text_messages($messages)
{
    return array_merge($messages, array(
        'invalid_name' => array(
            'description' => __("נא להזין אותיות בלבד.", 'contact-form-7'),
            'default' => __('נא להזין אותיות בלבד.', 'contact-form-7')
        )
    ));
}

function bu_email_customer_details_fields($fields, $sent_to_admin, $order)
{
    if ($sent_to_admin && isset($fields['customer_note'])) {
        $fields['customer_note']['value'] = "<b style='font-size: 18px;'>" . $fields['customer_note']['value'] . "</b>";
    }
    return $fields;
}

add_filter('woocommerce_email_customer_details_fields', 'bu_email_customer_details_fields', 10, 3);


add_filter('comments_open', function () {
    return true;
});

//add_filter('woocommerce_package_rates', 'wf_sort_shipping_methods', 10, 2);

function wf_sort_shipping_methods($available_shipping_methods, $package)
{
    // Arrange shipping methods as per your requirement
    $sort_order = array(
        'local_pickup' => array(),
        'wf_shipping_ups' => array(),
        'wf_shipping_usps' => array(),
        'free_shipping' => array(),
        'legacy_flat_rate' => array(),
    );

    // unsetting all methods that needs to be sorted
    foreach ($available_shipping_methods as $carrier_id => $carrier) {
        $carrier_name = current(explode(":", $carrier_id));
        if (array_key_exists($carrier_name, $sort_order)) {
            $sort_order[$carrier_name][$carrier_id] = $available_shipping_methods[$carrier_id];
            unset($available_shipping_methods[$carrier_id]);
        }
    }

    // adding methods again according to sort order array
    foreach ($sort_order as $carriers) {
        $available_shipping_methods = array_merge($available_shipping_methods, $carriers);
    }
    return $available_shipping_methods;
}

add_action('wp_loaded', function () {
    global $GT;
    $GT->setCustomCheckoutFields([
        'billing' => [
            'billing_email' => [
                'placeholder' => 'כתובת מייל *',
                'required' => true,
                'custom_attributes' => ['empty-message' => 'נצטרך את המייל שלך כדי לשלוח לך מידע על ההזמנה'],
                'class' => ['form-row-first', 'validate-email'],
            ],
            'billing_checkbox' => [
                'type' => 'checkbox',
                'custom_attributes' => ['checked' => 'checked'],
                'class' => ['form-row-first'],
                'label' => '<span>מאשרת קבלת עדכונים במייל וב-SMS</span>',
                'clear' => true,
            ],
            'billing_first_name' => [
                'placeholder' => 'שם פרטי *',
                'required' => true,
                'custom_attributes' => [
                    'empty-message' => 'נצטרך את השם שלך כדי לדעת למי לשלוח את ההזמנה',
                    'invalid-numbers-message' => 'אפשר להשתמש פה רק באותיות בעברית או באנגלית'
                ],
                'class' => ['form-row-last', 'validate-no-numbers'],
                'clear' => true,
            ],
            'billing_last_name' => [
                'placeholder' => 'שם משפחה *',
                'required' => true,
                'custom_attributes' => [
                    'empty-message' => 'נצטרך את השם משפחה שלך כדי לדעת למי לשלוח את ההזמנה',
                    'invalid-numbers-message' => 'אפשר להשתמש פה רק באותיות בעברית או באנגלית'],
                'class' => ['form-row-last', 'validate-no-numbers'],
                'clear' => true,
            ],
            'billing_phone' => [
                'placeholder' => 'טלפון נייד *',
                'required' => true,
                'custom_attributes' => [
                    'empty-message' => 'נצטרך את הטלפון שלך כדי לעדכן אותך לגבי ההזמנה',
                    'invalid-phone-message' => 'יש להזין 10 ספרות ללא מקף'
                ],
                'class' => ['form-row-first', 'validate-cell-phone'],
                'clear' => true,
            ],
            'account_password' => [
                'type' => 'password',
                'placeholder' => 'סיסמה (הזיני סיסמה אם ברצונך לפתוח חשבון)',
                'required' => false,
                'custom_attributes' => ['empty-message' => 'יש ליצור סיסמה כדי שתוכלי לעקוב אחרי ההזמנה',
                    'min-length' => '6',
                    'invalid-length-message' => 'נא להזין סיסמא בת 6 תווים לפחות',
                ],
                'class' => ['form-row-first', 'validate-length'],
                'clear' => true,
            ],
            'billing_country' => [ // This field is mandatory
                'type' => 'country',
                'required' => true,
                'label' => 'ארץ',
                'class' => ['form-row-last, hidden'],
            ],
        ],
        'order' => [
            'order_comments' => [
                'type' => 'textarea',
                'class' => [],
                'placeholder' => 'הערות להזמנה/הערות לשליח'
            ]
        ],
        'shipping' => [
            'billing_street' => [
                'placeholder' => 'רחוב',
                'required' => false,
                'class' => ['form-row-first'],
                'clear' => true,
            ],
            'billing_house' => [
                'placeholder' => 'מספר בית',
                'required' => false,
                'class' => ['form-row-last'],
                'clear' => true
            ],
            'billing_appartment' => [
                'placeholder' => 'דירה',
                'prefix' => 'דירה ',
                'required' => false,
                'class' => ['form-row-first'],
                'clear' => true
            ],
            'billing_floor' => [
                'placeholder' => 'קומה',
                'required' => false,
                'prefix' => 'קומה ',
                'class' => ['form-row-last'],
                'clear' => true
            ],
            'billing_city' => [
                'placeholder' => 'עיר *',
                'required' => true,
                'class' => ['form-row-first'],
                'custom_attributes' => ['empty-message' => 'נצטרך את שם העיר שלך בשביל לדעת לאן לשלוח את ההזמנה'],
                'clear' => true,
            ],
        ],
        'account' => []
    ]);

    $GT->setCustomAddressFields([
        'first_name' => [
            'placeholder' => 'שם פרטי',
            'label' => 'שם פרטי',
            'required' => true,
            'custom_attributes' => [
                'invalid-numbers-message' => 'אפשר להשתמש פה רק באותיות',
                'empty-message' => 'נצטרך את השם הפרטי שלך בשביל פרטי הקבלה'
            ],
            'class' => ['form-row-first', 'validate-no-numbers'],
            'clear' => true,
        ],
        'last_name' => [
            'placeholder' => 'שם משפחה',
            'label' => 'שם משפחה',
            'required' => true,
            'custom_attributes' => [
                'invalid-numbers-message' => 'אפשר להשתמש פה רק באותיות',
                'empty-message' => 'נצטרך את שם המשפחה שלך בשביל פרטי הקבלה'
            ],
            'class' => ['form-row-last', 'validate-no-numbers'],
            'clear' => true,
        ],
        'street' => [
            'placeholder' => 'רחוב',
            'label' => 'רחוב',
            'required' => false,
            'class' => ['form-row-first', 'validate-no-numbers'],
            'custom_attributes' => [
                'invalid-numbers-message' => 'אפשר להשתמש פה רק באותיות',
            ],
            'clear' => true,
        ],
        'house' => [
            'placeholder' => 'מספר בית',
            'label' => 'מספר בית',
            'required' => false,
            'class' => ['form-row-last', 'validate-numeric'],
            'clear' => true
        ],
        'appartment' => [
            'placeholder' => 'דירה',
            'label' => 'דירה',
            'required' => false,
            'class' => ['form-row-first', 'validate-numeric'],
            'clear' => true
        ],
        'floor' => [
            'placeholder' => 'קומה',
            'label' => 'קומה',
            'required' => false,
            'class' => ['form-row-last', 'validate-numeric'],
            'clear' => true
        ],
        'city' => [
            'placeholder' => 'עיר',
            'label' => 'עיר',
            'required' => true,
            'class' => ['form-row-first'],
            'clear' => true,
            'custom_attributes' => [
                'empty-message' => 'נצטרך את שם העיר שלך בשביל פרטי הקבלה (והמשלוח, אם הכתובות זהות)'
            ],
        ],
        'phone' => [
            'placeholder' => 'טלפון',
            'label' => 'טלפון',
            'required' => true,
            'class' => ['form-row-last', 'validate-cell-phone'],
            'clear' => true,
            'validate' => ['phone'],
            'custom_attributes' => [
                'invalid-phone-message' => 'מספר הטלפון שהוזן לא תקין',
                'empty-message' => 'נצטרך את הטלפון שלך כדי לעדכן אותך לגבי ההזמנה'
            ],
        ],
        'country' => [ // This field is mandatory
            'type' => 'country',
            'required' => true,
            'label' => 'ארץ',
            'class' => ['form-row-last, hidden'],
        ],
    ]);
});


add_filter("option_woocommerce_registration_generate_password", function () {
    return 'yes';
});

add_filter('woocommerce_min_password_strength', function ($strength) {
    return 5;
}, 10, 1);


add_action('admin_head', 'my_custom_fonts');
function my_custom_fonts()
{
    echo '<style>
	  table.wp-list-table .column-order_items{
	      width:12ch!important;
	  }
	  .chosen-container-single .chosen-drop {
	      left: auto;
	      right: 0px;
	   }
	   select[name="_customer_billing_country"], select[name="_customer_shipping_country"] { 
	      display: none
	   }
	   #posts-filter option[value="bacs"], #posts-filter option[value="other"], #posts-filter option[value="cod"] {
	      display: none;
	   }
  </style>';
}

add_action('woocommerce_email_customer_details', 'my_completed_order_email_instructions', 50, 4);
function my_completed_order_email_instructions($order, $sent_to_admin, $plain_text, $email)
{

    if ($order->has_shipping_method('local_pickup_plus')) {
        $order_id = $order->get_id();

        echo '<p style="text-align: right;border-top: 1px solid;    padding-top: 20px;"><strong>עוד דברים שכדאי לדעת</strong></p>
	      <p style="text-align: right;"><u>זמן המשלוח:</u> המשלוח יגיע לחנות שבחרת תוך 7 ימי עסקים מתאריך הרכישה. במקרה שהמוצר צריך לעבור לייצור מיוחד, המשלוח ייקח עד 10 ימי עסקים ואנחנו נעדכן אותך על כך במייל. אומרים שבשביל דברים מיוחדים שווה לחכות</p>
	      <p style="text-align: right;">בכל מקרה, <strong>נעדכן אותך במסרון (</strong><strong>SMS</strong><strong>) ברגע שההזמנה שלך תגיע לסניף</strong>. ההזמנה תחכה לך במשך 10 ימי עסקים אחרי קבלת המסרון, ובמקרה שלא נאספה, תוחזר למשרדים שלנו.</p>
	      <p style="text-align: right;">ניתן לבטל עסקת רכישה באמצעות הודעה לשירות הלקוחות. ביטול עסקת רכישה כפוף לדמי ביטול בשיעור 5% מערך העסקה.</p>';
    } elseif ($order->has_shipping_method('flat_rate')) {
        echo '<p style="text-align: right;border-top: 1px solid;    padding-top: 20px;"><strong>עוד דברים שכדאי לדעת</strong></p>
	      <p style="text-align: right;"><u>זמן המשלוח:</u> המשלוח יגיע תוך 7 ימי עסקים מתאריך הרכישה. במקרה שהמוצר צריך לעבור לייצור מיוחד, המשלוח ייקח עד 10 ימי עסקים ואנחנו נעדכן אותך על כך במייל. אומרים שבשביל דברים מיוחדים שווה לחכות</p>
	      <p style="text-align: right;">בכל מקרה, <strong>אנחנו נעדכן אותך במסרון (</strong><strong>SMS</strong><strong>) ברגע שההזמנה שלך תיאסף מהמשרד שלנו</strong> ע"י חברת השליחויות, ולאחר מכן החברה תתאם מולך את הזמן המדויק למסירת המשלוח.</p>
	      <p style="text-align: right;">ניתן לבטל עסקת רכישה באמצעות הודעה לשירות הלקוחות. ביטול עסקת רכישה כפוף לדמי ביטול בשיעור 5% מערך העסקה.</p>';
    } elseif ($order->has_shipping_method('free_shipping')) {
        echo '<p style="text-align: right;border-top: 1px solid;    padding-top: 20px;"><strong>עוד דברים שכדאי לדעת</strong></p>
	      <p style="text-align: right;"><u>זמן המשלוח:</u> המשלוח יגיע תוך 2-3 ימי עסקים מתאריך הרכישה. במקרה שהמוצר צריך לעבור לייצור מיוחד, המשלוח ייקח עד 10 ימי עסקים ואנחנו נעדכן אותך על כך במייל. אומרים שבשביל דברים מיוחדים שווה לחכות</p>
	      <p style="text-align: right;">בכל מקרה, <strong>אנחנו נעדכן אותך במסרון (</strong><strong>SMS</strong><strong>) ברגע שההזמנה שלך תיאסף מהמשרד שלנו</strong> ע"י חברת השליחויות, ולאחר מכן החברה תתאם מולך את הזמן המדויק למסירת המשלוח.</p>
	      <p style="text-align: right;">ניתן לבטל עסקת רכישה באמצעות הודעה לשירות הלקוחות. ביטול עסקת רכישה כפוף לדמי ביטול בשיעור 5% מערך העסקה.</p>';
    } else {
        echo '<p style="text-align: right;border-top: 1px solid;    padding-top: 20px;"><strong>עוד דברים שכדאי לדעת</strong></p>
	      <p style="text-align: right;"><u>זמן המשלוח:</u> המשלוח יגיע לנקודת האיסוף שבחרת תוך 7 ימי עסקים מתאריך הרכישה. במקרה שהמוצר צריך לעבור לייצור מיוחד, המשלוח ייקח עד 10 ימי עסקים ואנחנו נעדכן אותך על כך במייל. אומרים שבשביל דברים מיוחדים שווה לחכות</p>
	      <p style="text-align: right;">בכל מקרה, <strong>Boxit תעדכן אותך במסרון (</strong><strong>SMS</strong><strong>) ברגע שההזמנה שלך תגיע.</strong>. ההזמנה תחכה לך במשך 48 שעות מקבלת המסרון אם נקודת האיסוף היא לוקר, או במשך 7 ימים מקבלת המסרון אם נקודת האיסוף היא חנות.</p>
	      <p style="text-align: right;">ניתן לבטל עסקת רכישה באמצעות הודעה לשירות הלקוחות. ביטול עסקת רכישה כפוף לדמי ביטול בשיעור 5% מערך העסקה.</p>';
    }
}

//add_filter( 'wvs_variable_item', 'my_wvs_variable_item', 10, 5);  

function my_wvs_variable_item($data, $type, $options, $args, $saved_attribute)
{

}

add_action('woocommerce_before_single_variation', 'sg_before_add_to_cart_btn');

function sg_before_add_to_cart_btn()
{
    global $product;
    $ids = $product->get_category_ids();
    if (is_array($ids) && in_array(1382, $ids)) {
        return;
    }
    echo '<div class="sizechoose"><a href="#" data-toggle="modal" data-target="#sizeChoose"><img src="' . img('icon-sixe-chart.png') . '">מה המידה שלי?</a></div>';
}

add_filter('wp_get_attachment_image_attributes', 'change_attachement_image_attributes', 20, 2);

function change_attachement_image_attributes($attr, $attachment)
{
    // Get post parent
    $parent = get_post_field('post_parent', $attachment);

    // Get post type to check if it's product
    $type = get_post_field('post_type', $parent);
    if ($type != 'product') {
        return $attr;
    }

    /// Get title
    $title = get_post_field('post_title', $parent);

    $attr['alt'] = $title;
    $attr['title'] = $title;

    return $attr;
}

function rename_order_status_type($order_statuses)
{
    /*the order you want the status to be in. If you want to keep the position of a certain status just leave it blank. In this case the first status will retain its position*/

    $key_order = array("", "wc-processing", "wc-on-hold", "wc-service-processin", "wc-pending", "wc-sent-to-manufacto", "wc-orderd-from-branc", "wc-service-completed", "wc-completed", "wc-refunded", "wc-partial-refund", "wc-cancelled", "wc-failed", "wc-awaiting-approval", "wc-transit", "wc-dispatched");
    return reorder_associate_arr($key_order, $order_statuses);
}

function reorder_associate_arr($key_order, $order_statuses)
{
    $new_arr = array();

    $keys = array_keys($order_statuses);
    $index = 0;
    foreach ($key_order as $key) {
        if (trim($key) == "")
            $new_arr[$keys[$index]] = $order_statuses[$keys[$index]];
        else
            $new_arr[$key] = $order_statuses[$key];

        $index++;
    }

    return $new_arr;
}

add_filter('woocommerce_register_shop_order_post_statuses', 'rename_order_status_type');

add_action('admin_footer', 'fix_bulk_edit_checkbox', 5);
function fix_bulk_edit_checkbox()
{
    echo '<style> .w3exabe .slick-header-column.ui-state-default{overflow:visible!important;}</style>';
}

add_image_size('getraffic-search-thumb', 150, 150, false);


add_filter('woocommerce_min_password_strength', function ($strength) {
    return 2;
}, 10, 1);

/**
 * Remove password strength check.
 */
function gt_remove_password_strength()
{
    wp_dequeue_script('wc-password-strength-meter');
}

add_action('wp_print_scripts', 'gt_remove_password_strength', 10);

add_filter('woocommerce_save_account_details_required_fields', 'wc_save_account_details_required_fields');
function wc_save_account_details_required_fields($required_fields)
{
    unset($required_fields['account_display_name']);
    return $required_fields;
}


add_action('woocommerce_save_account_details', function ($user_id) {
    wp_redirect(wc_get_account_endpoint_url('edit-account'));
    exit;
}, 10, 1);


add_filter('wp_mail', function ($args) {
    if (strpos($args['subject'], 'שינוי סיסמה') === false) {
        return $args;
    }

    $args['subject'] = str_replace('[בי יוניק]', '', $args['subject']);

    $args['to'] = $args['to'] . ', yaniv@getraffic.co.il';

    ob_start();
    (new WC_Emails())->email_header($args['subject']);
    echo $args['message'];
    (new WC_Emails())->email_footer();
    $message = ob_get_contents();
    ob_end_clean();

    $wcMail = new \WC_Email();
    $message = apply_filters('woocommerce_mail_content', $wcMail->style_inline($message));

    $args['message'] = str_replace('יצירת קשר', '<a href="' . site_url() . '/יצירת-קשר">יצירת קשר</a>', $message);
    $args['headers'] = 'Content-Type: text/html';
    return $args;
});


add_filter('wpcf7_validate_text', function ($result, $tag) {
    if ($tag->name === 'your-name' && !($_POST[$tag->name])) {
        $result->invalidate($tag, 'נצטרך את השם שלך');
    }
    if ($tag->name === 'your-email' && !($_POST[$tag->name])) {
        $result->invalidate($tag, 'נצטרך לדעת את המייל שלך');
    } else if ($tag->name === 'your-email' && !filter_var($_POST[$tag->name], FILTER_VALIDATE_EMAIL)) {
        $result->invalidate($tag, 'כתובת המייל אינה תקינה');
    }

    if ($tag->name === 'tel-171' && !($_POST[$tag->name])) {
        $result->invalidate($tag, 'נצטרך את הטלפון שלך');
    } else if ($tag->name === 'tel-171' && strlen($_POST[$tag->name]) !== 7) {
        $result->invalidate($tag, 'יש להזין 10 ספרות ללא מקף');
    }

    return $result;
}, 20, 2);

add_filter('wpcf7_validate_select', function ($result, $tag) {
    if ($tag->name === 'job-list' && ($_POST[$tag->name] === 'מספר משרה')) {
        $result->invalidate($tag, 'יש לבחור משרה מהרשימה');
    }
    return $result;
}, 20, 2);


add_filter('init', 'create_orders_txt_file');
function create_orders_txt_file()
{
    global $wpdb;
    //if (is_admin() ){
    if ($_REQUEST['create_txt'] == '1') {
        echo "<pre>";
        $args = array(
            'post_date' => '2019-11-14',
            'limit' => 999,
            'status' => array('processing', 'completed'),
            'type' => 'shop_order',
        );
        //$orders = wc_get_orders( $args );
        $today = date('Y-m-d');
        $found_orders = $wpdb->get_col("SELECT ID FROM wp_6ptneef36a_posts WHERE  post_status IN ( 'wc-processing', 'wc-completed' ) AND post_type='shop_order' AND post_date LIKE '$today%'");
        $fh_3 = fopen("/home/buntest/public_html/wp-content/themes/getraffic-child/export_orders.txt", "wa+");
        $rand = '';
        foreach ($found_orders as $order) {
            $string_to_write = '';
            $rand = rand(12, 9900);
            $order = wc_get_order($order);
            $get_short_id = $wpdb->get_col("SELECT meta_value FROM " . $wpdb->prefix . "postmeta WHERE  meta_key='_alg_wc_custom_order_number' AND post_id='" . $order->get_id() . "' ");
            //print_r($order->id);
            $tapuz_delivery = get_post_meta($order->get_id(), '_tapuz_ship_data');
            $customer_name = get_post_meta($order->get_id(), '_billing_first_name', true) . " " . get_post_meta($order->get_id(), '_billing_last_name', true);
            // Loop though order items shipping
            $pickup = '';
            $pickup_id = '';
            //print_r($order->get_shipping_methods());
            //echo"<br/>";
            foreach ($order->get_shipping_methods() as $item_id => $item) {


                $item_data = $item->get_data();

                //print_r($item_data);
                //echo"<br/>";


                if ($item_data['method_id'] == 'local_pickup_plus' && !empty($item_data['meta_data'][0])) {

                    $item_meta_data = $item_data['meta_data'][0]->get_data();

                    $pickup = ($item_meta_data['value']['id']);
                    $pickup_id = get_pickup_location($pickup);
                    //print_r($pickup_id);


                }


                // Displaying the row custom meta data Objects (just for testing)
                //echo '<pre>'; print_r($pickup_id); echo '</pre>';
            }

            $package_no = str_pad($get_short_id[0], 12);
            $process_type = str_pad('99', 3);
            $addspaces1 = str_pad('', 29);
            $client_name = mb_str_pad($customer_name, 40, " ", STR_PAD_RIGHT);
            $addspaces2 = str_pad('', 120);
            $client_phone = str_pad(get_post_meta($order->get_id(), '_billing_phone', true), 10);
            $addspaces3 = str_pad('', 50);
            //$delivery_no=str_pad($tapuz_delivery[0]['delivery_number'],10);
            $delivery_no = mb_str_pad($pickup_id, 10, " ", STR_PAD_RIGHT);
            $addspaces4 = str_pad('', 81);
            $customer_id = str_pad(get_post_meta($order->get_id(), '_customer_user', true), 10);
            $internet_store = str_pad('90', 3);
            $pickup_store = str_pad('90', 3);
            $string_to_write = $package_no . $process_type . $addspaces1 . $client_name . $addspaces2 . $client_phone . $addspaces3 . $delivery_no . $addspaces4 . $customer_id . $internet_store . $pickup_store;
            if ($pickup_id != false)
                fwrite($fh_3, $string_to_write . "\n");
            // echo$string_to_write;


        }
        echo 'done';
        fclose($fh_3);
        ///die;
        //$curl = shell_exec("curl -T /bunique/public_html/wp-content/themes/storesy-child/export_orders.txt -u ftpuser:ftp4bunique ftp://212.179.248.200/b-unique_export_orders.txt 2>&1");

        $ch = curl_init();
        $localfile = '/home/buntest/public_html/wp-content/themes/getraffic-child/export_orders.txt';
        $convert = 'iconv -f UTF8 -t WINDOWS-1255 ' . $localfile . ' > ' . $localfile . '2';
        exec($convert);

        $remotefile = 'b-unique_export_orders_'.time().'.txt';
        $fp = fopen($localfile . '2', 'r');
        curl_setopt($ch, CURLOPT_URL, 'ftp://ftpuser:ftp4bunique@212.179.248.200/' . $remotefile);
        curl_setopt($ch, CURLOPT_UPLOAD, 1);
        curl_setopt($ch, CURLOPT_INFILE, $fp);
        curl_setopt($ch, CURLOPT_INFILESIZE, filesize($localfile . '2'));
        curl_exec($ch);
        $error_no = curl_errno($ch);
        print_r($error_no);
        curl_close($ch);
        if ($error_no == 0) {
            $error = 'File uploaded succesfully.';
        } else {
            $error = 'File upload error.';
        }
        echo $error;
        die;
    }
    //}
}

function mb_str_pad($input, $pad_length, $pad_string, $pad_style, $encoding = "UTF-8")
{
    return str_pad($input,
        strlen($input) - mb_strlen($input, $encoding) + $pad_length,
        $pad_string, $pad_style);
}

function get_pickup_location($id)
{
    switch ($id) {
        case"0": //דרורים
            $location = 'דרורים';
            //return '23';
            break;
        case"1": //גבעתיים
            $location = 'גבעתיים';
            //return '13';
            break;
        case"2": //מלחה
            $location = 'מלחה';
            //return '22';
            break;
        case"4": //כפר גנים
            $location = 'כפר גנים';
            //return '15';
            break;
        case"5": // אם המושבות
            $location = 'אם המושבות';
            //return '20';
            break;
        case"6": //קריון
            $location = 'קריון';
            //return '24';
            break;
        case"7": //ראשונים
            $location = 'ראשונים';
            //return '21';
            break;
        case"9": //רחובות
            $location = 'רחובות';
            //return '25';
            break;
        case"10": //איילון
            $location = 'איילון';
            //return '19';
            break;
        case"11": //דיזינגוף
            $location = 'דיזינגוף';
            //return '16';
            break;
        case"12": //קריון
            $location = 'הגרא';
            //return '0';
            break;
        case"13": //שבעת הכוכבים
            $location = 'שבעת הכוכבים';
            //return '26';
            break;
        case"14": //כפר סבא
            $location = 'כפר סבא';
            //return '26';
            break;
        case"15": //עין שמר
            $location = 'עין שמר';
            //return '26';
            break;
        case"17": //עיר ימים
            $location = 'עיר ימים';
            //return '26';
            break;
        default:
            return false;
    }
    return mb_substr($location, 0, 10);
}

add_filter('woocommerce_redirect_single_search_result', '__return_false');

add_filter('woocommerce_product_is_on_sale', function ($sale, $product) {
    if (!$sale && $product instanceof WC_Product_Variation) {
        return wc_get_product($product->get_parent_id())->is_on_sale();
    }

    return $sale;
}, 10, 2);

add_filter('wc_social_login_buttons_return_url', function ($returnUrl) {
    if (isset($_GET['return'])) {
        return $_GET['return'];
    }

    return $returnUrl;
}, 10, 2);

add_action('woocommerce_checkout_create_order', function (WC_Order $order, $data) {
    if ($order->get_meta('_billing_checkbox')) {
        $order->add_meta_data('mailchimp_response',
            (new \Getraffic\MailChimp())->subscribe($order->get_billing_email(), $order->get_billing_first_name(), $order->get_billing_last_name(), '', true)
        );
    }

}, 10, 2);


//function woocommerce_shop_order_search_order_total( $search_fields ) {
//    $old_search = print_r($search_fields, true);
//    $search_fields = array("_order_key", "_billing_email", "_billing_phone", "_product_sku");
//    //error_log("search_fields1: $old_search" );
//    // error_log("new search_fields: ". print_r($search_fields, true) );
//    return $search_fields;
//}
//add_filter( 'woocommerce_shop_order_search_fields', 'woocommerce_shop_order_search_order_total', 99 );
//
//function billing_phone_search_fields( $meta_keys ){
//    $meta_keys[] = '_billing_phone';
//    $meta_keys[] = '_alg_wc_custom_order_number';
//    return $meta_keys;
//}
//add_filter( 'woocommerce_shop_order_search_fields', 'billing_phone_search_fields', 10, 1 );


add_filter('wc_pelecard_gateway_iframe_args', function ($args) {
    $args['CreateToken'] = true;
    return $args;
});

add_filter('wc_pelecard_gateway_supports', function ($supports) {
    $supports = ['products'];
    return $supports;
});

function woocommerce_shop_order_search_order_total($search_fields){
    $search_fields[] = '_alg_wc_custom_order_number';
    return $search_fields;
}
add_filter('woocommerce_shop_order_search_fields', 'woocommerce_shop_order_search_order_total');

function layer_get_request_ip() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function layer_is_office_ip( array $ips = ['62.219.136.63', '82.81.6.113'] ) {
    return in_array( layer_get_request_ip(), $ips );
}

add_action('wp_enqueue_scripts', 'layer_load_scripts_immediately_after_jquery', 5);
function layer_load_scripts_immediately_after_jquery(){
    wp_register_script(
        'jquery-global',
        get_stylesheet_directory_uri() . '/assets/js/jquery-global.js',
        array( 'jquery' ),
        time()
    );
    wp_enqueue_script( 'jquery-global' );
    GT::enqueueAsset('orderby-css', '/assets/src/scss/orderby.css', [], false, false);
}

/**
 *  Get all scripts and styles from Wordpress
 */
function layer_print_scripts_styles() {?>
    <script>
        /*if( ( 'jQuery' in  window) && ( '$' in window ) === false ) {
            window.$ = window.jQuery
        }*/
    </script>
<?php
    if( layer_is_office_ip() ) {

        $result = [];
        $result['scripts'] = [];
        $result['styles'] = [];

        // Print all loaded Scripts
        global $wp_scripts;
        foreach( $wp_scripts->queue as $script ) :
            //$result['scripts'][$script] =  $wp_scripts->registered[$script]->src . ";";
            $result['scripts'][$script] =  print_r($wp_scripts->registered[$script], true);
        endforeach;

        // Print all loaded Styles (CSS)
        global $wp_styles;
        foreach( $wp_styles->queue as $style ) :
            //$result['styles'][$style] =  $wp_styles->registered[$style]->src . ";";
        endforeach;

       // print_r( $result );
        //exit();
    }
}

add_action( 'wp_head', 'layer_print_scripts_styles', 10);


//add_action('wp_enqueue_scripts', 'layer_enqueue_my_scripts', 0);
function layer_enqueue_my_scripts(){

    if( layer_is_office_ip() ) {

        $result = [];
        $result['scripts'] = [];
        $result['styles'] = [];

        // Print all loaded Scripts
        global $wp_scripts;
        foreach( $wp_scripts->queue as $script ) :
            //$result['scripts'][$script] =  $wp_scripts->registered[$script]->src . ";";
            $result['scripts'][$script] =  print_r($wp_scripts->registered[$script], true);
        endforeach;

        // Print all loaded Styles (CSS)
        global $wp_styles;
        foreach( $wp_styles->queue as $style ) :
            $result['styles'][] =  print_r($wp_styles->registered[$style], true);
        endforeach;

        //print_r( $result );
        //exit();
    }
}

// Printed to the DOM before jquery has loaded
remove_action('wp_head', array('CriteoTagsProvider', 'addCriteoTag'));
add_action('wp_head', array('CriteoTagsProvider', 'addCriteoTag'), 999);

function bunique_get_picture_tag( $src, array $source = [], array $img_attributes = [] ) {
    $defaults = array(
        'alt'       => '',
        'loading'   => 'lazy',
        'class'     => 'img-responsive'
    );
    $img_attributes = wp_parse_args($img_attributes, $defaults );
    array_walk($img_attributes, function(&$value, $key) { $value = (string)$key . '="' . (string)$value . '"'; }); ?>
    <picture>
        <?php if( $source ) : ?>
            <?php foreach ( $source as $media => $srcset ) : ?>
                <source <?php if( $media ) : ?> media="<?php echo $media ?>" <?php endif;?> srcset="<?php echo $srcset; ?>">
            <?php endforeach; ?>
        <?php endif; ?>
        <img src="<?php echo $src; ?>" <?php echo implode(' ', $img_attributes ); ?>>
    </picture>
<?php }


add_action( 'after_setup_theme', 'bunique_setup' );
function bunique_setup() {

    add_image_size(
        'site-logo',
        204
    );
}

add_filter('woocommerce_restock_refunded_items', function(){return false;});