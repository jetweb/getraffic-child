<div class="container">
    <div class="footer-widgets">
        <div class="f-widget widget-right">
            <div class="row">
                <div class="col-md-6">
                    <div class="row full-width">
                        <a id="back_to_top"></a>
                        <div class="col-sm-4 pl-0">
                            <div class="footer-widget f-star">
                                <img src="<?php echo img('footer_star.png'); ?>" alt="" />
                                <?php wp_nav_menu([
                                    'theme_location' => 'footer-1',
                                    'menu_class'     => 'footer-menu',
                                    'fallback_cb'    => false,
                                ]); ?>
                            </div>
                        </div>
                        <div class="col-sm-4 pl-0">
                            <div class="footer-widget f-pencil">
                                <img src="<?php echo img('footer_pencil.png'); ?>" alt="" />
                                <?php wp_nav_menu([
                                    'theme_location' => 'footer-2',
                                    'menu_class'     => 'footer-menu',
                                    'fallback_cb'    => false,
                                ]); ?>
                            </div>
                        </div>
                        <div class="col-sm-4 pl-0">
                            <div class="footer-widget f-arrow">
                                <img src="<?php echo img('footer_arrow.png'); ?>" alt="" />
                                <?php dynamic_sidebar('footer-3'); ?>
                                <div class="footer-menu-icons">
                                    <?php
                                    $icons = gt_get_field('footer_menu_icons', 'option');
                                    foreach ($icons as $icon) {
                                        ?>
                                        <a href="<?php echo $icon['link']; ?>" rel="nofollow">
                                            <img src="<?php echo $icon['icon']; ?>" alt="">
                                        </a>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 pr-0">
                    <div class="col-md-5 pr-0">
                        <?php
                        $gcLink = gt_get_field('gift_card_link', 'option');
                        ?>
                        <a href="<?php echo $gcLink['link']; ?>" class="gc-link">
                        <h4 style="font-size: 24px; margin-bottom: 4px;">הגיפט קארד שלנו</h4>
                            <img src="<?php echo $gcLink['image']; ?>"/>
                        </a>
                    </div>
                    <div class="col-md-7">
                        <?php echo do_shortcode('[newsletter]'); ?>
                        <div class="only-mobile address-footer-mobile"><?php get_template_part('templates/contact/address'); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    #back_to_top{
        background-image: url('<?php echo img('back_to_top.png'); ?>');
    }
    .footer-widget {
        background-image: url('<?php echo img('border.png'); ?>');
    }
</style>

<script type="text/javascript">
    jQuery(document).ready(function () {
        let $ = jQuery;
		window.onscroll = function() {
		  if (document.body.scrollTop > 250 || document.documentElement.scrollTop > 250) {
			$("#back_to_top").show();
		  } else {
			$("#back_to_top").hide();
		  }
		}
        $("#back_to_top").click(function(){ 
            $('html, body').animate({scrollTop:0}, '300');
        });
    });
</script>
