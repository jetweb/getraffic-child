<!-- Modal -->
<div class="modal fade gtModal" id="registerModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="container">
                <div class="row">
                    <div class="join-us-container">
                        <h1 role="presentation" class="tony-title">הרשמה לאתר</h1>
                        <p>כיף שהחלטת להצטרף אלינו! מעכשיו תוכלי ליהנות מדגמים בלעדיים לאתר והמון מבצעים שווים</p>
                        <p class="validation"></p>
                        <form method="post" id="registrationForm">
                            <input type="hidden" name="action" value="ajax-register-user">
                            <div class="popup-firstname-field popup-firstname">
                                <input type="text" class="validate validate-only-letters" data-error-letters="אפשר להשתמש פה רק באותיות בעברית או באנגלית" data-empty-message="כדי שתוכלי להירשם, נצטרך לדעת מה השם הפרטי שלך" placeholder="שם פרטי *" id="first_name" name="first_name" />
                                <div class="error"></div>
                            </div>
                            <div class="popup-lastname-field popup-lastname">
                                <input type="text" class="validate validate-only-letters" data-error-letters="אפשר להשתמש פה רק באותיות בעברית או באנגלית" data-empty-message="כדי שתוכלי להירשם, נצטרך לדעת מה שם המשפחה שלך" placeholder="שם משפחה *" name="last_name"/>
                                <div class="error"></div>
                            </div>
                            <div class="popup-phone-field popup-phone">
                                <input type="text" class="validate" data-empty-message="נצטרך את הטלפון שלך כדי לעדכן אותך לגבי ההזמנות שלך" placeholder="טלפון נייד *" name="phone"/>
                                <div class="error"></div>
                            </div>
                            <div style="clear: both"></div>
                            <div class="popup-mail-field">
                                <input type="text" class="validate popup-mail" data-empty-message="כדי שתוכלי להירשם, נצטרך לדעת מה המייל שלך" placeholder="כתובת מייל *" name="email"/>
                                <div class="error error_mail"></div>
                            </div>
                            <div class="popup-password-field popup-password new_password form-row-eye">
                                <input type="password" class="validate validate-password" data-empty-message="כדי שהחשבון שלך באתר יהיה מאובטח, יש ליצור לו סיסמה" placeholder="סיסמה *" name="password"/>
                                <div class="error error_password"></div>
                                <img class="closed-eye" src="<?php echo img('closed-eye.png'); ?>"><img class="opened-eye" src="<?php echo img('opened-eye.png'); ?>">
                            </div>
                            <div class="popup-confirm-password-field popup-confirm-password confirm_new_password form-row-eye">
                                <input type="password" class="validate" data-empty-message="רק עוד פעם אחת בשביל לוודא... תודה :)" placeholder="אימות סיסמה *" name="password"/>
                                <div class="error error_confirm_password"></div>
                                <img class="closed-eye" src="<?php echo img('closed-eye.png'); ?>"><img class="opened-eye" src="<?php echo img('opened-eye.png'); ?>">
                            </div>
                            <label class="bd-benefits" for="accept_benefits">
                                <input type="checkbox" id="accept_benefits" name="accept_benefits"/>
                                <span>עדכנו אותי על מבצעים מיוחדים, קופונים בלעדיים ועוד דברים מעניינים שקורים באתר במייל וב-SMS</span>
                            </label>
                            <label class="bd-terms" for="accept_terms">
                                <input type="checkbox" id="accept_terms" name="accept_terms"/>
                                <span>אני מאשרת את <a href="#">תנאי השימוש</a> באתר *</span>
                                <span class="error_terms"></span>
                            </label>
                            <div><button class="tony-button full-width sharp-corners register-button" name="btn_submit">תרשמו אותי</button></div>
                            <div class="tooltip-phone">
                                <div class="tooltip-phone-image"><img src="<?php echo img('tooltip-phone-image.png') ?>"></div>
                                <div class="tooltip-phone-text">מספר הטלפון ישמש אותנו כדי לעדכן אותך לגבי הזמנות (למשל – אם הדגם שהזמנת צריך להישלח לייצור מיוחד) ולגבי מבצעים מיוחדים</div>
                            </div>
                            <div class="tooltip-mail">
                                <div class="tooltip-mail-image"><img src="<?php echo img('tooltip-mail-image.png') ?>"></div>
                                <div class="tooltip-mail-text">המייל שלך ישמש כדי להתחבר לאתר וגם כדי לשלוח לך פרטים על ההזמנות שלך, על החשבון שלך (למשל אם תצטרכי לשחזר סיסמה) וכדי לעדכן אותך על מבצעים</div>
                            </div>
                        </form>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="modal-bg-blue"></div>
    </div>
</div>

<script defer>

    jQuery(document).ready(function ($) {
        $('#registerModal .register-button').bind('click', function () {

            let valid = true;
            $('#registerModal input.validate').each(function () {
                if ($(this).val() == '') {
                    valid = false;
                    $(this).next().css('color', '#ff0000').html($(this).data('empty-message'));
                    $(this).css('border', '1px solid #ff0000'); 
                }
                else if ($(this).parent().hasClass('popup-phone') && !validatePhone($(this).val())) {
                    valid = false;
                    $(this).next().css('color', '#ff0000').html('יש להזין 10 ספרות ללא מקף');
                    $(this).css('border', '1px solid #ff0000');
                }
                else if ($(this).hasClass('popup-mail') && !validateEmail($(this).val())) {
                    valid = false;
                    $(this).next().css('color', '#ff0000').html('כתובת המייל אינה תקינה');
                    $(this).css('border', '1px solid #ff0000');
                }
                else if ($(this).hasClass('validate-only-letters') && hasNumberOrSpecialChar($(this).val())) {
                    valid = false;
                    $(this).next().css('color', '#ff0000').html($(this).data('error-letters'));
                    $(this).css('border', '1px solid #ff0000');
                }
                else if ($(this).hasClass('validate-password') && $(this).val().length < 6) {
                    valid = false;
                    $(this).next().css('color', '#ff0000').html('הסיסמה צריכה להיות מורכבת מ-6 תווים לפחות');
                    $(this).css('border', '1px solid #ff0000');
                }
                else{
                    $(this).next().html('');
                    $(this).css('border', '1px solid black'); 
                }
                
            });

            if (!$('#registerModal #accept_terms').prop('checked')) {
                valid = false;
                $('.error_terms').css('color', '#ff0000').html('יש לאשר את תנאי השימוש באתר');
            }else{
                $('.error_terms').html('');
            }

            var password = $('.new_password').find('.validate').val();
            var confirm_password = $('.confirm_new_password').find('.validate').val();

            if (password && confirm_password && password !== confirm_password) {
                valid = false;
                $('.error_confirm_password').html('הסיסמה לא תואמת לסיסמה בשדה הקודם, בבקשה נסי שוב');
            }
            
            if (!valid) {
                //$('p.validation').html(result);
            } else {
                let registeredEmail = $('form#registrationForm .popup-mail').val();
                $.ajax({
                    type: 'post',
                    url: ajax_url,
                    data: $('form#registrationForm').serialize().replace('phone=','phone=' + $('.popup-phonecode').val()),
                    beforeSend: function () {
                    },
                    complete: function () {
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        if (response.error) {
                            $('p.validation').html(response.error);
                        } else if (response.redirect) {
                            $("#registerModal").modal("hide");
                            $("#registerThankyou").modal("show");
                            $("#registerThankyou .register-email").text(registeredEmail);
                        }
                    },
                });
            }

            return false;
        });
    });

    jQuery('.form-row-eye img').click(function(){
            
            if(jQuery(this).prevAll(".form-row-eye input.validate").attr('type') == 'text'){
                jQuery(this).prevAll(".form-row-eye input.validate").attr('type', 'password');
                jQuery(this).css('display', 'none');
                jQuery(this).prev().css('display', 'block');
               }
            else{
                jQuery(this).prevAll(".form-row-eye input.validate").attr('type', 'text');
                jQuery(this).css('display', 'none');
                jQuery(this).next().css('display', 'block');
                
            }
        });

    jQuery(".popup-mail").focusin(function() { $('.tooltip-mail').show(); });
    jQuery(".popup-mail").focusout(function() { $('.tooltip-mail').hide(); });
    jQuery(".popup-phone").focusin(function() { $('.tooltip-phone').show(); });
    jQuery(".popup-phone").focusout(function() { $('.tooltip-phone').hide(); });

</script>
