<!-- Modal -->
<div class="modal fade popup-newsletter" id="newsletter_thankyou" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="container">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="popup-newsletter-left"><img src="<?php echo img('newsletter-left.png') ?>"></div>
                    <div class="popup-newsletter-right">
                        <div class="newsletter-thankyou-plane"><img src="<?php echo img('newsletter-thankyou-image.png') ?>"></div>
                        <h1 role="presentation" class="newsletter-thankyou-title">נרשמת בהצלחה לעדכונים שלנו!</h1>
                        <p class="newsletter-thankyou-subtitle">יש לנו הרגשה שבקרוב תקבלי כל-מיני דברים שווים ומעניינים, אז כדאי לשים לב לתיבת המייל</p>
                        <div class="newsletter-thankyou-img"><img src="<?php echo img('gold-line-horizontal.png'); ?>"></div>                       
                        <p class="newsletter-thankyou-bottomtext"><a href="/">המשיכי לאתר</a></p>
                        <p class="newsletter-thankyou-bottom">* שימי לב גם לתיבת הספאם (דואר זבל) שלך כדי לראות שלא הגענו לשם בטעות. חבל שתפספסי את כל הטוב הזה...</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>