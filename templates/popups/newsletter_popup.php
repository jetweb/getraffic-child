<!-- Modal -->
<div class="modal fade popup-newsletter" id="popupNewsletter" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="container">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="row">
                    <div class="popup-newsletter-left test"><img src="<?php echo img('newsletter-left.png') ?>"></div>
                    <div class="popup-newsletter-right">
                        <div class="popup-newsletter-plane"><img src="<?php echo img('newsletter-plane.png') ?>"></div>
                        <h1 role="presentation" class="popup-newsletter-title">עדכונים שיתאימו לך בדיוק</h1>
                        <div class="popup-newsletter-img"><img src="<?php echo img('gold-line-horizontal.png'); ?>"></div>
                        <div class="popup-newsletter-text">
                            <p>רוצה לדעת ראשונה על מבצעים, להספיק לתפוס את המידות הכי מבוקשות בסוף עונה וליהנות מקופונים בלעדיים?</p>
                        </div>
                        <?php echo do_shortcode('[newsletter]'); ?>
                        <p class="popup-newsletter-bottomtext"><a href="#">אולי בפעם אחרת >></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    document.addEventListener('wpcf7mailsent', function (event) {
        if ('384' == event.detail.contactFormId) {
            $("#popupNewsletter").modal("hide");
            $("#newsletter_thankyou").modal("show");
        }
    }, false);
</script>