<!-- Modal -->
<div class="modal fade gtModal" id="registerModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="container">
                <div class="row">
                    <div class="join-us-container">
                        <h1 class="tony-title">Hi there!</h1>
                        <p class="validation">

                        </p>
                        <form method="post" id="registrationForm">
                            <input type="hidden" name="action" value="ajax-register-user">
                            <input type="text" class="validate popup-firstname" placeholder="שם פרטי *" name="first_name" />
                            <input type="text" class="validate popup-lastname" placeholder="שם משפחה *" name="last_name"/>
                            <input type="text" class="validate popup-phone" placeholder="טלפון נייד *" name="phone"/>
                            <select name="phonecode" class="popup-phonecode">
                                <option value="050">050</option>
                                <option value="052">052</option>
                                <option value="053">053</option>
                                <option value="054">054</option>
                                <option value="058">058</option>
                                <option value="073">073</option>
                                <option value="077">077</option>
                            </select>
                            <input type="text" class="validate" placeholder="כתובת מייל *" name="email"/>        
                            <div class="new_password form-row-eye">
                                <input type="password" class="validate" placeholder="סיסמא *" name="password"/>
                                <img class="closed-eye" src="<?php echo img('closed-eye.png'); ?>"><img class="opened-eye" src="<?php echo img('opened-eye.png'); ?>">
                            </div>
                            <div class="confirm_new_password form-row-eye">
                                <input type="password" class="validate" placeholder="אימות סיסמא *" name="password"/>
                                <img class="closed-eye" src="<?php echo img('closed-eye.png'); ?>"><img class="opened-eye" src="<?php echo img('opened-eye.png'); ?>">
                            </div>
                            <label class="bd-benefits" for="accept_benefits">
                                <input type="checkbox" id="accept_benefits" name="accept_benefits"/>
                                <span>ברצוני לקבל פרסום על הטבות, עדכונים וקולקציות חדשות באמצעות דוא״ל וsms</span>
                            </label>
                            <label class="bd-terms" for="accept_terms">
                                <input type="checkbox" id="accept_terms" name="accept_terms"/>

                                <span>מאשרת את <a href="#">תנאי השימוש</a></span>
                            </label>
                            <div><button class="tony-button full-width sharp-corners register-button">הרשמה</button></div>
                        </form>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="modal-bg-blue"></div>
    </div>
</div>

<script defer>
    jQuery(document).ready(function ($) {
        $('.register-button').bind('click', function () {
            let result = validate();
            if (result) {
                $('p.validation').html(result);
            } else {
                $.ajax({
                    type: 'post',
                    url: ajax_url,
                    data: $('form#registrationForm').serialize().replace('phone=','phone=' + $('.popup-phonecode').val()),
                    beforeSend: function () {
                    },
                    complete: function () {
                    },
                    success: function (response) {
                        response = JSON.parse(response);
                        if (response.error) {
                            $('p.validation').html(response.error);
                        } else if (response.redirect) {
                            location.href = response.redirect;
                        }
                    },
                });
            }

            return false;
        });
    });

    function validate() {

        let result = '';
        
        $('#registerModal input.validate').each(function () {
            if ($(this).val() == '') {
                result += 'נא להזין ' + $(this).attr('placeholder') + '<br/>';
            }
        });

        if (!$('#registerModal #accept_terms').prop('checked')) {
            result += 'נא להסכים לתנאי השימוש';
        }

        return result;
    }
        jQuery('.form-row-eye img').click(function(){
            
            if(jQuery(this).prevAll(".form-row-eye input.validate").attr('type') == 'text'){
                jQuery(this).prevAll(".form-row-eye input.validate").attr('type', 'password');
                jQuery(this).css('display', 'none');
                jQuery(this).prev().css('display', 'block');
               }
            else{
                jQuery(this).prevAll(".form-row-eye input.validate").attr('type', 'text');
                jQuery(this).css('display', 'none');
                jQuery(this).next().css('display', 'block');
                
            }
        });
</script>

<style>

    .new_password, .confirm_new_password{
        position: relative;
    }
    .closed-eye{
        position: absolute;
        left: 15px;
        top: 25px;        
    }
    .opened-eye{
        position: absolute;
        left: 15px;
        top: 20px;
        display: none;
    }
    

    .join-us-container {
        margin: 0px auto;
        width: 500px;
        text-align: center;
    }

    .join-us-container h1 {
        direction: ltr;
    }

    .gtModal .modal-dialog .modal-content .join-us-container form {
        width: 80%;
        text-align: center;
        margin: auto;
    }
    
    .bd-terms{
        margin: 0;
        float: right;
    }
    .bd-terms a{
        text-decoration: underline;
    }
/*
    input[type='text'],
    input[type='password'] {
        width: 100%;
        border: 1px solid black;
        margin: 10px 0;
        padding: 7px 15px 5px;
    }
*/
    .gtModal .modal-dialog .modal-content input[type='text'], input[type='password'] {
        width: 100%;
        border: 1px solid black;
        margin: 10px 0;
        padding: 7px 15px 5px;
    }

    p.validation {
        text-align: right;
        padding-right: 40px;
        color: #ce1f16;
    }
    input[type="checkbox"] {
      appearance: none;
      -webkit-appearance: none;
      -moz-appearance: none;
    }
    input[type="checkbox"] + span:before {
      font: 12pt FontAwesome;
      content: '\00f096';
      display: inline-block;
      width: 16pt;
    }
    input[type="checkbox"]:checked + span:before {
        content: '\00f046';        
    }
    .gtModal {
/*    z-index: 9999;*/
}
@media screen and (max-width: 500px){
	.join-us-container {        
        width: 100%;
    }
    .join-us-container p{        
        padding: 0 10px;
    }
}
</style>