<!-- Modal -->
<div class="modal fade gtModal" id="registerThankyou" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="container">
                <div class="row">
                    <div class="registration_thankyou">
                        <h1 role="presentation">הדרך לרגליים מפונקות ומאושרות יותר מתחילה כאן</h1>
                        <p>נרשמת בהצלחה לאתר בי יוניק!</p>
                        <p>כבר הרבה יותר נחמד לנו כשאת פה. שלחנו לך את פרטי ההתחברות שלך ועוד קצת מידע ל  <span class="register-email"></span></p>
                        <a href="<?php get_site_url(); ?>">שנתחיל בקניות?</a>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="modal-bg-blue"></div>
    </div>
</div>
