<!-- Modal -->
<div class="modal fade passReset" id="resetPassword" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="container">
                <div class="row">                    
                    <div class="reset_password">
                        <h1 role="presentation" class="passReset-title">איפוס סיסמא</h1>
                        <div class="passReset-img"><img src="<?php echo img('gold-line-horizontal.png'); ?>"></div>
                        <div class="passReset-text"><p>איבדת את הסיסמא? הכניסי את המייל עמו יצרת את החשבון<br>ותקבלי אליו קישור ליצירת סיסמא חדשה</p></div>

                        <form class="passReset-form" id="password_reset" method="post" action="/my-account/lost-password/">
                            <input type="text" class="validate" placeholder="כתובת מייל" name="user_login"/>
                            <div><button class="tony-button full-width sharp-corners"></button></div>

                            <?php do_action( 'woocommerce_lostpassword_form' ); ?>


                            <p class="woocommerce-form-row form-row">
                                <input type="hidden" name="wc_reset_password" value="true" />
                                <button type="submit" class="woocommerce-Button button" value="שלחו לי מייל לאיפוס סיסמא">שלחו לי מייל לאיפוס סיסמא</button>
                            </p>

                            <?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>
                        </form>
                        <p class="reset-message" style="color: red;"></p>
                    </div>  
                    <div class="reset_password-thankyou">
                        <h1 role="presentation" class="passReset-title-thankyou">איפוס סיסמא נשלח אלייך למייל</h1>
                        <div class="passReset-img"><img src="<?php echo img('gold-line-horizontal.png'); ?>"></div>
                        <div class="passReset-text-thankyou"><p>עדיין לא מצליחה לשחזר סיסמא? ניתן לפנות לשירות הלקוחות שלנו 073-3383888</p></div>
                        <div class="passReset-link-thankyou"><a href="/my-account">חזרה לעמוד התחברות </a></div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .reset_password-thankyou{
        display: none
    }
</style>
<script type="text/javascript" defer>
     jQuery(document).ready(function ($) {
        $('.passReset .reset_password .ResetPassword p.form-row button.button').html('שלחו לי מייל לאיפוס סיסמא');
        $(".passReset .reset_password .ResetPassword p.form-row input.input-text").attr("placeholder", "כתובת מייל");

        $('form#password_reset').submit(function(e) {
            e.preventDefault();
            $('p.reset-message').text('');

            var form = $(this);
            var url = form.attr('action');

            if (!$(".passReset .reset_password input[name=user_login]").val().length) {
                $('p.reset-message').text('יש להזין כתובת מייל');
                return;
            }
            if (!validateEmail($(".passReset .reset_password input[name=user_login]").val())) {
                $('p.reset-message').text('כתובת המייל אינה תקינה');
                return;
            }

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                success: function(data)
                {
                    if ($(data).find('.woocommerce-message').length) {
                        $('p.reset-message').text($(data).find('.woocommerce-message').text());
                        $('.reset_password-thankyou').show(200);
                        $('.reset_password').hide(200)
                    }
                    else if ($(data).find('.woocommerce-error').length) {
                        $('p.reset-message').text($(data).find('.woocommerce-error').text().replace('שם משתמש או כתובת אימייל', 'כתובת מייל'));
                    }
                }
            });


            return false;

        });

    });
</script>
