<!-- Modal -->
<div class="modal fade sizeChoose" id="sizeChoose" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="popup-container">
                <div class="popup-content">
                    <div class="row">
                        <div class="col-lg-12 popup-top">
                            <div class="popup-left">
                                <div class="popup-foot"><img class="size-full wp-image-337 aligncenter" src="<?= gt_get_field('popup-foot-image', 'option'); ?>" alt="" width="159" height="235" /></div>
                                <?= gt_get_field('popup-foot-text', 'option'); ?>
                                <div class="popup-heart"><img class="size-full wp-image-338 aligncenter" src="<?= gt_get_field('popup-heart', 'option'); ?>" alt="" width="45" height="45" /></div>
                                <p style="font-weight: bold;">כדאי להשתדל ולמדוד בצורה הכי מדויקת שאפשר, כי ההבדל בין מידה למידה הוא רק כמה מילימטרים. 
בהצלחה ותתחדשי!</p>

                            </div>
                            <div class="popup-line"><img src="<?php echo img('popup-divider-2x300.png'); ?>"></div>
                            <div class="popup-right">
                                <h1 role="presentation">מדריך מידת נעליים</h1>
                                <p style="font-weight: bold;">כך תדעי מה המידה הנכונה בשבילך</p>

                                <h1 role="presentation">1</h1>
                                <?= gt_get_field('text1', 'option'); ?>
                                <h1 role="presentation">2</h1>
                                <?= gt_get_field('text2', 'option'); ?>
                                <h1 role="presentation">3</h1>
                                <?= gt_get_field('text3', 'option'); ?>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 popup-table">
                            <h4>נעלי נשים</h4>
                            <table>
                                <thead>
                                    <tr class="spacer">
                                        <th>מידת נעליים</th>
                                        <th>35</th>
                                        <th>36</th>
                                        <th>37</th>
                                        <th>38</th>
                                        <th>39</th>
                                        <th>40</th>
                                        <th>41</th>
                                        <th>42</th>
                                        <th>43</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>אורך כף הרגל (ס''מ)</td>
                                        <td>21.5-22.1</td>
                                        <td>22.2-22.8</td>
                                        <td>22.9-23.5</td>
                                        <td>23.6-24.2</td>
                                        <td>24.3-24.9</td>
                                        <td>25-25.6</td>
                                        <td>25.7-26.3</td>
                                        <td>26.4-26.9</td>
                                        <td>27-27.6</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 popup-table popup-second">
                            <h4>נעלי ילדים</h4>
                            <table>
                                <thead>
                                    <tr class="spacer">
                                        <th>מידת נעליים</th>
                                        <th>22</th>
                                        <th>23</th>
                                        <th>24</th>
                                        <th>25</th>
                                        <th>26</th>
                                        <th>27</th>
                                        <th>28</th>
                                        <th>29</th>
                                        <th>30</th>
                                        <th>31</th>
                                        <th>32</th>
                                        <th>33</th>
                                        <th>34</th>
                                        <th>35</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>אורך כף הרגל (ס"מ)
                                        <td>14.7</td>
                                        <td>15.2</td>
                                        <td>15.7</td>
                                        <td>16.5</td>
                                        <td>17</td>
                                        <td>18</td>
                                        <td>18.4</td>
                                        <td>19</td>
                                        <td>19.7</td>
                                        <td>20.4</td>
                                        <td>21.3</td>
                                        <td>21.5</td>
                                        <td>22.2</td>
                                        <td>22.8</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
