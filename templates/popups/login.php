<div class="login-popup">
    <?php if (!is_user_logged_in()) { ?>
        <h2 class="account">כניסה לחשבון</h2>
        <span></span>
        <div class="login-close">x</div>
        <div class="padded">
            <input id="loginUser" type="text" name="username" placeholder="כתובת מייל"/>
            <input id="loginPass" type="password" name="pass" placeholder="סיסמה"/>
            <div class="login-error login-message"></div>
            <div class="login-success login-message"></div>
            <?php if (is_checkout()) {
                ?>
                <a class="forgot-pass" href="<?php echo wp_lostpassword_url(); ?>">שכחתי סיסמה</a>
                <?php
            } else {
                ?>
                <a class="forgot-pass" href="#" data-toggle="modal" data-target="#resetPassword">שכחתי סיסמה</a>
            <?php } ?>
            <a class="forgot-pass-mobile" href="<?php echo get_site_url() . '/my-account/lost-password' ?>">שכחתי סיסמה</a>
            <a href="" class="tony-button sharp-corners login-popup-btn">
                התחברי
            </a>
            <div class="facebook">
                <?php
                $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                echo do_shortcode('[woocommerce_social_login_buttons return_url="' . $actual_link . '"]');
                ?>
            </div>
            <?php wp_nonce_field('ajax-login-nonce', 'security'); ?>
            <p class="register">חדשה אצלנו?<a href="#" data-toggle="modal" data-target="#registerModal">הירשמי לאתר</a></p>
            <p style="display: none;" class="register"><a href="#" data-toggle="modal" data-target="#popupNewsletter">Stay in touch</a></p>
            <p style="display: none;" class="register"><a href="#" data-toggle="modal" data-target="#newsletter_thankyou">newsletter_thankyou</a></p>
        </div>
    <?php } else { ?>
        <div class="padded">
            <div class="account-logged-in">

                <span class="name">היי

                    <?php
                    $user = wp_get_current_user();
                    if ($user->first_name) {
                        echo $user->first_name;
                    } else {
                        echo $user->display_name;
                    }
                    ?></span>
                <a class="logout-link" href="<?php echo wp_logout_url(home_url()); ?>">התנתקי</a>
            </div>
            <a class="account-logged-in account-popup-link my-account" href="<?php echo esc_url(wc_get_endpoint_url('', '', wc_get_page_permalink('myaccount'))); ?>">
                החשבון שלי
            </a>
            <a class="account-logged-in account-popup-link my-orders" href="<?php echo esc_url(wc_get_endpoint_url('orders', '', wc_get_page_permalink('myaccount'))); ?>">
                ההזמנות שלי
            </a>
            <a class="account-logged-in account-popup-link my-wishlist" href="<?php echo esc_url(wc_get_endpoint_url('mywishlist', '', wc_get_page_permalink('myaccount'))); ?>">
                פריטים שאהבתי
            </a>
            <!--

                        <a class="account-logged-in account-popup-link my-points">

                            בחשבונך 0 נקודות

                        </a>

            -->
        </div>
    <?php } ?>
</div>
<script defer>
    jQuery(document).ready(function ($) {
        $('#loginUser, #loginPass').keypress(function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                triggerLogin();
                return false;
            }
        });
        $('.login-popup-trigger').click(function () {
            $('.login-popup').toggle("slide", {direction: "up"}, "slow");
            $('.topicon.login').toggleClass('active');
            $('.mini-cart-wrapper').css('display', 'none');
            return false;
        });
        $('.login-close').click(function () {
            $('.login-popup').toggle("slide", {direction: "up"}, "slow");
            return false;
        });
        $('a.login-popup-btn').click(function (e) {
            e.preventDefault();
            triggerLogin();
            return false;
        });
        $(document.body).on('login_success', function (e, message) {
            $('.login-success').text(message);
        });
        $(document.body).on('login_error', function (e, message) {
            $('.login-error').text(message);
        });
    });

    function triggerLogin() {
        $('.login-message').text('');
        logUserIn(
            $('#loginUser').val(),
            $('#loginPass').val(),
            $('#security').val(),
            true
        );
    }
</script>

