<div class="contact-form">    
    <?php
    if (have_posts()) {
        while (have_posts()) : the_post();
            ?>                
            <?php the_content(); ?>
        <?php
        endwhile;
    }
    ?>
</div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        let $ = jQuery;
        $('#imgupload').change( function(event) {
            var len = $('.img-remove').length;
            if(len < 1){
             $('.contactus-file').append('<a class="img-remove" id="remvImg" style="" onclick="removeimg()" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Remove Image"><i class="fa fa-trash"></i></a>');
            }
        });        
    });
    function removeimg(){
        $('#imgupload').val('');
        $("#remvImg").remove();
    }
    document.addEventListener('wpcf7mailsent', function (event) {
        if ('4043661249154' == event.detail.contactFormId) {
            setTimeout(function(){
                location = 'תודה';
            }, 0);
        }
    }, false);
</script>
