<div class="contact-details">
    <p class="contact-bottom-title"><?php echo gt_get_field('contact_bottom_title', 279); ?></p>
    <p class="contact-bottom-subtitle"><?php echo gt_get_field('contact_bottom_subtitle', 279); ?></p>    
    <div class="contact-icons">
        <?php 
        $contact_icons = gt_get_field('contact_icons', 279);  
        $i = 1;
        foreach($contact_icons as $contact_icon){ ?>
            <div class="contact-icon">
                <div class="contact-icon-image"><a href="<?php echo $contact_icon['link']; ?>"><img src="<?php echo $contact_icon['image']['url']; ?>"/></a></div>
                <div><img src="<?php echo img('icon-bottom-image.png'); ?>"/></div>
                <p><a class="contact-icon-<?php echo $i; ?>" href="<?php echo $contact_icon['link']; ?>"><?php echo $contact_icon['text']; ?></a></p>
            </div>        
      <?php $i++; } ?>
    </div>    
</div>
