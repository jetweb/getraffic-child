<?php
$order   = get_query_var('order');
$items   = get_query_var('orderItems');
$subject = get_query_var('emailSubject');
(new WC_Emails())->email_header($subject);
?>
    <h1>
        <?= $subject ?>
    </h1>
<h3>הלקוח ביקש להחליף את המוצרים הבאים : </h3>
<ul>
    <?php
        foreach ($items as $item) {
            if ($item instanceof WC_Order_Item_Product) {
            ?>
            <li><?php echo $item->get_id() ?> - <?php echo $item->get_name() ?></li>
        <?php }
        }
    ?>
</ul>
    <a href="<?php echo get_edit_post_link($order->get_id()); ?>">לחץ כאן לצפייה בהזמנה</a>
<?php (new WC_Emails())->email_footer(); ?>
