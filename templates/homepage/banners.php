<!--
<div class="only-mobile pink-banner-mobile">
    <?php $sliders = gt_get_field('sliders', 'option'); ?>
    <?php foreach ($sliders as $slider) { ?>
        <div>
            <img src="<?php echo $slider['image']; ?>" alt="">
            <div class="text"><?php echo $slider['text']; ?></div>     
        </div>
    <?php } ?>
</div>
-->

<div class="container">
    <div class="homepage-banners">
        <?php
        $banners = gt_get_field('homepage_banners');

        foreach ($banners as $banner) {
            ?>
            <div class="homepage-banner">
                <?php
                if ($banner['inverted']) {
                    printBannerText($banner);
                    printBannerImage($banner);
                } else {
                    printBannerImage($banner);
                    printBannerText($banner);
                }
                ?>
            </div>
        <?php } ?>
    </div>
    <div class="seperator"></div>
    <div class="clearfix"></div>
</div>
<style>
    .homepage-banners {
        display: flex;
    }

    .homepage-banner {
        width: 100%;
        padding: 15px;
    }

    .homepage-banner a img {
        width: 100%;
    }

    .banner-text {
        text-align: center;
        /* background-image: url('<?php echo img('banner_border.png'); ?>'); */
        background-repeat: no-repeat;
        background-position: bottom center;
        padding-bottom: 10px;
        margin-bottom: 40px;
    }

    .english-text {
        font-size: 32px;
    }

    a.banner-image {
        position: relative;
    }

    .banner-button {
        position: absolute;
        border: 2px solid white;
        color: white;
        margin-left: auto;
        margin-right: auto;
/*        left: 0;*/
/*        right: 0;*/
        left: -30px;
        right: 30px;
        top: 0;
        width: 125px;
        direction: ltr;
        text-align: center;
        font-weight: 100;
        padding: 5px;
        font-size: 13px;
        letter-spacing: 1px;
        display: none;
    }

    .homepage-banner:hover .banner-button {
        display: inline;
    }

    .seperator {
        background-image: url('<?php echo img('separator.png'); ?>');
        width: 88%;
        height: 80px;
        background-repeat: no-repeat;
        background-position: right;
        float: left;
        clear: both;
    }
    .only-mobile{
        display: none;
    }
@media screen and (max-width: 768px) {
    .only-mobile{
        display: block;
    }
    .only-mobile.pink-banner-mobile{
        display: block;
        text-align: center;
        background: #f6f1f0;
        margin-top: 14px;
    }
    .only-mobile.pink-banner-mobile button.slick-prev, .only-mobile.pink-banner-mobile button.slick-next{
        display: none !important;
    }
    .only-mobile.pink-banner-mobile img{
        padding-top: 20px;
        margin: auto;
        width: 20%;
    }
    .only-mobile.pink-banner-mobile .text{
        padding-top: 10px;
    }
    .only-mobile.pink-banner-mobile div{
        font-size: 10px;
        padding: 0px 0 5px 0;
        margin-top: -1px;
        direction: ltr;
    }
    .homepage-banner a img {
        max-height: 133px;
        object-fit: cover;
    }
    .banner-text {
        margin-bottom: -15px;
        font-size: 14px;
    }
    .homepage-banners {
        margin-top: 7px;
    }
    .homepage-banner {
        padding: 7px 0;
    }
    .english-text {
        font-size: 26px;
        letter-spacing: 2px;
    }
    .seperator {
        width: 38%;
        height: 40px;
        float: none;
        background-size: contain;
        margin-top: 0px;
    margin-right: -13px;
    }
}  
    
    
    .banner-overlay {
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        right: 0;
        justify-content: center;
        align-items: center;
        background: rgb(0, 0, 0, 0.6);
        display: none;
    }
    .overlayed-image:hover .banner-overlay {
        display: flex;
    }
    .overlayed-image{
        position: relative;
    }
    .homepage-banner .banner-overlay .banner-link {
        border: 1px solid white;
        color: #FFF;
        font-size: 10px;
        font-weight: 100;
        padding: 9px 32px 7px;
        direction: ltr;
        font-family: 'assistantsemibold', 'NarkisBlockMFW', 'Heebo', sans-serif;
    }
    
</style>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.pink-banner-mobile').slick({
            dots: true,
            autoplay: true,
            autoplaySpeed: 3000,
        });
    });
</script>
<?php
function printBannerImage($banner)
{ ?>
    <a href="<?php echo $banner['link']; ?>" class="banner-image">
        
        <img class="mob" src="<?php echo $banner['mobile_image']; ?>" alt="">
        
        <div class="overlayed-image desktop">
            <img src="<?php echo $banner['image']; ?>" alt="">
            <div class="banner-overlay">
                <a href="<?php echo $banner['link']; ?>" class="banner-link"><?php echo $banner['button_text']; ?></a>
            </div> 
        </div>
        
        
    </a>
<?php }

function printBannerText($banner)
{ ?>
<a href="<?php echo $banner['link']; ?>" class="banner-link">
    <div class="banner-text-wrapper">
        <div class="banner-text">
            <h2 class="english-text" role="heading" aria-level="2">
                <?php echo $banner['english_text']; ?>
            </h2>
            <?php echo $banner['hebrew_text']; ?>
        </div>
    </div>
</a>
<?php } ?>

<style>
img.mob {
    display: none;
}

@media screen and (max-width: 768px) {
    .desktop {display: none;}
    img.mob {
        display: block;
    }
}
</style>
