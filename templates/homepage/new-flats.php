<?php $settings = gt_get_field('new_flats'); ?>
<div class="container">
    <div class="new-flats">
        <div class="white-border">
            <h1>
                <?php echo $settings['title']; ?>
            </h1>
            <?php echo $settings['text']; ?>
        </div>
        <img src="<?php echo $settings['image']; ?>" alt="" class="flats-image">
        <span class="sideways">
            <?php echo $settings['title']; ?>
        </span>
    </div>
</div>
<style>
    .new-flats {
        background-image: url('<?php echo img('flat_border.png'); ?>');
        background-repeat: no-repeat;
        background-position: left bottom;
        padding-bottom: 40px;
        position: relative;
        width: 100%;
        margin-bottom: 40px;
        margin-top: 60px;
    }

    img.flats-image {
        width: 60%;
    }

    span.sideways {
        transform: rotate(-90deg);
        position: absolute;
        font-size: 60px;
        letter-spacing: 10px;
        left: -70px;
        top: 280px;
    }

    .white-border {
        background-image: url('<?php echo img('white_frame.png');?>');
        background-size: 100% 100%;
        width: 583px;
        height: 583px;
        position: absolute;
        padding: 120px 80px;
        top: 35px;
        left: 140px;
    }

    @media screen and (min-width: 1600px) {
        .white-border p {
            font-size: 22px;
        }
    }

    @media screen and (max-width: 1600px) {
        .white-border {
            width: 450px;
            height: 450px;
        }

        span.sideways {
            top: 200px;
        }
    }

    @media screen and (max-width: 1200px) {
        .white-border {
            width: 350px;
            height: 350px;
            padding: 40px;
        }

        span.sideways {
            top: 150px;
        }
    }
</style>

