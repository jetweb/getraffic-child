<div class="pink-banners">

    <div class="container">

        <?php

        $banners = get_field('p_b', get_queried_object_id());

        foreach ($banners as $banner) {

            ?>

            <div class="pink-banner">

                <img src="<?php echo $banner['image']; ?>" alt="">

                <div class="text">

                    <?php echo $banner['text']; ?>

                </div>

            </div>

            <?php

        } ?>

    </div>
</div>
<?php 
$stores = get_field('our_stores_mobile', get_queried_object_id());
?>

<div class="only-mobile stores-mobile-container">

    <div class="stores-mobile">

        <img src="<?php echo $stores['title_image']; ?>">

        <div class="text"><?php echo $stores['text']; ?></div>

        <a href="<?php echo $stores['link']; ?>"><?php echo $stores['link_text']; ?></a>

    </div>

</div>

<style>


    .pink-banners {

        background-color: #f5e7e4;



    }

    .pink-banners .container {

        display: flex;

        justify-content: center;

        flex-wrap: wrap;

    }

    .pink-banner {

        padding: 40px;

        text-align: center;

    }

    .pink-banner img{

        max-height: 150px;

    }

    .pink-banner .text {

        font-size: 18px;

        font-weight: bold;

    }

    .only-mobile{

        display: none;

    }

@media screen and (max-width: 768px) {

 
   .pink-banners{

        display: none;

        margin-top: 15px;

    }

    .only-mobile{

        display: block;

    }

    .pink-banners .container {

        justify-content: space-between;

    }

    .pink-banner {

        text-align: center;

        width: 25%;

        padding: 20px 7px 10px;
        display: flex;
        flex-direction: column;
        justify-content: space-around;

    }

    .pink-banner img{

        width: 100%;

    }

    .pink-banner .text {

        font-size: 12px;

    }

    .only-mobile .stores-mobile{

        background: url(<?php echo $stores['bg_image']; ?>);

        background-size: cover;

        background-repeat: no-repeat;

        background-position: top center;  

        text-align: center;

        padding-bottom: 14%;

    }

    .only-mobile.stores-mobile-container{

        margin: 0 10px;

    }

    .only-mobile .stores-mobile img{

        width: 35%;

        padding-top: 29px;

    }

    .only-mobile .stores-mobile .text{

        color: #fff;

    font-size: 14px;

    }

    .only-mobile .stores-mobile a{

        color: #fff;

	font-size: 12px;
	border: 1px solid #fff;
	padding: 4px 10px;
	margin-top: 10px;
	display: inline-block;
    }

}    

</style>