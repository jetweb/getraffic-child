<?php

use Getraffic\Instagram;

$instaFields = get_field('instagram', get_queried_object_id());
?>
<div class="insta-line align-center" style="display: none">

        <style>
            .insta-feed {
                overflow: hidden;
                flex-wrap: nowrap;
            }

            .insta-feed .insta-item {
                position: relative;
            }

            .insta-feed .insta-item:hover .insta-overlay {
                display: block;
            }

            .insta-feed .insta-item img.thumb {
                width: 100%;
            }

            .insta-feed .insta-item .insta-overlay {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-image: url(/wp-content/themes/getraffic-child/assets/images/insta.png);
                background-repeat: no-repeat;
                background-position: center center;
                background-color: rgb(0, 0, 0, 0.5);
                display: none;
            }

            @media screen and (max-width: 768px) {
                .insta-feed {
                    flex-wrap: wrap;
                }
            }
        </style>
</div>


<div class="insta-line align-center">
    <div class="insta-text">
        <h1 class="sh-title"><?php echo $instaFields['title'] ?></h1>
        <h2 class="sh-subtitle small"><?php echo $instaFields['subtitle'] ?></h2>
    </div>
    <div class="insta-feed-container">
        <?php if (!empty($instaFields['insta_user'])) {
            Instagram::printFeed($instaFields['insta_user']);
        }
        ?>
    </div>
    <div class="mobile-text">
        <?php echo $instaFields['subtitle'] ?>
    </div>
</div>


<style>
    .insta-line .mobile-text {
        display: none;
    }

    .insta-line {
        background-color: #ffffff;
        padding: 88px 0;
        margin: 40px 0 40px 80px;
        background-image: url(<?php echo img('insta_bg.png'); ?>);
        background-repeat: no-repeat;
        background-position: left center;
        padding-left: 450px;
        background-size: 450px;
        position: relative;
    }

    .insta-feed .insta-item {
        margin: 0 0 0 0.4%;
    }

    .insta-text h1 {
        direction: ltr;
        margin: 0;
        font-size: 45px;
    }

    .insta-text {
        font-size: 30px;
        position: absolute;
        left: 57px;
        width: 370px;
        top: 9.5vw;
    }

    .insta-feed-container {
        position: relative;
    }

    .insta-feed-container .container-fluid {
        width: calc(100% - 100px);
    }

    .insta-feed-container .slick-arrow {
        z-index: 22;
    }

    @media screen and (max-width: 1200px) {
        .insta-line {
            padding-left: 0;
            background-image: url(<?php echo img('insta_mobile.png'); ?>);
            background-repeat: no-repeat;
            background-position: center top;
            -webkit-background-size: 95%;
            background-size: 95%;
        }

        .insta-line .mobile-text {
            background-image: url(<?php echo img('dots.png'); ?>);
            display: block;
            background-repeat: no-repeat;
            background-position: left center;
            padding: 35px 80px;
        }

        .insta-text {
            position: static;
            padding-left: 0;
            width: auto;
            display: none;
        }

        .insta-item {
            margin-left: 15px;
        }
    }

    @media screen and (max-width: 768px) {
        .insta-line {
            padding: 76px 0 0 0;
            margin: 40px auto 0;
        }

        .insta-line .mobile-text {
            background-image: url(<?php echo img('dots.png'); ?>);
            display: block;
            background-repeat: no-repeat;
            background-position: 2% center;
            background-size: 5%;
            font-size: 13px;
        }

        .insta-line .insta-container .slick-slider {
            margin-bottom: -6px;
        }
    }
</style>
<script defer>
    jQuery(document).ready(function ($) {
        initSlickCarousel($('.insta-feed'), true, false, 7, [
            {
                breakpoint: 3220,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 2000,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 100,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
        ]);
    });
</script>

<div class="only-mobile pink-banners">
    <div class="container">
        <?php
        $banners = get_field('p_b', get_queried_object_id());
        foreach ($banners as $banner) {
            ?>
            <div class="pink-banner">
                <img src="<?php echo $banner['image']; ?>" alt="">
                <div class="text">
                    <?php echo $banner['text']; ?>
                </div>
            </div>
            <?php
        } ?>
    </div>

</div>
