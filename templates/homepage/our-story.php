<?php $settings = get_field('our_story', get_queried_object_id()); ?>
<div class="our-story">
    <div class="container">
        <?php echo $settings['content']; ?>
    </div>
</div>

<style>
    .our-story {
        background-image: url('<?php echo $settings['bg_image']; ?>');
        padding: 130px 0;
    }
    .our-story .container {
        width: 80%;
        border: 1px solid white;
        color: white;
        text-align: center;
        padding: 50px 170px;
    }
</style>