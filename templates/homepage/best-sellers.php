<?php $bestSellers = gt_get_field('homepage_best_sellers'); ?>

<div class="container best-sellers-container">

    <?php

    $products = [];

    foreach ($bestSellers['best_sellers'] as $post) {

		$p = wc_get_product($post->ID);

		if ($p->status=='publish') $products[] = $p;

    }



	GT::renderCrossSellProducts(

        $bestSellers['title'],

        $bestSellers['sub_title'],

        $products

    );

    ?>

</div>



<style>

    .best-sellers-container h1, .best-sellers-container h2 {

        text-align: center;

    }

    @media screen and (max-width: 768px) {

        .best-sellers-container .xsell-container{

            margin: 0px auto;

        }

        .best-sellers-container .xsell-container h1.best-match{

            margin: 0px auto;

            font-size: 32px;

            letter-spacing: 1px;

            margin-bottom: 5px;

            margin-top: -13px;

        }

        .best-sellers-container .xsell-container h2.best-match {

            font-size: 14px;

            font-weight: 600;

	     margin-bottom: 20px;
        }

}    

</style>