<div class="post-menu">
    <?php wp_nav_menu([
        'menu' => 'post-menu',
        'menu_class'     => 'nav nav-right',
        'walker'         => new CustomMenuWalker(),
        'fallback_cb'    => false
    ]); ?>
</div>