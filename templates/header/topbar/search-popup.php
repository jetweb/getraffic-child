<div class="search-popup-wrap">
    <div class="search-popup">
        <div class="search-input">
            <input type="text" id="seacrhTerm" value="" class="search-value" title="search" placeholder="חפשי לפי סוג (לדוגמה: מגפונים, נעלי עקב, נעלי בובה), צבע או שם דגם"/>
			<img src="/wp-content/themes/getraffic-child/assets/images/search-icon.png" class="search-icon"/>
			<img src="/wp-content/uploads/2020/02/ajax-loader.gif" class="searching-loader"/>
        </div>
        <div class="results-popup-wrapper">
            <div class="results-popup">
                <!-- //fatch data from search.js line:22 -->
            </div>
        </div>
        <div class="x-wrapper">
             <div class="ws-x-btn"></div>
        </div>
      
    </div>
</div>

<?php GT::enqueueAsset('search-popup-css', '/assets/src/scss/search-popup.css', []); ?>

<style>
.searching-loader {
	display:none;
    float: left;
    left: 63px;
    position: absolute;
    top: 31px;
}
</style>