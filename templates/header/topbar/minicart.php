<div class="topicon minicart">
    <a class="mini-cart-toggle">
        <img src="<?php echo img('cart.png'); ?>"/>
        <span class="mini-count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
    </a>
    <div class="mini-cart-wrapper">
        <div class="widget_shopping_cart_content">
            <?php woocommerce_mini_cart(); ?>
        </div>
    </div>
</div>
