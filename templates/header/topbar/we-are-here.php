<div class="topicon location">

    <a class="location" href="<?php echo gt_get_field('location_href', 'option'); ?> ">

        <img src="<?php echo img('location.png'); ?>" alt=""/>סניפים

    </a>

</div>

<div class="topicon we-are-here">

    <div class="we-are-here-box" onclick="$(this).toggleClass('active');">

        <img src="<?php echo img('phone.png'); ?>" alt=""/>

        אנחנו כאן בשבילך

        <div class="we-are-here-drop">

            <img src="<?php echo img('sep.png'); ?>" class="separator" alt="">

            <?php

                $items = gt_get_field('we_are_here', 'option');

                foreach ($items as $item) {

					$nofollow=$item['nofollow'];

                    ?>

                    <a class="we-are-here-item" href="<?php echo $item['link']; ?>" target="_blank" <?php if ($nofollow[0]==1) echo 'rel="nofollow"'; ?>>

                        <img src="<?php echo $item['icon']; ?>" alt="" class="icon">

                        <?php echo $item['text']; ?>

                    </a>

                <?php }

            ?>

        </div>

    </div>

</div>



<style>



    .we-are-here-box {

        outline: 1px solid black;

        outline-offset: -1px;

    padding: 5px 5px 4px 55px;

    /* padding: 5px 5px 5px 25px; */

    background-position: 10px center;

    background-repeat: no-repeat;

    font-size: 14px;

    /* font-size: 14px; */

        background-image: url('<?php echo img('arrow_black_down.png'); ?>');

        position: relative;

        cursor: pointer;

        letter-spacing: 1px;

    }



    .we-are-here-box.active {

        background-image: url('<?php echo img('arrow_black_up.png'); ?>');

    }



    .we-are-here-drop {

        position: absolute;

        padding: 0;

        margin: 0;

        top: 31px;

        width: 100%;

        left: 0;

        background-color: #f3f3f3;

        z-index: 9999;

        display: none;

    }



    .we-are-here-box.active .we-are-here-drop {

        display: block;

        padding: 10px 2px 15px 0;

    }



    .we-are-here-drop img.separator {

        width: 100%;

        top: 0;

        position: absolute;

        left: 0;

    }



    a.we-are-here-item {

        display: block;

        text-align: right;

        /* padding-right: 25px; */

        font-size: 15px;

        padding: 8px 5px 0 0;

        letter-spacing: 0px;

    }

    a.we-are-here-item:hover {

        font-weight: 900;

    }

    @media screen and (max-width: 768px) {

    .we-are-here-box {

        padding: 3px 7px 4px 30px;

        font-size: 12px;

        font-weight: bold;

        background-size: 9%;

	height: 26px;

    }

        a.we-are-here-item {

        display: block;

        text-align: right;

        /* padding-right: 25px; */

        font-size: 11px;

        padding: 7px 5px 0 0;

    }

        .we-are-here-drop {

            

    }

        .we-are-here-box.active .we-are-here-drop {

        top: 26px;

            padding: 10px 2px 10px;

    }

	div.stickyWeAreHere .we-are-here-box {padding: 3px 12px 7px 35px;}

    }

</style>