<?php
global $wp_query;
$object = get_queried_object();
//print_r($object);
$slider_transition_time = extractFields('slider_transition_time', $object);
$auto_transition = extractFields('auto_transition', $object);

$bgImage = extractFields('main_bg_image', $object);
$search_bg_image = gt_get_field('search_results_background', 'option');
if (is_search()) {
    $bgImage['bg_image']['url'] = $search_bg_image;
    $bgImage['bg_image_mobile']['url'] = $search_bg_image;
}
$bgMobile = $bgImage['bg_image_mobile']['url'];
if (empty($bgMobile)) {
    $bgMobile = $bgImage['bg_image']['url'];
}
if ($bgImage['bg_image'] && !is_product() && !is_product_category() && !isset($_GET['s'])) {
    ?>
    <div class="bg-image" data-bg="url(<?php echo $bgImage['bg_image']['url']; ?>)"
         data-bgmobile="url(<?php echo $bgMobile; ?>)">
        <div class="container align-center">
            <div class="align-center bg-container">
                <h1 class="bold">
                    <?php echo $bgImage['title']; ?>
                </h1>
                <h2 class="bold-text"><?php echo $bgImage['desc']; ?></h2>
            </div>
        </div>
    </div>
    <?php
}
if ($bgImage['bg_image'] && !is_product() && is_product_category()) {
    $cat_id = $wp_query->get_queried_object_id();
    $cat_desc = term_description($cat_id, 'product_cat');
    $term = get_term_by('id', $cat_id, 'product_cat', 'ARRAY_A');

    ?>
    <div class="bg-image" data-bg="url(<?php echo $bgImage['bg_image']['url']; ?>)"
         data-bgmobile="url(<?php echo $bgMobile; ?>)">
        <div class="container align-center">
            <div class="align-center bg-container">
                <h1 class="bold">
                    <?php echo $term['name']; ?>
                </h1>
                <h2 class="bold-text"><?php echo $cat_desc; ?></h2>
            </div>
        </div>
    </div>
    <?php
} ?>

<div class="slider-wrapper">
    <ul class="slides"
        data-auto-transition="<?php echo $auto_transition; ?>"
        data-transition-time="<?php echo $slider_transition_time; ?>">
        <?php
        if ($slides = extractFields('header_slider', $object)) : ?>
            <?php
            $img_attributes = array(
                'alt'   => '',
                'class' =>  'bg'
            );
            foreach ($slides as $slide) :
                $bgImageUrl = $slide['bg_image']['url'];
                $bgImageMobileUrl = $slide['bg_image_for_mobile']['url'];
                $bgImageAlt = $slide['bg_image']['alt'];
                $mainImage = $slide['main_image']['url'];
                $mainImageAlt = $slide['main_image']['alt'];
                $title = $slide['title'];
                $buttonText = $slide['button_text'];
                $buttonLink = $slide['button_link'];
                $bgColor = $slide['bg_color'];
                $fullWidth = $slide['full_width'];
                $accessText = $slide['access_text'];
                ?>
                <li>
                    <a href="<?php echo $buttonLink; ?>">

                        <?php if ( false ) : ?>
                            <?php if ($bgImageUrl) { ?>
                                <img src="<?php echo $bgImageUrl ?>" class="bg" alt="<?php echo $bgImageAlt ?>"/>
                            <?php } ?>

                            <?php if ($bgImageMobileUrl) { ?>
                                <img src="<?php echo $bgImageMobileUrl ?>" class="bg-mobile" alt="<?php echo $bgImageAlt ?>"/>
                            <?php } else { ?>
                                <img src="<?php echo $bgImageUrl ?>" class="bg-mobile" alt="<?php echo $bgImageAlt ?>"/>
                            <?php } ?>
                        <?php endif; ?>
                        <?php
                        $img_attributes['alt'] = $bgImageAlt;
                        bunique_get_picture_tag( $bgImageUrl , [
                            '(min-width:768px)'                     =>  $bgImageUrl,
                            '(min-width:0px) and (max-width:767px)' =>  ( ! empty( $bgImageMobileUrl ) ? $bgImageMobileUrl : $bgImageUrl )
                        ], $img_attributes ); ?>
                        <?php if ($accessText) { ?>
                            <span style="padding-right: 60px" class="access-text"><?= $accessText; ?></span>
                        <?php } ?>
                        <?php if ( false ) : // This part was checked on the 2020-05-26 and not printed to the DOM ?>
                            <div class="overlay" style="background-color: <?php echo $bgColor; ?>">
                                <img src="<?php echo $mainImage ?>"
                                     alt="<?php echo $mainImageAlt ?>"
                                     style="max-width: 100%;<?php echo $fullWidth ? 'width:100%;' : ''; ?>">

                                <h2><?php echo $title; ?></h2>

                                <?php if ($buttonText) { ?>
                                    <a class="tony-button sharp-corners wide"
                                       href="<?php echo $buttonLink; ?>">
                                        <?php echo $buttonText; ?>
                                    </a>
                                <?php } ?>
                            </div>
                        <?php endif; ?>
                    </a>
                </li><?php
            endforeach;
        endif; ?>
    </ul>
</div>
<style>
    ul.slick-dots {
        bottom: 20px;
    }
</style>
