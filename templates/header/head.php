<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Yaniv Tabibi">

    <?php if( false ) : ?>
        <link href="https://fonts.googleapis.com/css?family=Heebo:300,400,500,700|Varela+Round&amp;subset=hebrew" rel="stylesheet">
    <?php endif; ?>

    <title><?php wp_title('');?></title>
    <!-- Hotjar Tracking Code for www.b-unique.co.il -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:500130,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <?php wp_head(); ?>
    <style>
        /*Menu*/
        ul.sub-menu .container .submenu-wrapper {
            width: 78% !important;
            padding: 0 !important;
            height: auto !important;
            flex-flow: row !important;
        }
        ul.sub-menu {
            /*right: inherit !important;*/
            /*left: inherit !important;*/
            width: 100% !important;
            padding: 20px !important;
        }
        .mega-banners {
            display: none !important;
        }

        ul.nav li ul.sub-menu li {
            width: auto !important;
            display: inline-block;
            margin-left: 35px;
            margin-top: 10px;
        }
        ul.nav li {
            margin: 0;
        }
        ul.nav li a {
            padding: 10px 30px 10px 30px
        }
    </style>
</head>
