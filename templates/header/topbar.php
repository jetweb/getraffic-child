<?php if (get_field('topbar-checkbox', 'option')){ ?>
    <div class="top-stripe">    
        <a style="color:<?php echo get_field('font_color', 'option'); ?>; font-family:<?php echo get_field('font_family', 'option'); ?>; font-size:<?php echo get_field('font-size', 'option'); ?>px;" href="<?php echo get_field('top_stripe_text_link', 'option'); ?>"><?php echo get_field('top_stripe_text', 'option'); ?></a>
    </div>
<?php } ?>
<div class="topbar">
    <div class="container align-center padmenot topicons-container">
        <div class="topicons-right">
            <?php get_template_part('templates/header/topbar/account'); ?>
            <?php get_template_part('templates/header/topbar/minicart'); ?>
            <?php get_template_part('templates/header/topbar/wishlist'); ?>
            <?php get_template_part('templates/header/topbar/search'); ?>
        </div>
        <?php get_template_part('templates/header/topbar/logo'); ?>
        <div class="topicons-left">
            <?php get_template_part('templates/header/topbar/we-are-here'); ?>
        </div>
    </div>
</div>
<?php get_template_part('templates/header/topbar/search-popup'); ?>