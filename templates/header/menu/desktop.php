<div class="menu-right">

    <?php wp_nav_menu([

        'theme_location' => 'header-menu-right',

        'menu_class'     => 'nav nav-right',

        'walker'         => new CustomMenuWalker(),

        'fallback_cb'    => false

    ]); ?>

</div>

<!--div class="menu-left">

    <?php wp_nav_menu([

        'theme_location' => 'header-menu-left',

        'menu_class'     => 'nav nav-left',

        'walker'         => new CustomMenuWalker(),

        'fallback_cb'    => false

    ]); ?>

</div--!>