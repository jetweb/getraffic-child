<div class="sticky-mobile">

    <div class="menubar-mobile">

        <?php get_template_part('templates/header/topbar/logo-mobile'); ?>

        <div class="hamburger hamburger--collapse">

            <div class="hamburger-box">

                <div class="hamburger-inner"></div>

            </div>

        </div>

    </div>

    <div class="searchbox">

        <input type="text" class="search-value" placeholder="חיפוש">

    </div>    

</div>

<div id="menu-mobile">



    <div class="mobile-login-link">

        <a rel="nofollow" href="<?php
        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        echo get_site_url() . '/my-account?return=' . $actual_link ?>">התחברות</a> \

        <a href="<?php echo get_site_url() . '/registration' ?>">הרשמה</a>

    </div>

    <div class="mobile-menu-back-button">

        <a href="" class="to-all">



        </a>

        <a href="" class="back">

            <img src="<?php echo img('arr_left.png'); ?>" alt="">

        </a>

        <div class="clearfix"></div>

    </div>

    <ul>

        <?php //listMenuForMobile('header-menu-mobile'); ?>

        <?php listMenuForMobile('header-menu-left'); ?>

    </ul>



    <div class="mobile-menu-icons">

        <?php

        $icons = gt_get_field('mobile_menu_icons', 'option');

        foreach ($icons as $icon) {

            ?>

            <a href="<?php echo $icon['link']; ?>">

                <img src="<?php echo $icon['icon']; ?>" alt="">

            </a>

            <?php

        }

        ?>

    </div>



    <?php listMobileBottomMenu(); ?>

    <?php

    /**

     * Recursive function to print mobile array

     *

     * @param $item

     * @param $map

     */

    function printMenuItem($item, $map, $parentId)

    {

        $itemIcon       = gt_get_field('menu_item_icon', $item);

        $itemPluralName = gt_get_field('plural_item_name', $item);

        $mobileItemClass = str_replace(' ', '_', $item->title);



        if (!empty($map[$item->ID])) {

            echo '<li data-item-id="' . $item->ID . '" onclick="openSubMenuForParent(' . $item->ID . ', \'' . (empty($itemPluralName) ? $item->title : $itemPluralName) . '\', \'' . $item->url . '\');">';

        } else {

            echo '<li data-item-id="' . $item->ID . '" class="' . $mobileItemClass .'" onclick="location.href=\'' . $item->url . '\';">';

        }

        if ($itemIcon) {

            echo '<img alt="" src="' . $itemIcon . '" />';

        }

        $linq = empty($map[$item->ID]) ? ('href="' . $item->url . '"') : '';

        echo '<a ' . $linq . '>' . $item->title . '</a>';

        echo '</li>';



        if (!empty($map[$item->ID])) {

            echo '<ul class="submenu" data-item-id="' . $item->ID . '" data-parent-id="' . $parentId . '">';

            foreach ($map[$item->ID] as $subItem) {

                printMenuItem($subItem, $map, $item->ID);

            }

            echo '</ul>';

        }

    }



    function listMenuForMobile($locationName)

    {

        $locations = get_nav_menu_locations();

        $items     = wp_get_nav_menu_items($locations[$locationName]);

        $menuArray = [];

        if (empty($items)) {

            return;

        }

        // Sort the items as an associative array of parent_id->children, to be used as a map for recursive printing

        foreach ($items as $item) {

            $menuArray[$item->menu_item_parent][] = $item;

        }

        foreach ($menuArray[0] as $topLevelItem) {

            printMenuItem($topLevelItem, $menuArray, -1);

        }

    }



    function listMobileBottomMenu()

    {

        $locations = get_nav_menu_locations();

        $items     = wp_get_nav_menu_items($locations['mobile-menu-bottom']);



        if (empty($items)) {

            return;

        }

        // Sort the items as an associative array of parent_id->children, to be used as a map for recursive printing

        echo '<ul class="mobile-bottom-menu">';

        foreach ($items as $item) {

            ?>

            <li>

                <a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>

            </li>

            <?php

        }

        echo '</ul>';

    }



    ?>

</div>





<script type="text/javascript">

    var objToStick = jQuery(".sticky-mobile");

    var stickyMinicart = jQuery(".topicon.minicart");

    var stickyWishlisticon = jQuery(".topicon.wishlisticon");

    var stickyWeAreHere = jQuery(".topicon.we-are-here");

    var topOfObjToStick = jQuery(objToStick).offset().top;



    jQuery(window).scroll(function ($) {

        var windowScroll = jQuery(window).scrollTop();

    

        if (windowScroll > topOfObjToStick) {

            jQuery(objToStick).addClass("topWindow");

            jQuery(stickyWeAreHere).addClass("stickyWeAreHere");

            jQuery(stickyMinicart).addClass("stickyMinicart");

            jQuery(stickyWishlisticon).addClass("stickyWishlisticon");

        } else {

            jQuery(objToStick).removeClass("topWindow");

            jQuery(stickyWeAreHere).removeClass("stickyWeAreHere");

            jQuery(stickyMinicart).removeClass("stickyMinicart");

            jQuery(stickyWishlisticon).removeClass("stickyWishlisticon");

        };        

    });

</script>

<style>

    @media screen and (max-width: 768px){

        .topWindow, .stickyMinicart, .stickyWishlisticon, .stickyWeAreHere{

            position: fixed !important;

        }

        .topWindow, .stickyMinicart, .stickyWishlisticon{

            top: 0;

        }

        .topWindow {

            z-index:8;

            width: 100%;

            background: #fff;

            border-bottom: 1px solid #ccc;

        }

        .stickyMinicart {

            left: 15px;

        }

        .stickyWishlisticon {

            left: 35px;

        }

        .stickyWeAreHere {

            top: 43px;

            width: auto !important;   

        }

        .stickyWeAreHere .we-are-here-box{

            padding: 6px 20px 7px 52px;

        }

        .topicons-left.only-mobile{

            left: 0;

        }

    }

</style>

