<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>
<div class="blog-articles" id="blog-primary">    
    <?php
        $allposts = get_posts();
        foreach( $allposts as $post ){ setup_postdata($post); ?> 
        <div class="content">
            <div class="blog-image"><?php echo get_the_post_thumbnail( $id, 'large' ); ?>
                <div class="blog-date"><?php echo get_the_date('j.n.Y'); ?></div>
            </div>                
            <h2 class="blog-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <div class="blog-excerpt"><?php the_excerpt(); ?></div>
        </div>
        <?php }
        wp_reset_postdata();
    ?>   
</div>
