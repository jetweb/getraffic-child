<?php get_template_part('templates/popups/password_reset'); ?>

<div class="blockscreen"></div>
<div class="login-popup">
    
    <?php if (!is_user_logged_in()) { ?>
    
        <h2 class="account">התחברות</h2>
        <div class="padded">
            <input id="loginUser" type="text" name="username" placeholder="שם משתמש"/>
            <input id="loginId" type="text" name="userId" placeholder=".טלפון / ת.ז"/>
            <div class="checkout-password">
            <input id="loginPass" type="password" name="pass" placeholder="סיסמא"/>
            <img class="closed-eye" src="<?php echo img('closed-eye.png'); ?>"><img class="opened-eye" src="<?php echo img('opened-eye.png'); ?>">
            <a class="forgot-pass" href="#" data-toggle="modal" data-target="#resetPassword">איפוס סיסמא</a>
            </div>
            <div class="login-error login-message"></div>
            <div class="login-success login-message"></div>
            <a href="" class="tony-button sharp-corners login-popup-btn">
                כניסה
            </a>  
            <a class="checkout-club" href="<?php get_site_url(); ?>/club/">מועדון לקוחות?</a>
            <?php wp_nonce_field('ajax-login-nonce', 'security'); ?>            
        </div>
    
    <?php }  ?>
</div>

<script defer>
    jQuery(document).ready(function ($) {
        $('#loginUser, #loginPass').keypress(function(e) {
            if ( e.keyCode === 13 ) {
                e.preventDefault();
                triggerLogin();
                return false;
            }
        });
        
        $('.padded img').click(function(){
            
            if($(this).prevAll("input#loginPass").attr('type') == 'text'){
                $(this).prevAll("input#loginPass").attr('type', 'password');
                $(this).css('display', 'none');
                $(this).prev().css('display', 'block');
               }
            else{
               $(this).prevAll("input#loginPass").attr('type', 'text');
               $(this).css('display', 'none');
               $(this).next().css('display', 'block');
                
            }
        });
        
        $('.login-popup-trigger').click(function() {
            $('.login-popup').toggle();
            $('.topicon.login').toggleClass('active');
            $('.blockscreen').css('display', 'block');
            return false;
        });

        $('a.login-popup-btn').click(function(e) {
            e.preventDefault();
            triggerLogin();
            return false;
        });

        $(document.body).on('login_success', function(e, message) {
            $('.login-success').text(message);
        });

        $(document.body).on('login_error', function(e, message) {
            $('.login-error').text(message);
        });
    });

    function triggerLogin() {
        $('.login-message').text('');
        logUserIn(
            $('#loginUser').val(),
            $('#loginPass').val(),
            $('#security').val(),
            true
        );
    }
</script>
