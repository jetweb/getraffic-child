
<div class="shop_table woocommerce-checkout-review-order-table">

    <?php
    do_action( 'woocommerce_review_order_before_cart_contents' );

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
            $thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image('checkout-thumbnails'), $cart_item, $cart_item_key );
            ?>
            <div class="checkout-summary-cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                <div class="product-name">
                    <?php echo $thumbnail; ?>
                    <?php
                    $textQuantity = 'כמות';
                    echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                        '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><img src="'. img('del.png') .'" /></a>',
                        esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                        __( 'Remove this item', 'woocommerce' ),
                        esc_attr( $_product->get_id() ),
                        esc_attr( $cart_item_key ),
                        esc_attr( $_product->get_sku() )
                    ), $cart_item_key );
                    ?>
                </div>
                <div class="product-total">
                    <div class="checkout-product-details">
                        <div class="checkout-product-details"><?php echo $_product->get_title(); ?></div>
                        <div class="checkout-product-price">
                            <?php
                            $discount = '';
                            $_parent_product = wc_get_product($_product->get_parent_id());
                            if ($_product->is_on_sale() || ( $_parent_product && $_parent_product->is_on_sale()) ) {
                                $currency_symbol = get_woocommerce_currency_symbol(get_woocommerce_currency());
                                echo '<span class="checkout-regular-price">' . $currency_symbol . number_format((float)($_product->get_regular_price() * $cart_item['quantity']), 2, '.', '') . '</span>';
                                $discount = 'discount';
                            }
                            ?>
                            <?php echo '<span class="checkout-price ' . $discount . '">' . apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ) . '</span>'; ?>
                        </div>
                    </div>
                    <div class="checkout-product-variations">
                        <?php
                            $values = GT::getRealAttributeValues($_product);
                            foreach ($values as $term) {
                                echo '<span>' . $term->realAttributeName . ': ' . urldecode($term->name) . '</span>&nbsp;|&nbsp;';
                            }
                        ?>
                        <span class="item-q"><?=$textQuantity?>: <?=$cart_item['quantity']?></span>
                    </div>
                </div>
            </div>
            <?php
        }
    }

    do_action( 'woocommerce_review_order_after_cart_contents' );
    ?>
</div>
