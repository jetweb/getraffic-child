<div class="checkout-shipping">
    <?php if (WC()->cart->needs_shipping() && WC()->cart->show_shipping()) : ?>
        <?php do_action('woocommerce_cart_totals_before_shipping'); ?>
        <?php wc_cart_totals_shipping_html(); ?>
        <?php do_action('woocommerce_cart_totals_after_shipping'); ?>
    <?php elseif (WC()->cart->needs_shipping() && 'yes' === get_option('woocommerce_enable_shipping_calc')) : ?>
        <tr class="shipping">
            <th><?php _e('Shipping', 'woocommerce'); ?></th>
            <td data-title="<?php esc_attr_e('Shipping', 'woocommerce'); ?>"><?php woocommerce_shipping_calculator(); ?></td>
        </tr>
    <?php endif; ?>
    <div class="woocommerce-shipping-fields checkout-shipping-fields">
        <div class="shipping-fields-container">
            <?php
            /** @global WC_Checkout $checkout */
            $checkout = new WC_Checkout();
            $fields   = $checkout->get_checkout_fields('shipping');
            foreach ($fields as $key => $field) {
                woocommerce_form_field($key, $field, $checkout->get_value($key));
            }
            ?>
            <input type="hidden" name="ship_to_different_address" value="1">
        </div>
    </div>


</div>
