<div class="checkout-points">
    <p>מועדון החברות של TONYMOLY</p>
    <div class="flex">

        <?php if (is_user_logged_in()) { ?>
            <div class="coupon-input">
            <span class="points">יש לך
                <?php echo strip_tags(do_shortcode('[rs_my_reward_points] נקודות למימוש ')); ?>
            </span>
                <button class="tony-button sharp-corners pink apply-coupon" name="apply_coupon" value="ממשי">מימוש נקודות</button>
                <input type="hidden" id="total-points" value="<?php echo strip_tags(do_shortcode('[rs_my_reward_points]')); ?>">
            </div>
        <?php } ?>

        <div class="points-collected">
            בקנייה זו תצברי <?php echo GT::getPointsToEarnInCart(); ?> נקודות לרכישה הבאה!
        </div>
    </div>
</div>

<style>
    .checkout-points {
        display: <?php echo GT::isUserInClub() ? 'block' : 'none' ?>;
    }
</style>
