<?php GT::enqueueAsset('checkout-js', '/assets/src/js/checkout.js', [], true, true); ?>
<?php get_template_part('templates/checkout/authentication'); ?>
<div id="nav-billing" class="checkout-wizard-step">
    <h2 class="wizard-title" onclick="showStep('billing');">1. פרטי לקוח</h2>
    <div class="wizard-item-content">
        <div class="checkout-message">המייל שלך ישמש כדי לשלוח לך קבלה על הקנייה ומידע על ההזמנה</div>

        <div class="checkout-billing-form" id="customer_details">
            <?php do_action('woocommerce_checkout_billing'); ?>
        </div>

        <div class="full-width checkout-button-wrapper">
            <a class="checkout-button" onclick="proceedToCoupons();">
                לשלב הבא
            </a>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div id="nav-coupons" class="checkout-wizard-step">
    <h2 class="wizard-title" onclick="proceedToCoupons();">2. קופונים והטבות</h2>
    <div class="wizard-item-content">
        <?php
        get_template_part('templates/checkout/coupons');
        ?>
        <div class="full-width checkout-button-wrapper">
            <a class="checkout-button" onclick="proceedToDelivery();">
                לשלב הבא: אפשרויות משלוח
            </a>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div id="nav-delivery" class="checkout-wizard-step">
    <h2 class="wizard-title" onclick="proceedToDelivery()">3. שיטת משלוח</h2>
    <div class="wizard-item-content">
        <?php get_template_part('templates/checkout/shipping'); ?>

        <div class="full-width checkout-button-wrapper">
            <a class="checkout-button" onclick="proceedToLastStep();">
                לשלב הבא: תשלום מאובטח
            </a>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div id="nav-payment" class="checkout-wizard-step"  onclick="proceedToLastStep();">
    <h2 class="wizard-title">4. התשלום</h2>
    <div class="wizard-item-content">       

        <?php get_template_part('templates/checkout/payment-buttons'); ?>
        <a class="submit-checkout checkout-button" onclick="jQuery('form.checkout.woocommerce-checkout').submit();">
            המשיכי לתשלום מאובטח
        </a>
    </div>
</div>
<div class="clearfix"></div>

<script>
    let isUserLoggedIn = <?php echo is_user_logged_in() ? 'true' : 'false'; ?>;
    let isUserInClub = <?php echo GT::isUserInClub() ? 'true' : 'false' ?>;
    let enableJoinPopup = <?php echo gt_get_field('points_and_club_settings', 'option')['allow_join_club'] ? 'true' : 'false' ?>;
    let autoJoinClub = <?php echo gt_get_field('points_and_club_settings', 'option')['auto_join_club'] ? 'true' : 'false' ?>;
    let firstDelivery = true;

    function proceedToCoupons() {
        if (currentStep === 'billing') {
            if ($('select.pickup_location').val() === '') {
                $('select.pickup_location').val(0).trigger('change');
            }
            validateCheckout();
        } else {
            showStep('coupons');
        }
    }

    function proceedToDelivery() {
        if (currentStep === 'billing') {
            return;
        }
        if (firstDelivery) {
            $('select.pickup_location').val('').trigger('change');
        }
        showStep('delivery');
    }

    function proceedToLastStep() {
        let valid = true;

        $('span.no-pickup-selected').remove();

        $('.shipping-fields-container .form-row.validate-required').each(function() {
            if (!validateFormElement($(this).find('input'))) {
                valid = false;
            }
        });

        if ($('select.pickup_location').val() === '') {
            valid = false;
            $('select.pickup_location').parent().append('<span class="no-pickup-selected" style="color:red;">יש לבחור סניף לאיסוף מתוך הרשימה</span>');
        }

        if (valid) {
            showStep('payment');
        }
    }

    jQuery( document.body ).on( 'updated_checkout', function(e, data) {
        //debugger;
        //$('label[for=shipping_method_0_free_shipping7]').parent().prependTo('ul#shipping_method');

        $('span.select2-container').click(function() {
            firstDelivery = false;
        });
    });

    jQuery(document).ready(function($) {
        let checkboxField = $('p#billing_checkbox_field');
        checkboxField.remove();
        checkboxField.find("input").attr("checked", "checked");
        checkboxField.insertAfter($('p#order_comments_field'));
    });

</script>

<?php get_template_part('templates/popups/join-us'); ?>


