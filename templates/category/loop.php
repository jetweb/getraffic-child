<?php
do_action( 'woocommerce_before_shop_loop' );
$fields = gt_get_field('category_page_settings', 'option');

if (isset($_GET['s'])) {
    $productID = wc_get_product_id_by_sku($_GET['s']);
    if ($productID) {
        echo "<div class='products' data-page='1'>";
        GT::renderProduct(wc_get_product($productID), true, 'thumbnail', $fields['spread_variations']);
        echo "</div>";
        return;
    }
}

function getQueriedVariation($product, $spreadVariations)
{
    $filterBySize  = false;
    $filterByColor = false;
    if (isset($_GET['filter_size']) && !empty($_GET['filter_size']) && ($product instanceof WC_Product_Variable)) {
        $filterBySize = true;
    }
    if (isset($_GET['filter_color']) && !empty($_GET['filter_color']) && ($product instanceof WC_Product_Variable)) {
        $filterByColor = true;
    }
    if ($filterBySize && $filterByColor) {
        $spreadVariations = false;
        $attrs = $product->get_attributes();
        if ($attrs['pa_color'] && $attrs['pa_color']['variation']) {
            $lastMatched = null;
            foreach (explode(',', $_GET['filter_color']) as $color) {
                foreach ($product->get_available_variations() as $variation) {
                    if ($color == urldecode($variation['attributes']['attribute_pa_color'])) {
                        foreach (explode(',', $_GET['filter_size']) as $size) {
                            if ($size == urldecode($variation['attributes']['attribute_pa_size'])) {
                                $lastMatched = wc_get_product($variation['variation_id']);
                                // Don't render if not in stock size+color variation
                                if (!$lastMatched->is_in_stock()) {
                                    return null;
                                }
                            }
                        }
                    }
                }
            }
            if ($lastMatched) {
                GT::renderProduct($lastMatched, true, 'thumbnail', $spreadVariations);
            }
        } else {
            GT::renderProduct($product, true, 'thumbnail', $spreadVariations);
        }
    } else if ($filterByColor) {
        $spreadVariations = false;
        $attrs = $product->get_attributes();
        if ($attrs['pa_color'] && $attrs['pa_color']['variation']) {
            foreach (explode(',', $_GET['filter_color']) as $color) {
                foreach ($product->get_available_variations() as $variation) {
                    if ($color == urldecode($variation['attributes']['attribute_pa_color'])) {
                        $productToRender = wc_get_product($variation['variation_id']);
                        GT::renderProduct($productToRender, true, 'thumbnail', $spreadVariations);
                        break;
                    }
                }
            }
        } else {
            GT::renderProduct($product, true, 'thumbnail', $spreadVariations);
        }
    } else if ($filterBySize) {
        $attrs = $product->get_attributes();
        if ($attrs['pa_size'] && $attrs['pa_size']['variation']) {
            $lastMatched = null;
            foreach (explode(',', $_GET['filter_size']) as $size) {
                foreach ($product->get_available_variations() as $variation) {
                    if ($size == urldecode($variation['attributes']['attribute_pa_size'])) {
                        $lastMatched = wc_get_product($variation['variation_id']);
                        // Don't render if not in stock size+color variation
                        if ($lastMatched->is_in_stock()) {
                            GT::renderProduct($lastMatched, true, 'thumbnail', $spreadVariations);
                            //break;
                        }
                    }
                }
            }
            if ($lastMatched) {

            }
        }
    } else {
        GT::renderProduct($product, true, 'thumbnail', $spreadVariations);
    }
}

if (wc_get_loop_prop('total')) {
    echo "<div class='products' data-page='1'>";
    $productCount = 0;
    while (have_posts()) {
        the_post();
        do_action('woocommerce_shop_loop');
        global $product;
        $productToRender = $product;

        $spreadVariations = $fields['spread_variations'];

        getQueriedVariation($product, $spreadVariations);


        ++$productCount;
    }
} else {
    get_template_part('templates/category/search-empty');
}

$cat = get_queried_object();
if (is_object($cat) && isset($cat->term_id)) {
    $banners = gt_get_field('page_banners', 'product_cat_' . $cat->term_id);
    if ($banners) {
        foreach ($banners as $banner) { ?>
            <div class="archive-banner" style="
            <?php if ($productCount > 2) { ?>
                    grid-column: <?php echo $banner['from_col']; ?> / <?php echo $banner['to_col']; ?>;
                    grid-row: <?php echo $banner['from_row']; ?> / <?php echo $banner['to_row']; ?>;
            <?php } ?>
                    "
                 data-show_mobile="<?php echo $banner['show_in_mobile_devices'] ? 'block' : 'none'; ?>"
                 data-mobile-column="<?php echo $banner['from_col_mobile']; ?> / <?php echo $banner['to_col_mobile']; ?>"
                 data-original-column="<?php echo $banner['from_col']; ?> / <?php echo $banner['to_col']; ?>"
            >
                <a href="<?php echo $banner['link']; ?>"><img style="width:100%;" src="<?php echo $banner['image']; ?>"/></a>
            </div>
        <?php }
    }
}

//GT::printQuickViews();

echo "</div>";


?>
