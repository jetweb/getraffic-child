<?php
$fields = gt_get_field('category_page_settings', 'option');
if (wc_get_loop_prop('total')) {
    ?> <p class="results-found">
        <?= str_replace('[count]', wc_get_loop_prop('total'), $fields['x_results_found']); ?>
    </p> <?php
}
?>

<?php if (!empty($s)) { ?>
<form action="">
    <div class="search-result">
        <div class="search-result-wrap">
            <div class="search-result-input">
                <div class="searchIcon"></div>
                <input type="text" name="s" id="seacrhResult" value="<?= $s ?>" class="search-value" title="search" placeholder="<?=$fields['s_placeholder'];?>"/>
 			<img src="/wp-content/themes/getraffic-child/assets/images/search-icon.png" class="search-icon"/>
           </div>
        </div>
    </div>
</form>
<?php } ?>
