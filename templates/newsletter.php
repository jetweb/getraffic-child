<div class="f-newsletter">
    <h4 class="align-right">בואי נשמור על קשר</h4>
    <form class="newsletter-form" method="post" data-id="120444">
        <input type="email" id="email" class="newsletter" name="email" placeholder="כתובת המייל שלך"/><br/>
        <input type="submit" class="newsletter-submit" value="כן, עדכנו אותי!">
        <div class="newsletter-response"></div>

        <div class="mc4wp_approve">

            <label for="approve">
                <input id="approve" name="approve" class="newsletter-approve" type="checkbox">
                <span>אני מסכימה לקבל מידע וחומר שיווקי בהתאם <a href="https://www.b-unique.co.il/terms-and-conditions/" target="_blank">לתנאי השימוש באתר</a></span>
            </label>
        </div>
    </form>
</div>
