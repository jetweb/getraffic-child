<div class="summary entry-summary">
    <?php
        global $product;
    $product = get_query_var('qvProduct');
    ?>
    <?php $prod_categories = get_the_terms($product->get_id(), 'product_cat'); ?>
    <?php foreach ($prod_categories as $prod_category){ 
        $cat_link = get_term_link($prod_category->term_id);
        $category_names[] = '<a href="' . $cat_link . '">' . $prod_category->name . '</a>';
    } ?>    
    <div class="mobile-gallery position-relative"></div>    
    <div class="single-product-top">
        <?php 
            $qv_title = $product->get_title();
            ?>
        <div class="single-product-title"><h1><?php echo $qv_title; ?></h1></div>
        <div class="flex-mobile">
            
            <span class="single-product-stars"><?php GT::showProductAverageRating($product); ?></span>
        </div>        
        <?php  woocommerce_template_single_price(); ?>
    </div>
    <?php 
    $product_short_description = $product->get_short_description();
    if(!empty($product_short_description)) { ?>    
        <div class="product-description"><?php echo $product_short_description; ?></div>
    <?php } ?>   

    <div class="add-to-cart-form">
        <?php if($product->is_type( 'variable' ) && $product->is_in_stock()) { ?>    
            <div class="sizechoose"><a href="#" data-toggle="modal" data-target="#sizeChoose"><img src="<?php echo img('icon-sixe-chart.png') ?>">מדריך מידות</a></div>
        <?php } ?>
        <?php woocommerce_template_single_add_to_cart(); ?>
        <div class="variation-error"></div>
        
    </div>
    <div class="clearfix"></div>
</div>
