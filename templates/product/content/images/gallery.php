<?php
// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if (!function_exists('wc_get_gallery_image_html')) {
return;
}

global $product;

$columns           = apply_filters('woocommerce_product_thumbnails_columns', 4);
$post_thumbnail_id = $product->get_image_id();
$wrapper_classes   = apply_filters('woocommerce_single_product_image_gallery_classes', [
'woocommerce-product-gallery',
'woocommerce-product-gallery--' . (has_post_thumbnail() ? 'with-images' : 'without-images'),
'woocommerce-product-gallery--columns-' . absint($columns),
'images',
]);
?>

<div class="<?php echo esc_attr(implode(' ', array_map('sanitize_html_class', $wrapper_classes))); ?>" data-columns="<?php echo esc_attr($columns); ?>" style="opacity: 0; transition: opacity .25s ease-in-out;">
    <?php GT::renderWishlistButton($product->get_id(), img('heart_pink_full.png'), img('heart_pink.png')); ?>
    <figure class="woocommerce-product-gallery__wrapper">
        <?php
        if (has_post_thumbnail()) {
            $html = wc_get_gallery_image_html($post_thumbnail_id, true);
        } else {
            $html = '<div class="woocommerce-product-gallery__image--placeholder">';
            $html .= sprintf('<img src="%s" alt="%s" class="wp-post-image" />', esc_url(wc_placeholder_img_src()), esc_html__('Awaiting product image', 'woocommerce'));
            $html .= '</div>';
        }
        
        echo apply_filters('woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id);
        if (!empty($product->get_gallery_image_ids())) {
            ?>
            <?php woocommerce_show_product_thumbnails(); ?>
        <?php } ?>
        
    </figure>
    
</div>

<script>
    jQuery('div.woocommerce-product-gallery__image a').attr('data-gallery', 'product-gallery');
  /*  jQuery('div.woocommerce-product-gallery__image a').bind('click', function (event) {
        event.preventDefault();
        jQuery(this).ekkoLightbox({
            alwaysShowClose: true,
            rightArrow: '<img style="position: absolute;left: 20px;" src="<?php echo img('arrow_white_left.png'); ?>">',
            leftArrow: '<img style="float: right;margin-right: 20px;" src="<?php echo img('arrow_white_right.png'); ?>">',
        });
    });*/

    let amount = 0;

    function scroll() {
        jQuery('.gallery-thumbnails-wrapper').animate({
            scrollLeft: amount
        }, 100, 'linear', function () {
            if (amount != '') {
                scroll();
            }
        });
    }

    jQuery('.thumb-right').hover(function () {
        amount = '+=10';
        scroll();
    }, function () {
        amount = '';
    });
    jQuery('.thumb-left').hover(function () {
        amount = '-=10';
        scroll();
    }, function () {
        amount = '';
    });
</script>
<style>
    a.whishlist {
        position: absolute;
        top: 10px;
        left: 25px;
        z-index: 1;
    }
    img.ajax-loading {
        z-index: 4;
    }
    img.wp-smiley, img.emoji {
        display: none !important;
    }
    .yith-wcwl-add-to-wishlist {
        display: none;
    }
    .woocommerce-product-gallery__image img:first-child {
        width: 100% !important;
    }
</style>
