<?php
$title         = get_query_var('xsell_title');
$subtitle      = get_query_var('xsell_subtitle');
$xsellProducts = get_query_var('xsell_products');
$template      = get_query_var('xsell_template');
$elementID     = get_query_var('element_id');
?>
<div class="container xsell-container">
    <h2 class="best-match"><?php echo $title; ?></h2>
    <p class="best-match"><?php echo $subtitle; ?></p>
    <div class="position-relative">
        <div id="<?php echo $elementID; ?>" class="xsell-products">
            <?php
            global $product;
            foreach ($xsellProducts as $xSell) {
                GT::renderProduct($xSell, true, $template, false, true, 'carousel-thumb');
            }
            ?>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function () {
        let $ = jQuery;

        slickInitializers['<?php echo $elementID; ?>'] =
            function initSlick() {
                let slides = <?php echo count($xsellProducts); ?>;
                initSlickCarousel($('#<?php echo $elementID; ?>'), true, true, slides, [
                    {
                        breakpoint: 5220,
                        settings: {
                            slidesToShow: Math.min(4, slides),
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 1920,
                        settings: {
                            slidesToShow: Math.min(4, slides),
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 1400,
                        settings: {
                            slidesToShow: Math.min(4, slides),
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: Math.min(4, slides),
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 10,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]);
            };

        slickInitializers['<?php echo $elementID; ?>']();
    });


</script>
