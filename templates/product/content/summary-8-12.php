<div class="summary entry-summary">
    <?php global $product; ?>    
    <?php $prod_categories = get_the_terms($product->get_id(), 'product_cat'); ?>
    <?php foreach ($prod_categories as $prod_category){ $category_names[] = $prod_category->name; } ?>
    
    <div class="mobile-gallery position-relative"></div>    
    <div class="single-product-top">
        <div class="single-product-title"><?php woocommerce_template_single_title(); ?></div>
        <div class="flex-mobile">
            <span class="single-product-categories"><?php  echo implode(" | ", $category_names); ?></span>
            <span class="single-product-stars"><?php GT::showProductAverageRating($product); ?></span>
        </div>        
        <?php  woocommerce_template_single_price(); ?>
    </div>
    <?php 
    $product_short_description = $product->get_short_description();
    if(!empty($product_short_description)) { ?>    
        <div class="product-lite-text product-description"><?php echo $product_short_description; ?></div>
    <?php } ?>   

    <div class="add-to-cart-form">
        <?php woocommerce_template_single_add_to_cart(); ?>
        <?php if($product->is_type( 'variable' )) { ?>    
            <a href="#<?php echo gt_get_field('popup_window', 'options'); ?>"> <img src="<?php echo img('icon-sixe-chart.png') ?>">מדריך מידות</a>
        <?php } ?>
    </div>
    <div class="clearfix"></div>
    <?php get_template_part('templates/product/content/info-blocks'); ?>
    <div class="clearfix"></div>
    <?php
        if (class_exists('YITH_WCWL_Shortcode')) {
            echo YITH_WCWL_Shortcode::add_to_wishlist([]);
        }
    ?>
    <div class="clearfix"></div>
</div>
<style type="text/css">
    .product-description {
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        margin-bottom: 20px;
        padding: 10px 0 !important;
        text-align: center;
    }
    .product-lite-text{
        padding: 5px 0 20px;
    }
    .quantity {
        display: none;
    }
    .single-product-top{
        display: flex;
        flex-direction: column;
    }
    .single-product-categories{
        font-size: 20px;
    }
    .single-product-title h1{
        margin-bottom: 0;
    }
    .add-to-cart-form{
        position: relative;
    }
    .add-to-cart-form a{
        position: absolute;
        top: 50px;
        left: 0;
        font-size: 12px;
    }
    .add-to-cart-form a img{
        padding-left: 5px;
    }
    .add-to-cart-form form button[type=submit] {
        background-color: #000 !important;
        color: #FFF !important;
        text-transform: uppercase;
        font-weight: 100;
        padding: 8px 12px 7px 12px;
        font-size: 22px;
        letter-spacing: 0.5px;
        border: none;
        cursor: pointer;
        width: 100%;
        border-radius: 0;
    }

    .product_title.entry-title
    {
        font-weight: 100;
        font-size: 28px;
        text-align: center;
    }

    .single-product-stars {
        text-align: center;
    }
    .flex-mobile{
        text-align: center;
    }
    .price, .prices
    {
        display: flex;
        padding: 0;
        margin: 0;
        justify-content: center;
        align-items: baseline;
        flex-direction: row-reverse;
    }

    .woocommerce-Price-amount.amount {
        font-size: 26px !important;
        letter-spacing: -1px;
        margin: 0 10px;
    }
    span.woocommerce-Price-currencySymbol {
        font-size: 16px;
    }
    .new-price span
    {
        color: #990000;
    }

    .xsell-selector {
        display: flex;
        justify-content: center;
    }
    .xsell-selector-item
    {
        color: #b2b2b2;
        cursor: pointer;
    }
    .xsell-selector-item.active
    {
        color: #000;
    }
    .xsell-selector-item.active h1.best-match {
        text-decoration: underline;
    }
    
    @media screen and (max-width: 768px){
        .flex-mobile{
            display: flex;
            justify-content: space-between;
        }
        .product_title.entry-title {
            text-align: right;
        }
        .price, .prices {
            justify-content: flex-end;
        }
        
        .recents-title, .xsell-selector img{
            display: none;
        }
        .xsell-selector-item.active h1.best-match {
            text-decoration: none;
            font-size: 40px;
            letter-spacing: 1px;
        }
       .xsell-container {
            margin: 40px auto !important;
        }
        .woocommerce-Price-amount.amount {
            font-size: 12px !important;
        }
    }
</style>