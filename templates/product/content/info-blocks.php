<?php
if (!function_exists('printProductInfoBlock')) {
    function printProductInfoBlock($title, $content, $isOpen = false, $isLTR = false)
    {
        ?>
        <div class="info-block <?php echo $isOpen ? '' : 'collapsed'; ?> <?php echo $isLTR ? 'ltr' : ''; ?>">
            <div class="info-block-title">
                <?php echo $title; ?>
                <a class="info-block-toggle">
                    <img src="<?php echo img('arrow_top.png'); ?>"/>
                </a>
            </div>
            <div class="info-block-content">
                <div data-acsb-navigable="true" class="product-lite-text"><?php echo $content; ?></div>
            </div>
        </div>
        <?php
    }
}?>

<div class="info-blocks-title"><?php echo gt_get_field('info_blocks_title', 'option'); ?></div>
<div class="info-blocks">
    <?php
    global $product;
    $info = gt_get_field('product_info');
    $fields = gt_get_field('product_info', 'option');

    $break = false;
    $warText = $fields['warranty_default'];
    foreach (get_the_terms( $product->get_id(), 'product_cat') as $term) {
        foreach ($fields['warranty'] as $warranty) {
            if ($warranty['product_cat'] === $term->term_id) {
                $warText = $warranty['war_text'];
                $break = true;
                break;
            }
        }
        if ($break) {
            break;
        }
    }


    printProductInfoBlock('תיאור הדגם', $product->get_description(), true);
    //printProductInfoBlock('פרטים נוספים', $info);
    printProductInfoBlock('משלוחים והחזרות', $fields['returns']);
    printProductInfoBlock('אחריות', $warText);
    ?>

    
</div>
מק"ט: <?php echo$product->get_sku();?>
<style type="text/css">
    .info-block.ltr p {
        text-align: left;
    }
</style>
