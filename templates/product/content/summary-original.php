<div class="summary entry-summary">
    <?php global $product; ?>
        
    <div class="single-product-top">        
        <div class="single-product-title"><?php woocommerce_template_single_title(); ?></div>
        <div class="single-product-stars"><?php GT::showProductAverageRating($product); ?></div>
        <?php  woocommerce_template_single_price(); ?>
    </div>
    <div class="product-lite-text product-description"><?php echo $product->get_short_description(); ?></div>
    <div class="mobile-gallery position-relative"></div>

    <div class="add-to-cart-form">
        <?php woocommerce_template_single_add_to_cart(); ?>
    </div>
    <div class="clearfix"></div>
    <?php get_template_part('templates/product/content/info-blocks'); ?>
    <div class="clearfix"></div>
    <?php
        if (class_exists('YITH_WCWL_Shortcode')) {
            echo YITH_WCWL_Shortcode::add_to_wishlist([]);
        }
    ?>
    <div class="clearfix"></div>
</div>
<style type="text/css">
    .product-description {
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        margin-bottom: 20px;
        padding: 10px 0 !important;
        text-align: center;
    }
    .product-lite-text{
        padding: 5px 0 20px;
    }
    .quantity {
        display: none;
    }
    .single-product-top{
        display: flex;
        flex-direction: column;
    }

    .single-product-title h1{
        margin-bottom: 0;
    }

    .add-to-cart-form form button[type=submit] {
        background-color: #000 !important;
        color: #FFF !important;
        text-transform: uppercase;
        font-weight: 100;
        padding: 8px 12px 7px 12px;
        font-size: 22px;
        letter-spacing: 0.5px;
        border: none;
        cursor: pointer;
        width: 100%;
        border-radius: 0;
    }

    .product_title.entry-title
    {
        font-weight: 100;
        font-size: 28px;
        text-align: center;
    }

    .single-product-stars {
        text-align: center;
    }

    .price, .prices
    {
        display: flex;
        padding: 0;
        margin: 0;
        justify-content: center;
    }

    .woocommerce-Price-amount.amount {
        font-size: 26px;
        letter-spacing: -1px;
        margin-right: 10px;
    }
    .new-price span
    {
        color: #990000;
    }

    .xsell-selector {
        display: flex;
        justify-content: center;
    }
    .xsell-selector-item
    {
        color: #b2b2b2;
        cursor: pointer;
    }
    .xsell-selector-item.active
    {
        color: #000;
    }
    .xsell-selector-item.active h1.best-match {
        text-decoration: underline;
    }
</style>