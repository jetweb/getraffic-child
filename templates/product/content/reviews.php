<div class="product-reviews">
    <div class="container">
        <h1 aria-level="2">חוות דעת</h1>
        <?php
        global $product;
        ?>
        <div class="star-rating average"><span style="width:<?php echo (round($product->get_average_rating()) / 5) * 100; ?>%">דורג <strong
                        class="rating"><?php echo $product->get_average_rating(); ?></strong> מתוך 5</span></div>
        <?php
        $count = $product->get_rating_count();
        
        if ($count && wc_review_ratings_enabled()) { ?>
            <div class="rev-count">
                <div><?php echo $product->get_rating_count(); ?> לקוחות דירגו</div>
                <a class="tony-button-white add-comment">רוצה להשאיר חוות דעת על המוצר</a>
            </div>
        <?php } else { ?>
            <a class="tony-button-white add-comment">רוצה להשאיר חוות דעת על המוצר</a>
        <?php } ?>


        <?php
        // If comments are open or we have at least one comment, load up the comment template.
        comments_template();
        if (comments_open() || get_comments_number()) {
        }
        ?>
    </div>
</div>
<script defer>
    jQuery(document).ready(function ($) {
        $('a.add-comment').click(function () {
            $('#review_form').show(400);
            $('#review_form')[0].scrollIntoView({behavior: "smooth"});
        });
        $(".description").append("<p class='description-text'>האם הביקורת עזרה לך?</p>");
    });
</script>

