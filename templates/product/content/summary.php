<div class="summary entry-summary">
    
    <?php global $product; ?>    
    <?php $prod_categories = get_the_terms($product->get_id(), 'product_cat'); ?>
    <?php foreach ($prod_categories as $prod_category){ 
        $cat_link = get_term_link($prod_category->term_id);
        $category_names[] = '<a href="' . $cat_link . '">' . $prod_category->name . '</a>';
    } ?>
    <?php $brand = $product->get_attribute( 'pa_brand' ); ?>
    <div class="mobile-gallery position-relative"></div>
    <div class="single-product-top">
        <div class="single-product-title" data-acsb-navigable="true">
<!--            <h1>--><?php //echo $product->get_title(); ?><!--</h1>-->
            <?php woocommerce_template_single_title(); ?>
            <span class="single-product-stars only-mobile"><?php GT::showProductAverageRating($product); ?></span>
        </div>
        <div class="flex-mobile">
            <span style="display: none;" class="single-product-categories"><?php  echo implode(" | ", $category_names); ?></span>
            <span class="single-product-categories brand"><?php echo $brand; ?></span>
            <span class="single-product-stars"><?php GT::showProductAverageRating($product); ?></span>
        </div>
        <div class="prices" tabindex="0" data-acsb-navigable="true">
            <?php echo $product->get_price_html(); ?>
        </div>

    </div>
    <?php
    $product_short_description = $product->get_short_description();
    if(!empty($product_short_description)) { ?>    
        <div class="product-description" data-acsb-navigable="true"><?php echo $product_short_description; ?></div>
    <?php } ?>   

    <div class="add-to-cart-form">
        <?php woocommerce_template_single_add_to_cart(); ?>
        <div class="variation-error"></div>
        
    </div>
    <div class="clearfix"></div>
    <?php get_template_part('templates/product/content/info-blocks'); ?>
    <div class="clearfix"></div>
    <?php
        if (class_exists('YITH_WCWL_Shortcode')) {
            echo YITH_WCWL_Shortcode::add_to_wishlist([]);
        }
    ?>
    <div class="clearfix"></div>
</div>
<script type="text/javascript" defer>
    jQuery(document).ready(function ($) {
        var $ = jQuery;        
        $('.single_add_to_cart_button').click(function() {            
            setTimeout(function () {
                $('.single_add_to_cart_button').removeClass('added');
            }, 8000);
        });
    });
</script> 
