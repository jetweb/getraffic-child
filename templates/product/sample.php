<?php

$product      = get_query_var('productToRender');

$thumb        = get_query_var('thumb');

$idForWLandQV = get_query_var('idForWLandQV');

$variation = get_query_var('variationDataArray');



?>



<div class="product-wrapper sample" data-product-id="<?php echo $product->get_id(); ?>">

    <div class="thumb-wrapper">

        <a href="<?php echo $product->get_permalink(); ?>" class="thumb-link">

            <img src="<?php echo $thumb; ?>" alt="" class="thumb">

        </a>

        <div class="thumb-overlay">

                <?php if ($product instanceof WC_Product_Variable) { ?>

                    <a href="#" onclick='return openQuickView(<?php echo $idForWLandQV; ?>, <?php echo json_encode($variation['attributes']); ?>, <?php echo json_encode($variation['image']['url']); ?>);' class="shop-button">הוספה לסל</a>

                <?php } else { ?>



					<?php

					$args = GT::getLoopAddToCartArguments($product);

					$pos = strpos($args['class'], 'button');

					if ($pos !== false) {

						$args['class']  = substr_replace($args['class'],'shop-button', $pos, strlen('button'));

					}



					echo apply_filters( 'woocommerce_loop_add_to_cart_link', // WPCS: XSS ok.

						sprintf( '<a href="%s" data-quantity="%s" class="%s" %s>%s</a>',

							esc_url( $product->add_to_cart_url() ),

							esc_attr( isset( $args['quantity'] ) ? $args['quantity'] : 1 ),

							esc_attr( isset( $args['class'] ) ? $args['class'] : 'button' ),

							isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '',

							'הוספה לסל' 

						),

						$product, $args );

					?>





				<?php } ?>



        </div>

    </div>

    <div class="product-info">

        <a href="<?php echo $product->get_permalink(); ?>"><h3 class="title"><?php echo $product->get_title(); ?></h3></a>

        <?php echo $product->get_price_html(); ?>

    </div>

    <div class="sample-cart">

    </div>

</div>



