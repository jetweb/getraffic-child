<?php $product = get_query_var('qvProduct'); ?>
<div class="qv-modal">
    <div class="quick-view">
        <input type="hidden" name="cartItemToReplace" value="<?php echo get_query_var('cartItemKey'); ?>">
        <div class="qv-details">
            <?php get_template_part('templates/product/content/quickview-summary'); ?>
            <a class="qv-link" href="<?php echo $product->get_permalink(); ?>">לפרטים נוספים</a>
        </div>
        <div class="qv-gallery">
            <?php get_template_part('templates/product/content/images/quickview-gallery'); ?>
        <div class="info-blocks-title"><?php echo gt_get_field('info_blocks_title', 'option'); ?></div>
        </div>
    </div>
</div>
