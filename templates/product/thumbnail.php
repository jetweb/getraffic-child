<?php
$product      = get_query_var('productToRender');
$showControls = get_query_var('showControls');
$thumb        = get_query_var('thumb');
$description  = get_query_var('description');
$idForWLandQV = get_query_var('idForWLandQV');
$thumbHover   = get_query_var('thumbHover');
?>



<div class="product-wrapper test" data-product-id="<?php echo $product->get_id(); ?>">
    <div class="thumb-wrapper">
        <?php
        if ($product instanceof WC_Product_Simple) {
            GT::printProductBadge($product);
        }

        ?>
        <a href="<?php echo $product->get_permalink(); ?>">
            <img src="<?php echo $thumb; ?>"
                 alt="<?php echo $product->get_title(); ?>"
                 class="thumb"
                 data-hover="<?php echo $thumbHover;?>"
                 data-original-src="<?php echo $thumb; ?>">
        </a>


        <?php if ($showControls) { ?>
            <div class="prod-thumb-bottom">
                <?php GT::renderWishlistButton($idForWLandQV, img('heart_pink_full.png'), img('heart_pink.png')); ?>
<!--
                <?php if ($variation && $variation['attributes'] && $variation['image']) { ?>
                    <a href="#" onclick='return openQuickView(<?php echo $idForWLandQV; ?>, <?php echo json_encode($variation['attributes']); ?>, <?php echo json_encode($variation['image']['url']); ?>);' class="quickview">QUICKVIEW</a>
                <?php } else { ?>
                    <a href="#" onclick='return openQuickView(<?php echo $idForWLandQV; ?>);' class="quickview">QUICKVIEW</a>
                <?php } ?>
-->
            </div>
        <?php } ?>
        
	<!--a href="<?php echo $product->get_permalink(); ?>" class="thumb-overlay">
            <span class="shop-button"><?php echo gt_get_field('overlay_button', 'option') ?></span>
        </a--!>


        <?php
        //echo apply_filters('post_thumbnail_html', '', $product->get_id(), 0, null, null);
        if ($product instanceof WC_Product_Variation) {
            echo apply_filters('post_thumbnail_html', '', $product->get_parent_id(), 0, null, null);
        } else {
            echo apply_filters('post_thumbnail_html', '', $product->get_id(), 0, null, null);
        }
        ?>
    </div>

    <div class="product-info" data-acsb-navigable="false" data-acsb-navigable-now="false">
        <a href="<?php echo $product->get_permalink(); ?>"><h3 class="title" data-acsb-navigable="true"><?php echo $product->get_title(); ?></h3></a>
        <span class="price">
            <div style="display: inline" data-acsb-navigable="true">
            <?php echo $product->get_price_html(); ?>
            </div>
            <?php $brand = $product->get_attribute( 'pa_brand' );
            if (!$brand && $product instanceof WC_Product_Variation) {
                $brand = wc_get_product($product->get_parent_id())->get_attribute( 'pa_brand' );
            }
            if ($brand) { ?>
        <span class="single-product-categories brand"> // <?php echo $brand; ?></span>
        <?php } ?>
        </span>
    </div>
   <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>

</div>

