<div class="post_text"> 
    <div class="post_text_link">
        <a href="<?php echo get_site_url() . '/articles'; ?>"><div>חזרה לבלוג</div><img src="<?php echo img('back-icon.png'); ?>"></a>
    </div>
    <?php
    $settings = gt_get_field('post_text_icons', 'option');
    ?>
    <div class="post_text_icons">
        <?php
        foreach ($settings as $icon) {
            ?>
            <div class="post_text-icon"><a href="<?php echo $icon['link']; ?>"><img src="<?php echo $icon['image']['url']; ?>"></a></div>
            <?php
        }
        ?>
    </div>
</div>