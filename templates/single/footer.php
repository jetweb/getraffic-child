<?php
$settings = gt_get_field('footer', 'option');
?>
<div class="single-footer">
    <?php
    foreach ($settings as $icon) {
        ?>
        <div class="single-footer-icon"><a href="<?php echo $icon['link']; ?>" rel="nofollow"><img src="<?php echo $icon['image']['url']; ?>"></a></div>
        <?php
    }
    ?>
</div>
<div class="single-postfooter">כל הזכויות שמורות לר. ביסה בע״מ רח׳ הגר״א 17 תל אביב</div>