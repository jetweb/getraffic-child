<div class="topbar">
    <div class="container align-center padmenot topicons-container">
        <div class="topicons-right">
            <?php get_template_part('templates/header/topbar/we-are-here'); ?>
        </div>
        <div class="top-single-menu">
            <?php get_template_part('templates/header/topbar/menu'); ?>
        </div>
        <div class="topicons-left">
            <?php get_template_part('templates/header/topbar/logo'); ?>
        </div>
    </div>
</div>
