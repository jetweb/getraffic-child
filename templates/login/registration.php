
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="container">
                <div class="row">
                    <div class="join-us-container">
                        <h1 class="tony-title">הרשמה לאתר</h1>
                        <p>כיף שהחלטת להצטרף אלינו! מעכשיו תוכלי ליהנות מדגמים בלעדיים לאתר והמון מבצעים שווים</p>
                        <p class="validation"></p>
                        <form method="post" id="registrationForm">
                            <input type="hidden" name="action" value="ajax-register-user">
                            <div class="popup-firstname-field popup-firstname">
                                <input type="text" class="validate validate-only-letters" data-error-letters="אפשר להשתמש פה רק באותיות בעברית או באנגלית" data-empty-message="כדי שתוכלי להירשם, נצטרך לדעת מה השם הפרטי שלך" placeholder="שם פרטי *" id="first_name" name="first_name" />
                                <div class="error"></div>
                            </div>
                            <div class="popup-lastname-field popup-lastname">
                                <input type="text" class="validate validate-only-letters" data-error-letters="אפשר להשתמש פה רק באותיות בעברית או באנגלית" data-empty-message="כדי שתוכלי להירשם, נצטרך לדעת מה שם המשפחה שלך" placeholder="שם משפחה *" name="last_name"/>
                                <div class="error"></div>
                            </div>
                            <div class="popup-phone-field popup-phone">
                                <input type="text" class="validate" data-empty-message="נצטרך את הטלפון שלך כדי לעדכן אותך לגבי ההזמנות שלך" placeholder="טלפון נייד *" name="phone"/>
                                <div class="error"></div>
                            </div>
                            <div style="clear: both"></div>
                            <div class="popup-mail-field">
                                <input type="text" class="validate popup-mail" data-empty-message="כדי שתוכלי להירשם, נצטרך לדעת מה המייל שלך" placeholder="כתובת מייל *" name="email"/>
                                <div class="error error_mail"></div>
                            </div>
                            <div class="popup-password-field popup-password new_password form-row-eye">
                                <input type="password" class="validate validate-password" data-empty-message="כדי שהחשבון שלך באתר יהיה מאובטח, יש ליצור לו סיסמה" placeholder="סיסמה *" name="password"/>
                                <div class="error error_password"></div>
                                <img class="closed-eye" src="<?php echo img('closed-eye.png'); ?>"><img class="opened-eye" src="<?php echo img('opened-eye.png'); ?>">
                            </div>
                            <div class="popup-confirm-password-field popup-confirm-password confirm_new_password form-row-eye">
                                <input type="password" class="validate" data-empty-message="רק עוד פעם אחת בשביל לוודא... תודה :)" placeholder="אימות סיסמה *" name="password"/>
                                <div class="error error_confirm_password"></div>
                                <img class="closed-eye" src="<?php echo img('closed-eye.png'); ?>"><img class="opened-eye" src="<?php echo img('opened-eye.png'); ?>">
                            </div>
                            <label class="bd-benefits" for="accept_benefits">
                                <input type="checkbox" id="accept_benefits" name="accept_benefits"/>
                                <span>עדכנו אותי על מבצעים מיוחדים, קופונים בלעדיים ועוד דברים מעניינים שקורים באתר במייל וב-SMS</span>
                            </label>
                            <label class="bd-terms" for="accept_terms">
                                <input type="checkbox" id="accept_terms" name="accept_terms"/>
                                <span>אני מאשרת את <a href="#">תנאי השימוש</a> באתר *</span>
                                <span class="error_terms"></span>
                            </label>
                            <div><button class="tony-button full-width sharp-corners register-button" name="btn_submit">תרשמו אותי</button></div>
                            <div class="tooltip-phone">
                                

                            </div>
                            <div class="tooltip-mail">


                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-bg-blue"></div>
    </div>

