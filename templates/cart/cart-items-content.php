<?php
foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
    $_product      = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
    $product_id    = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
    $prod_category = get_the_terms($cart_item['product_id'], 'product_cat')[0]->name;
    if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
        $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item,
            $cart_item_key);
        ?>
        <div class="woocommerce-cart-form__cart-item <?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key)); ?>">
            <div class="product-thumbnail">
                <?php
                $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image('thumbnail'), $cart_item, $cart_item_key);
                if (!$product_permalink) {
                    echo wp_kses_post($thumbnail);
                } else {
                    printf('<a href="%s">%s</a>', esc_url($product_permalink), wp_kses_post($thumbnail));
                }
                ?>
            </div>
            <div class="cart-item-info">
                <div class="product-name" data-title="<?php esc_attr_e('Product', 'woocommerce'); ?>">
                    <?php
                    if (!$product_permalink) {
                        echo wp_kses_post(apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;');
                    } else {
                        echo wp_kses_post(apply_filters('woocommerce_cart_item_name',
                            sprintf('<a href="%s">%s</a>', esc_url($product_permalink), $_product->get_title()), $cart_item, $cart_item_key));
                    }
                    do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);
                    // Meta data.
                    echo wc_get_formatted_cart_item_data($cart_item); // PHPCS: XSS ok.
                    // Backorder notification.
                    if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                        echo wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification',
                            '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>'));
                    }
                    ?>
                </div>
                <div class="product-price" data-title="<?php esc_attr_e('Price', 'woocommerce'); ?>">
                    <?php
                    //echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); // PHPCS: XSS ok.
                    echo $_product->get_price_html();
                    ?>
                </div>
                <div class="cart-item-actions">
                    <div class="product-quantity" data-title="<?php esc_attr_e('Quantity', 'woocommerce'); ?>">
                        <?php
                        if ($_product->is_sold_individually()) {
                            $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                        } else {
                            $product_quantity = woocommerce_quantity_input([
                                'input_name'   => "cart[{$cart_item_key}][qty]",
                                'input_value'  => $cart_item['quantity'],
                                'max_value'    => $_product->get_max_purchase_quantity(),
                                'min_value'    => '0',
                                'product_name' => $_product->get_name(),
                            ], $_product, false);
                        }
                        echo '<div class="q_wrapper">';
                        echo sprintf('<a class="q_arrow_up %s">+</a>', $cart_item['quantity'] == $_product->get_max_purchase_quantity() ? 'disabled' : '');
                        echo sprintf('<input class="quantityHidden" type="text" name="cart[%s][qty]" value="%s" />', $cart_item_key, $cart_item['quantity']);
                        echo sprintf('<a class="q_arrow_down %s">-</a>', $cart_item['quantity'] == 0 ? 'disabled' : '');
                        echo '</div>';
                        //echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
                        ?>
                    </div>
                    <?php
                    set_query_var('attributesViewProduct', $_product);
                    set_query_var('cart_item_key', $cart_item_key);
                    $editType = gt_get_field('cart_attr_edit_type', 'option');
                    switch ($editType) {
                        case ('Quick View'):
                            get_template_part('templates/cart/attributes-view');
                            break;
                        case ('Dropdown'):
                            get_template_part('templates/cart/attributes-dropdown');
                            break;
                    }
                    ?>
                </div>
            </div>
            <div class="product-wishlist">
                <?php if (function_exists('YITH_WCWL')) { ?>
                    <a href="#" class="wishlist-icon inactive <?= (YITH_WCWL()->is_product_in_wishlist($product_id)) ? 'hidden' : '' ?>" data-product-id="<?= $product_id ?>" title="<?= gt_get_field('cart_wishlist_add_text', 'option'); ?>"
                       onclick="return addCartItemToWishlist($(this));">
                        <img src="<?php echo img('wishlist-icon.png') ?>" alt=""/>
                        <p>העבירי ל-Wishlist</p>
                    </a>
                    <a class="wishlist-icon active hidden <?= (YITH_WCWL()->is_product_in_wishlist($product_id)) ? '' : 'hidden' ?>" data-product-id="<?= $product_id ?>">
                        <img src="<?php echo img('wishlist-icon.png') ?>" alt=""/>
                        <p><?= gt_get_field('cart_wishlist_exists_text', 'option'); ?></p>
                    </a>
                <?php } ?>
            </div>
            <div class="product-remove">
                <?php
                // @codingStandardsIgnoreLine
                echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
                    '<a href="%s" class="remove" aria-label="%s" title="%s" data-product_id="%s" data-product_sku="%s"><img src="' . img('delete-icon.png') . '" /><p>הסרה</p></a>',
                    esc_url(wc_get_cart_remove_url($cart_item_key)),
                    __('Remove this item', 'woocommerce'),
                    __('Remove this item', 'woocommerce'),
                    esc_attr($product_id),
                    esc_attr($_product->get_sku())
                ), $cart_item_key);
                ?>
            </div>
        </div>
        <?php
    }
}
?>
  <!--script>
  $( function() {
	  if ($(window).width() > 768)
	  {
		$( '.wishlist-icon, .remove' ).tooltip();
	  }
  } );
  </script--!>
   <style>
  .arrow::before {color:#fff;border-top-color:#fff !important;border-color:transparent;}

  .tooltip, .tooltip-inner {
    color: black;
	font-size:13px;
    text-transform: uppercase;
  }
  .tooltip-inner {
  background:#fff;
  }

  </style>
