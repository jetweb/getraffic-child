<?php
/**
 * Template Name: Single post
 *
 * The template for displaying pages without the sidebar
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */

//get_header(); ?>
<div class="single-post">
<?php get_template_part('templates/header/head'); ?>
<?php get_template_part('templates/single/header'); ?>

<div class="post-container" id="primary">
    <div class="content">
        <?php
        if (have_posts()) {
            while (have_posts()) : the_post();
                ?>
                <div class="row margin-bottom">
                    
                    <div class="col-sm-12">
                        <div class="post-wrapper">
                            <h2><?php the_title(); ?></h2>
                            <h3><?php echo get_field('subtitle'); ?></h3>
                            <div class="post-date"><?php echo get_the_date('d.m.Y'); ?></div>
                            <div class="post-content"><?php the_content(); ?></div>
                        </div>
                        <?php get_template_part('templates/single/post_text_icons'); ?>
                        <div class="comments-toptext">ספרי לנו מה חשבת</div>
                        <div class="post-comments"><?php comments_template(); ?></div>

                        
                    </div>
                </div>
            <?php
            endwhile;
        }
        ?>
    </div>
</div>
<?php get_template_part('templates/single/footer'); ?>
</div>
<style>
    
    
    
    .single-post .container{
        max-width: 100%;
    }
    .single-post .topicon.location{
        display: none
    }
    .single-post .topbar {
        position: relative;
        background: #f4ebe5;
        height: 70px;
    }
    .single-post .topbar .topicons-right{
        position: absolute;
        right: 62px;
        top: -5px;
    }
    .single-post .topbar .topicons-left{
        position: absolute;
        left: 85px;
        top: 0;
    }
    .single-post .topbar .top-single-menu{
        margin-right: 260px;
        padding-top: 15px;
    }
    .single-post .topbar .top-single-menu .nav-right {
        justify-content: right;
    }
    .post-container{
        margin: 80px 140px 0;
    }
    .post-container .content{
        padding: 30px 176px 0 192px;
        background: #f8f8f8;
    }
    .post-container .content ul.wp-block-gallery{
/*        direction: rtl;*/
        margin-top: 40px;
    }
    .post-container .content h2{
        font-size: 17px;
        letter-spacing: 0.3px;
        font-weight: bold;
    }
    .post-container .content h3{
        font-size: 14px;
        border-bottom: 1px solid #000;
        padding-bottom: 14px;
        letter-spacing: 0.1px;
    }
    .post-container .content .post-date{
        font-size: 26px;
        line-height: 26px;
    }
    .post-container .content .post-content{
        margin-top: 35px;
    }
    .post-container .content .post-content p{        
        font-size: 13px;
        line-height: 16px;
        letter-spacing: 0.2px;
    }
    .post-container .content .post-content img{
        margin-bottom: 16px;
    }
    .post-container .content .post-content ul{
        margin-bottom: 0;
    }
    .single-footer{
        display: flex;
        justify-content: center;
        flex-direction: row-reverse;
        padding: 25px 0 20px;
        background: #f4ebe5;
    }
    .single-footer-icon{
        padding: 0 10px;
    }
    .single-postfooter{
        text-align: center;
        padding: 14px 0;
        font-size: 11px;
        font-weight: 600;
    }
    .post_text{
        display: flex;
        justify-content: space-between;
        border-bottom: 1px solid #000;
        border-top: 1px solid #000;
        padding: 17px 5px;
    }
    .post_text_link a, .post_text_icons{
        display: flex;
        flex-direction: row-reverse;
        align-items: center;
    }
    .post_text-icon{
        padding-right: 5px;
    }
    .post_text-icon img{
        width: 85%;
    }
    .post_text_link img{
        padding-left: 15px;
    }
    .comment-form-comment{
        text-align: center;
    }
    .comment-form-comment textarea{
        width: 520px;
        height: 135px;
    }
    div.comment-form-rating label, p.comment-form-comment label, comment-form-url, comment-form-cookies-consent{
        display: none;
    }
    .comments-toptext{
        text-align: center;
        margin-top: 40px;
        font-size: 20px;
    }
    .comment-form-author, .comment-form-email{
        float: right;
    }
    form.comment-form{
        text-align: center;
        padding-bottom: 15px;
    }
    @media screen and (max-width: 768px) {
    .comment-form-comment textarea{
        width: 100%;
    }
    .post-container {
        margin: 0;
    }
    .post-container .content {
        padding: 20px;
    }
    .single-post .topbar {
        position: relative !important;
        left: 0;
        height: 120px;
    }
    .single-post .topbar .top-single-menu{
        margin-right: 0px;
        padding-top: 0;
    }
    .single-post .topbar .top-single-menu .nav-right {
        justify-content: center;
    }
    .single-post .topicons-container a#logo {
        display: block;
    }
    .single-post .topicons-left {
        left: 0px;
        top: 0px;
    }
    .single-post .topbar .nav-right {
        padding-top: 70px;
    }
    .single-post .topbar ul.nav li {
        margin-right: 0;
    }
   .single-post .topbar .we-are-here-box {
        border: 1px solid #cccccc !important;
    }
}
</style>
<script type="text/javascript">
    jQuery(document).ready(function () {
        let $ = jQuery;
        $(".comment-notes").html('הוסיפי תגובה');
        $(".comment-form-author input").attr('placeholder', 'השם שלך *');
        $(".comment-form-email input").attr('placeholder', 'כתובת המייל *');
//        $(".form-submit input").val('שלח');
    });
</script>