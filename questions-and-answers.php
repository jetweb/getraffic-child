<?php
/**
 * Template Name: Questions and Answers
 *  *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>
<?php  
    $key = $_GET['key'];        
    $name = $_GET['q'];        
    $all_questions =  get_field($key, $post->post_parent);
?>
<div class="contact-banner"><img src="<?php echo img('questions-banner.png'); ?>"/></div>
<div class="questions-menu"><?php get_template_part('templates/sidemenu/sidemenu'); ?></div>
<div class="container question-container">
    <div class="row col-md-5 col-md-offset-2">
        <div class="question-image"><img src="<?php echo img($key . '-questions.png'); ?>"></div>
        <div class="main-questions"><h3><?php echo $name; ?></h3></div>
        <?php  
        
        if($all_questions){
            foreach ($all_questions as $questions){ 
        $i=1;
        foreach ($questions as $question){ 
            echo ($i%2) ? "<div class='question'>" . $question  . "<span class='questions-plus'>+</span><span class='questions-minus'>-</span></div>" : "<div  class='answer'>" . $question . "</div>"; 
            $i++;    
        }
            }
        }?>
    </div>
</div>
<div style="clear:both"></div>
<?php get_footer(); ?>
<style>
    .row{
        margin-top: 100px;
        margin-bottom: 100px;
    }
    .contact-banner img{
        width: 100%;
    }
    .questions-menu{
        width: 19%;
    float: right;
    }
    .sidemenu-menu{
        width: auto;
    }
    .main-questions{
        width: 100%;
        padding: 20px 0 27px 0;
        border-bottom: 1px solid #ccc;
    }
    .main-questions img{
        float: right;
        padding: 0 0 0 15px;
    }
    .main-questions h3{
        margin: 5px 0 0 0;
    }
    .question-image{
        width: 100%;
        text-align: center;
    }
    .question-image img{
        width: 15%;
    }
    .question-container h3{
        text-align: center;
        font-size: 15px;
    margin-top: -8px;
    }
    .question{
        width: 100%;
        border-bottom: 1px solid #ccc;
        padding: 30px 0 8px 0;
        cursor: pointer;
        font-size: 14px;
    }
    .question.active{
        border-bottom: none;
        padding-right: 0px;
    }
    .questions-plus, .questions-minus{
        font-size: 30px;
        color: #cdb6ac;
        margin-top: -10px;
    }
    .question .questions-minus{
        display: none;
    }
    .question .questions-plus{
        float: left;
    }
    .question.active .questions-minus{
        display: block;
        float: left;
    }
    .question.active .questions-plus{
        display: none;
    }
    .answer{
        display: none;
        border-bottom: 1px solid #000;
        padding: 20px 0;
        width: 100%;        
    }
    .answer p{        
        font-size: 14px;
    }
    @media screen and (max-width: 768px){
        
        .questions-menu {
            display: none;
        }
        .row {
            margin-top: 40px;
            margin-bottom: 0;
            margin-right: 0;
        }
    }
</style>
<script type="text/javascript">
    jQuery(document).ready(function () {
        let $ = jQuery;
        $(".question").click(function(){   
            if ($(this).hasClass("active")){
                $(this).removeClass("active")
            }else{$(this).addClass('active');}

            $(this).next().toggle('fast'); 
        });
    });
</script>