<?php
$product = get_query_var('queriedProduct');
$subject = get_query_var('emailSubject');
(new WC_Emails())->email_header($subject);

$prodID = $product->get_id();
if ($product instanceof WC_Product_Variation) {
    $prodID = $product->get_parent_id();
}

?>
    
    <p>היי,</p>
    <p>יש לנו עדכון שבטוח יגרום לך לחייך – הנעליים שרצית <?php echo $product->get_title(); ?> –  חזרו למלאי!
    <p>תרגישי בנוח לרקוד ריקוד קטן של אושר, אבל אל תתמהמהי יותר מדי - </p>
    <p>הן נחטפות צ'יק צ'ק אז חבל לפספס את ההזדמנות...</p>
    <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($prodID))[0] ?>" alt="" class="bism" style="display: block; margin: 0 auto; margin: 0 auto 20px; float: none;"/>
    <a href="<?php echo get_permalink($product->get_id()); ?>" style="text-decoration: none;    background: #fff;    color: #000;    width: 200px;    display: block;    height: 40px;    line-height: 40px;    text-align: center;    margin: 0 auto;    font-weight: bold;    border: 2px solid #000;    box-shadow: 1px 1px 1px;    margin-bottom: 20px;">מהר, לפני שייגמר!</a>

<?php (new WC_Emails())->email_footer(); ?>
