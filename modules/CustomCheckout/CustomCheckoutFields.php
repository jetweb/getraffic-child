<?php

namespace Getraffic;

class CustomCheckoutFields
{
    private $customAddressFields = null;

    public $fields = [
        'billing' => [
            'billing_email'      => [
                'placeholder' => 'כתובת מייל *',
                'required'    => true,
                'class'       => ['form-row-first'],
            ],
            'billing_checkbox'   => [
                'type'  => 'checkbox',
                'class' => ['form-row-last'],
                'label' => '<span>מאשרת קבלת עידכונים</span>',
                'clear' => true,
            ],
            'billing_first_name' => [
                'placeholder' => 'שם פרטי *',
                'required'    => true,
                'class'       => ['form-row-first'],
                'clear'       => true,
            ],
            'billing_last_name'  => [
                'placeholder' => 'שם משפחה *',
                'required'    => true,
                'class'       => ['form-row-last'],
                'clear'       => true,
            ],
            'billing_phone'      => [
                'placeholder' => 'טלפון נייד *',
                'required'    => false,
                'class'       => ['form-row-first'],
                'clear'       => true,
            ],
            'account_password'   => [
                'type'        => 'password',
                'placeholder' => 'סיסמה * (איתה תוכלי לעקוב אחרי ההזמנה שלך)',
                'required'    => false,
                'class'       => ['form-row-last'],
                'clear'       => true,
            ],
            'billing_country'    => [ // This field is mandatory
                'type'     => 'country',
                'required' => true,
                'label'    => 'ארץ',
                'class'    => ['form-row-last, hidden'],
            ],
        ],
        'order'   => [
            'order_comments' => [
                'type' => 'textarea',
                'class' => ['form-row-first'],
                'placeholder' => 'הערות להזמנה'
            ]
        ],
        'shipping' => [
            'billing_street' => [
                'placeholder' => 'רחוב',
                'required'    => false,
                'class'       => ['checkout-street'],
                'clear'       => true,
            ],
            'billing_house' => [
                'placeholder' => 'מספר בית',
                'required'    => false,
                'class'       => ['checkout-house'],
                'clear'       => true
            ],
            'billing_appartment' => [
                'placeholder' => 'דירה',
                'required'    => false,
                'class'       => ['checkout-appartment'],
                'clear'       => true
            ],
            'billing_city' => [
                'placeholder' => 'עיר',
                'required'    => false,
                'class'       => ['checkout-city'],
                'clear'       => true,
            ],
            'order_comments' => [
                'type' => 'textarea',
                'placeholder' => 'הערות להזמנה',
                'required'    => false,
                'class'       => ['checkout-comments'],
                'clear'       => true,
            ],
        ],
        'account' => []
    ];

    public function __construct(CustomAddressFields $fields)
    {
        $this->customAddressFields = $fields;
        add_filter('woocommerce_checkout_fields', function ($fields) {
            return $this->get_checkout_fields_array();
        });


        add_action('woocommerce_checkout_create_order', function($order) {
            if ($order instanceof \WC_Order) {
                $text = '';
                foreach ($this->fields['shipping'] as $name => $field) {
                    $value = $order->get_meta('_' . $name);
                    if (empty($value)) {
                        if (is_callable([$order, 'get_' . $name])) {
                            $value = $order->{"get_{$name}"}();
                        }
                    }
                    if (!empty($value)) {
                        $text .= $field['placeholder'] . ': ' . $value . PHP_EOL;
                    }
                }

                $order->set_shipping_address_1($text);
            }
        });
    }

    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    function get_checkout_fields_array()
    {
        $retVal   = [];
        $priority = 0;
        foreach ($this->fields as $group => $array) {
            $arrFields = [];
            foreach ($array as $fieldName => $field) {
                $priority                        += 10;
                $field['priority']               = $priority;
                $arrFields[$fieldName] = $field;
            }
            $retVal[$group] = $arrFields;
        }

        if (is_user_logged_in()) {
            unset($retVal['billing']['billing_password']);
        }

        return $retVal;
    }

}