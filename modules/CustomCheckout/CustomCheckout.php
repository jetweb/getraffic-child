<?php

class CustomCheckout extends WC_Checkout {
    public function __construct()
    {

    }

    public function validateCheckout()
    {
        try {
            $errors      = new WP_Error();

            $email = $_POST['billing_email'];
            if (empty($email) || !is_email($email)) {
                $errors->add('registration-error-invalid-email', __('Please provide a valid email address.', 'woocommerce'));
            }

            if  (!is_user_logged_in()) {
                if (email_exists($email)) {
                    $errors->add('registration-error-email-exists',
                        apply_filters('woocommerce_registration_error_email_exists', __('An account is already registered with your email address. Please log in.', 'woocommerce'),
                            $email));
                }
            }

            $is_correct = preg_match('/^[0-9]{9,16}$/', $_POST['billing_phone']);
            if ( $_POST['billing_phone'] && !$is_correct) {
                $errors->add('invalid-phone', 'מספר הטלפון צריך להיות מורכב מספרות בלבד ולהכיל לפחות 9 ספרות');
            }

            if ($_POST['billing_first_name'] && 1 === preg_match('~[0-9]~', $_POST['billing_first_name'])) {
                $errors->add('invalid-firstname', 'שם פרטי צריך להיות מורכב מאותיות בלבד');
            }

            if ($_POST['billing_last_name'] && 1 === preg_match('~[0-9]~', $_POST['billing_last_name'])) {
                $errors->add('invalid-lastname', 'שם משפחה צריך להיות מורכב מאותיות בלבד');
            }

            $nonce_value = wc_get_var( $_REQUEST['woocommerce-process-checkout-nonce'], wc_get_var( $_REQUEST['_wpnonce'], '' ) ); // @codingStandardsIgnoreLine.

            if ( empty( $nonce_value ) || ! wp_verify_nonce( $nonce_value, 'woocommerce-process_checkout' ) ) {
                WC()->session->set( 'refresh_totals', true );
                throw new Exception( __( 'We were unable to process your order, please try again.', 'woocommerce' ) );
            }

            wc_maybe_define_constant( 'WOOCOMMERCE_CHECKOUT', true );
            wc_set_time_limit( 0 );

            do_action( 'woocommerce_before_checkout_process' );

            if ( WC()->cart->is_empty() ) {
                /* translators: %s: shop cart url */
                throw new Exception( sprintf( __( 'Sorry, your session has expired. <a href="%s" class="wc-backward">Return to shop</a>', 'woocommerce' ), esc_url( wc_get_page_permalink( 'shop' ) ) ) );
            }

            do_action( 'woocommerce_checkout_process' );


            $posted_data = $this->get_posted_data();

            // Update session for customer and totals.
            $this->update_session( $posted_data );

            // Validate posted data and cart items before proceeding.
            $this->validate_checkout( $posted_data, $errors );

            foreach ( $errors->get_error_messages() as $message ) {
                wc_add_notice( $message, 'error' );
            }
        } catch ( Exception $e ) {
            wc_add_notice( $e->getMessage(), 'error' );
            $this->validate_checkout( $posted_data, $errors );
        }

        if ($errors->has_errors()) {
            $this->send_ajax_failure_response();
        }
    }
}
