<?php

namespace Getraffic;

class AjaxSearch implements ConfigurableModule
{
    const SETTINGS_PAGE_ID = 'ajax-search-settings-page';

    public function __construct()
    {
    }

    public static function getMatchingProducts($s, $max = 10)
    {
        $products = [];
        $foundProductIDs = [];

        // Search in product body
        $query = new \WP_Query([
            'post_type'      => 'product',
			'post_status'   => 'publish',
            'posts_per_page' => $max,
            's'              => $s
        ]);

        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $product_id = get_the_ID();
                $product    = wc_get_product($product_id);
                $products[] = $product;
                $foundProductIDs[] = $product_id;
            }
        }

        // Search by tag taxonomy
        $query = new \WP_Query([
            'post_type'      => 'product',
            'posts_per_page' => $max,
            'tax_query'      => [
                [
                    'taxonomy' => 'product_tag',
                    'field'    => 'slug',
                    'terms'    => [esc_attr($s)],
                    'operator' => 'IN',
                ],
            ],
        ]);

        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $product_id = get_the_ID();
                $product    = wc_get_product($product_id);
                $products[] = $product;
                $foundProductIDs[] = $product_id;
            }
        }



        $querysku = new \WP_Query([
            'post_type'      => ['product', 'product_variation'],
            'posts_per_page' => $max,
            'meta_query'    => array(
                'relation' => 'AND',
                array(
                    'key'     => '_sku',
                    'value'   => $s,
                    'compare' => 'LIKE',
                )
            ),
        ]);

        if ($querysku->have_posts()) {
            while ($querysku->have_posts()) {
                $querysku->the_post();
                $product_id = get_the_ID();
                $product    = wc_get_product($product_id);

                $prodIdToCheck = $product_id;
                if ($product instanceof \WC_Product_Variation) {
                    $prodIdToCheck = $product->get_parent_id();
                }
                if (!in_array($prodIdToCheck, $foundProductIDs)) {
                    $products[] = $product;
                    $foundProductIDs[] = $prodIdToCheck;
                }
            }
        }

        if ($max > 0 && (count($products) > $max)) {
            return array_slice($products, 0, $max);
        } else {
            return $products;
        }
    }

    public static function ajaxSearch()
    {
        $settings = get_option(self::SETTINGS_PAGE_ID);
        $products = self::getMatchingProducts($_POST['s'], $settings['max-products-in-ajax-search']);
        if ($products) {
            foreach ($products as $product) {
                $pimage     = $product->get_image('getraffic-search-thumb');
                $price      = $product->get_price();
                $rprice     = $product->get_regular_price();
                $sprice     = $product->get_sale_price();
                ?>
                <div class="result-item">
                    <a href="<?php echo esc_url($product->get_permalink()); ?>">
                        <span class="pimage"><?= $pimage ?></span>
                        <span><?php echo $product->get_title(); ?></span>
                        <span class="price"><?php echo $product->get_price_html(); ?></span>
                    </a><br>
                </div>
            <?php }
        } else {
            echo '<h3 class="no-results">'. $settings['text-no-results'] .'</h3>';
        }
        exit;
    }

    public static function getSettingsPage()
    {
        /** Register Admin Settings Page */
        $section = new AdminSettingsSection();
        $section
            ->setName('settings-section')
            ->setTitle('Search Settings')
            ->setSectionInfo('Ajax Search Settings')
            ->setFields(
                [
                    (new AdminSettingsField())
                        ->setName('max-products-in-ajax-search')
                        ->setTitle('Max Products in AJAX Search ?'),
                    (new AdminSettingsField())
                        ->setName('text-no-results')
                        ->setTitle('No Results Text'),
                ]
            );

        return new AdminSettingsPage(
            self::SETTINGS_PAGE_ID,
            'Getraffic Ajax Search',
            'Getraffic Ajax Search',
            'menu-settings-gt-ajax',
            [$section]);
    }
}
