<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php get_template_part('templates/header/head'); ?>
<body <?php //body_class(); ?> <?php bloginfo('charset'); ?>>
<div class="no-padding no-margin header-container">
    <div class="sticky">
        <?php get_template_part('templates/header/topbar'); ?>
        <?php get_template_part('templates/header/menu/index'); ?>
    </div>
    <?php get_template_part('templates/header/slider'); ?>
</div>

<?php get_template_part('templates/header/script/additional'); ?>
