<?php
/**
 * Template Name: Questions
 *  *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>
<div class="contact-banner"><img src="<?php echo img('questions-banner.png'); ?>"/></div>
<div class="questions-container">
    <div class="questions-top">
        <div class="questions-menu"><?php get_template_part('templates/sidemenu/sidemenu'); ?></div>
        <div class="questions-blocks">
             <a href="<?= add_query_arg( array('key'=> 'delivery', 'q' => 'הזמנות ומשלוחים'), get_page_link() . '/questions-and-answers' ); ?>"><div class="question-block">
                <div class="question-image"><img src="<?php echo img('questions-delivery.png'); ?>"/></div>
                <div class="question-text"><p>הזמנות ומשלוחים</p></div>
            </div></a>
            <a href="<?= add_query_arg( array('key'=> 'replacement', 'q' => 'החלפות והחזרות'), get_page_link() . '/questions-and-answers' ); ?>"><div class="question-block">
                <div class="question-image"><img src="<?php echo img('questions-arrows.png'); ?>"/></div>
                <div class="question-text"><p>החלפות והחזרות</p></div>
            </div></a>
            <a href="<?= add_query_arg( array('key'=> 'benefit', 'q' => 'מועדון, הנחות והטבות'), get_page_link() . '/questions-and-answers' ); ?>"><div class="question-block">
                <div class="question-image"><img src="<?php echo img('questions-bag.png'); ?>"/></div>
                <div class="question-text"><p>מועדון, הנחות והטבות</p></div>
            </div></a>
            <a href="<?= add_query_arg( array('key'=> 'branches', 'q' => 'חנויות וסניפים'), get_page_link() . '/questions-and-answers' ); ?>"><div class="question-block">
                <div class="question-image"><img src="<?php echo img('questions-branches.png'); ?>"/></div>
                <div class="question-text"><p>חנויות וסניפים</p></div>
            </div></a>
            <a href="<?= add_query_arg( array('key'=> 'payment', 'q' => 'תשלום וגיפט קארד'), get_page_link() . '/questions-and-answers' ); ?>"><div class="question-block">
                <div class="question-image"><img src="<?php echo img('questions-payment.png'); ?>"/></div>
                <div class="question-text"><p>תשלום וגיפט קארד</p></div>
            </div></a>
            <a href="<?= add_query_arg( array('key'=> 'responsibility', 'q' => 'אחריות'), get_page_link() . '/questions-and-answers' ); ?>"><div class="question-block">
                <div class="question-image"><img src="<?php echo img('questions-responsibility.png'); ?>"/></div>
                <div class="question-text"><p>אחריות</p></div>
            </div></a>
        </div>
    </div>
<?php get_template_part('templates/contact/address'); ?>
</div>
<div style="clear:both"></div>
<?php get_footer(); ?>

<style>
    .contact-banner img{
        width: 100%;
    }
    .questions-top{
        
    }
    .questions-top .sidemenu{
        margin-top: 25px;
    }
    .questions-top .sidemenu-menu{
        width: auto;
        margin-right: 55px;
    }
    .questions-menu{
        width: 19%;
    float: right;
    }
    .questions-blocks{
        display: flex;
        justify-content: space-between;
        padding-top: 100px;
        flex-wrap: wrap;
        width: 1090px;
    }
    .question-block{
        background: #f6eff0;
        width: 340px;
        height: 175px;
        text-align: center;
        padding-top: 25px;
        margin-bottom: 40px;
    }
    .question-image img{
        width: 25%;
    }
    .question-text p{
        font-size: 14px;
    font-weight: bold;
    padding-top: 10px;
    }
    @media screen and (max-width: 768px){
        .questions-blocks {    
            width: auto;
            justify-content: center;
            padding-top: 30px;
        }
        .question-block {
            margin: 0 20px 40px;
        }
        .questions-menu {
            display: none;
        }
        .contact-icon {
            padding: 10px;
        }
        .contact-icon-image img {
            width: 60%;
        }
    }
    @media screen and (max-width: 480px){
        .contact-icon {
            width: 20%;
        }
        .contact-bottom-title {
            padding: 0 20%;
        }
        .question-block {
            margin: 0 0px 40px;
        }
        }
</style>