<?php
/**
 * Template Name: Club
 *  *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>
<div class="container club">
    <div class="content">
        <div class="row">
            <div class="col-lg-6 club-right">
                <img class="right-top-image" src="<?php echo gt_get_field('image_logo', 'option')['url']; ?>">
                <div class="club-content">    
                    <?php
                    if (have_posts()) {
                        while (have_posts()) : the_post();
                            ?>                
                            <?php the_content(); ?>
                        <?php
                        endwhile;
                    }
                    ?>
                </div>
            </div>
            <div class="col-lg-6 club-left">
                <img class="left-top-image" src="<?php echo gt_get_field('image_left_1', 'option')['url']; ?>" />
                
                <img class="left-bottom-image" src="<?php echo gt_get_field('image_left_2', 'option')['url']; ?>">                
            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>
<script type="text/javascript">
    document.addEventListener('wpcf7mailsent', function (event) {
        if ('374' == event.detail.contactFormId) {
            location = 'קלוב-תודה';
        }
    }, false);
</script>