let returnAction = 'returnItems';

jQuery(document).ready(function ($) {
    $('input.show-shipping-fields').change(function() {
        console.log(this.checked);
        $(".myaccount_shipping_fields").toggle(this.checked);
        $(".woocommerce .getraffic-edit-account").toggleClass('shippingfields-active');
    });

    $('.form-row-eye img').click(function(){
        if($(this).prevAll("input.woocommerce-Input:first").attr('type') == 'text'){
            $(this).prevAll("input.woocommerce-Input:first").attr('type', 'password');
            $(this).css('display', 'none');
            $(this).prev().css('display', 'block');
        }
        else{
            $(this).prevAll("input.woocommerce-Input:first").attr('type', 'text');
            $(this).css('display', 'none');
            $(this).next().css('display', 'block');

        }
    });

    $(".tm-orders").click(function(){
        $(this).next().toggleClass("active");
        return false;
    });
    $('button.replace').click(function () {
        $('.orders-to-return').hide();
        $('.orders-to-return').slideDown();
        returnAction = 'replaceItems';
        $('.returns-toptext').text('אנא בחרי את המוצר אותו תרצי להחליף');
    });
    $('button.return').click(function () {
        $('.orders-to-return').hide();
        $('.orders-to-return').slideDown();
        returnAction = 'returnItems';
        $('.returns-toptext').text('אנא בחרי את המוצר אותו תרצי להחזיר');
    });

    $('a.submit-return').bind('click', function () {
        let orderItems = [];
        $(this).parent().find('input:checked').each(function () {
            orderItems.push($(this).data('order-item-id'));
        });

        $('.resend-error').remove();

        if (!orderItems.length) {
            let message = 'יש לבחור את המוצר אותו תרצי להחזיר';
            if (returnAction === 'replaceItems') {
                message = 'יש לבחור את המוצר אותו תרצי להחליף';
            } else {
                message = 'יש לבחור את המוצר אותו תרצי להחזיר';
            }
            $('<div class="resend-error" style="color: red;">' + message + '</div>').insertAfter($(this));

            return false;
        }

        $.ajax({
            type: 'post',
            url: ajax_url,
            data: {
                action: returnAction,
                orderItems
            },
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {
                $('.returns-top').slideUp();
                $('.returns-thanks').css('display', 'block');
            },
        });
        return false;
    });

    $('p.save_address button[type=submit]').click(function () {

        let valid = true;
        $('div.myaccount_billing_fields p.form-row').each(function() {
            let current = validateFormElement($(this).find('input'));
             valid = valid && current;
        });

        if ($('.input-checkbox.show-shipping-fields').is(':checked')) {
            $('div.myaccount_shipping_fields p.form-row').each(function() {
                let current = validateFormElement($(this).find('input'));
                valid = valid && current;
            });
        }

        if (!valid) {
            return false;
        }
    });
});
