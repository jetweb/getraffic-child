function validateOptions() {

    if ($('.product #pa_color option').index("option:selected")==0) {

        $('.product #pa_color option:first-child').html('יש לבחור צבע');

        $('.product #pa_color').css('color', '#990000');

    } else {

        $('.product #pa_color').css('color', '#000000');

    }

    if ($('.product #pa_size option').index("option:selected")==0) {

        $('.product #pa_size option:first-child').html('יש לבחור מידה');

        $('.product #pa_size').css('color', '#990000');

    } else {

        $('.product #pa_size').css('color', '#000000');

    }

}



jQuery(document).ready(function ($) {

    $('.product #pa_color').change(validateOptions);

    $('.product #pa_size').change(validateOptions);

    $(document).on('added_to_cart', function(e) {

        console.log('product added to cart triggered');

    });

    $( document.body )

        .off( 'click', '.add_to_cart_button');

    $('.variations_form').on('wc_variation_form', function() {

        $(this).off('click', '.single_add_to_cart_button');

    });

    $(document).on('click', '.single_add_to_cart_button, .add_to_cart_button', function (e) {

        e.preventDefault();

        let errorElem = $('.variation-error');

        errorElem.text('');



        if ( $( this ).is('.disabled') ) {

            if ( $( this ).is('.wc-variation-is-unavailable') ) {

                errorElem.text( wc_add_to_cart_variation_params.i18n_unavailable_text );

            } else if ( $( this ).is('.wc-variation-selection-needed') ) {

                validateOptions();

               // errorElem.text( wc_add_to_cart_variation_params.i18n_make_a_selection_text );

            }



            return false;

        }

        

        var $thisbutton = $(this),

            $form = $thisbutton.closest('form.cart'),

            id = $thisbutton.val(),

            product_qty = $form.find('input[name=quantity]').val() || $thisbutton.data('quantity') || 1,

            product_id = $form.find('input[name=product_id]').val() || id || $thisbutton.data('product_id'),

            variation_id = $form.find('input[name=variation_id]').val() || 0;



        performAjaxAddToCart(product_id, product_qty, variation_id, $thisbutton, $form.find('input[name=cartItemToReplace]').val() || 0);



        return false;

    });    

});



function performAjaxAddToCart(product_id, product_qty, variation_id, $thisbutton, cartItemKey = 0) {

    var data = {

        action: 'woocommerce_ajax_add_to_cart',

        product_id: product_id,

        product_sku: '',

        quantity: product_qty,

        variation_id: variation_id,

        cartItemKey: cartItemKey

    };



    $(document.body).trigger('adding_to_cart', [$thisbutton, data]);



    $.ajax({

        type: 'post',

        url: wc_add_to_cart_params.ajax_url,

        data: data,

        beforeSend: function () {

            $thisbutton.removeClass('added').addClass('loading');

        },

        complete: function () {

            $thisbutton.addClass('added').removeClass('loading');

        },

        success: function (response) {



            if (response.error & response.product_url) {

                window.location = response.product_url;

                return;

            } else {

                $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);

            }

        },

    });

}



function updateCartItemQuantity(quantity, itemKey) {

    let data = {

        action: 'woocommerce_ajax_update_quantity',

        quantity: quantity,

        cart_item_key: itemKey

    };



    $.ajax({

        type: 'post',

        url: wc_add_to_cart_params.ajax_url,

        data: data,

        beforeSend: function () {

        },

        complete: function () {

        },

        success: function (response) {

            for (fragment in response.fragments) {

                $(fragment).replaceWith(response.fragments[fragment]);

            }

            $(document.body).trigger('wc_fragments_refreshed', [response.fragments, response.cart_hash]);

            bindMiniCartFunctions();

        },

    });

}

