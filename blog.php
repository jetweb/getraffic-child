<?php
/**
 * Template Name: Blog
 *  *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
//get_header();
?>
<div class="blog">
<?php get_template_part('templates/header/head'); ?>
<?php get_template_part('templates/single/header'); ?>
    
<?php
$object = get_queried_object();
$bgImage = gt_get_field('main_bg_image');
if (!$bgImage && is_object($object) && isset($object->term_id)) {
    $bgImage = gt_get_field('main_bg_image', 'product_cat_' . $object->term_id);
}
if ($bgImage['bg_image']) {
    ?>
    <div class="bg-image">
        <div class="align-center">
            <div class="align-center bg-container">
                <h1 class="bold">
                    <?php echo $bgImage['title']; ?>
                </h1>
                <span class="bold-text"><?php echo $bgImage['desc']; ?></span>
            </div>
        </div>
    </div>
    <?php
}
?>  
<?php
$instaFields = get_field('instagram');
$storyFields = get_field('our_story');
$followUs = get_field('follow_us');
?>
    <div class="blog-container">
        <div class="blog-content">
            <div class="blog-right">
                <div class="blog-content">    
                    <?php                        
                        get_template_part( 'templates/content/content');
                    ?>
                </div>
            </div>
            <div class="blog-left">
                <div class="blog-insta">
                    <div class="blog-insta-hashtag">#<?php echo $instaFields['insta-title'] ?></div>
                    <div class="blog-insta-image"><a href="<?php echo $instaFields['insta-link'] ?>"><img src="<?php echo $instaFields['insta-image'] ?>"></a></div>
                    <div class="blog-insta-dots"><img src="<?php echo img('insta_dots.png'); ?>"></div>
                </div>
                <div class="blog-story">
                    <div class="blog-story-image"><a href="<?php echo $storyFields['story_link'] ?>"><img src="<?php echo img('story.png'); ?>"></a></div>
                </div>
                <div class="blog-follow">
                    <div class="blog-follow-us-image"><img src="<?php echo $followUs['main_image']; ?>"></div>
                    <div class="blog-follow-us-title"><?php echo $followUs['title']; ?></div>
                    <div class="blog-follow-us-subtitle"><?php echo $followUs['subtitle']; ?></div>
                    <div class="blog-follow-us-form"><?php echo do_shortcode('[newsletter]'); ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_template_part('templates/single/footer'); ?>
<?php get_template_part('templates/footer/ajax-url'); ?>
<?php //get_footer(); ?>

<style>
    div.bg-image {
        background-image: url('<?php echo $bgImage['bg_image']['url']; ?>');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
    }
    .blog-story {
        background-image: url('<?php echo $storyFields['story-bg']; ?>');
    }       
    .blog-story:before {
        content: "";
        padding-top: 100%;
        float: left;
    }
    
        </style>
