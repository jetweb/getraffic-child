<?php
/**
 * Template Name: Questions
 *  *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>
<div class="questions-container">
    <div class="questions-top">        
        <div class="questions-menu"><?php get_template_part('templates/sidemenu/sidemenu'); ?></div>
        <div class="questions-blocks">
            <?php 
            $allquestions = get_field('questions');

            foreach ($allquestions as $key => $question){ ?>
                <?php $question_name = $question['name'];                                
                ?>
                <div class="question-block" id="<?php echo "tab_". ($key + 1); ?>">
                    <div class="question-image"><img src="<?php echo $question['image']; ?>"/></div>
                    <div class="question-text"><p><?php echo $question_name; ?></p></div>
                </div>
              <?php  } ?>
        </div>
    </div>
 </div>   
    
<div class="container question-container">
    <div class="row col-md-5 col-md-offset-2">
        <?php 
            foreach ($allquestions as $key => $questions){    ?>        
                <div class="questions" data-target="<?= "tab_". ($key + 1) ?>">
                    <div class="question-inside-image"><img src="<?php echo $questions['image']; ?>"/></div>
                    <div class="main-questions"><h3><?php echo $questions['name']; ?></h3></div>
                    <?php if($questions['question_group']){
                         foreach ($questions['question_group'] as $question){  ?>                             
                            <?php $i=1;                             
                            foreach ($question as $answer){  
                                echo ($i%2) ? "<div class='question'>" . $answer  . "<span class='questions-plus'>+</span><span class='questions-minus'>-</span></div>" : "<div  class='answer'>" . $answer . "</div>"; 
                                $i++;
                            }
                        }                            
                    } ?>
                    <a class="return" href="#">חזרה לכל השאלות<img src="<?php echo img('questions-return.png') ?>"></a>
                </div>
        <?php } ?>
    </div>
</div>
<div style="clear:both"></div>
<div class="questions-details"> 
    <?php get_template_part('templates/contact/address'); ?>
</div>   
<div style="clear:both"></div>
<?php get_footer(); ?>

<script>
    jQuery(document).ready(function(){
        let $ = jQuery;
        $('.questions').hide();
        
        jQuery('.question-block').click(function(){        
            var selectedTopic = $(this).attr('id');
            $('.questions-blocks').addClass('hide');
            $('.questions-blocks').removeClass('show');
            $('.questions').hide();
            $('.questions-details').hide();
            $('.questions[data-target=' + selectedTopic + ']').show();        
        }); 
        
        $(".question").click(function(){              
            if (!$(this).hasClass("active")){            
                $('.answer').hide();
                $('.question').removeClass("active");
                $(this).toggleClass ('active');
                $(this).next().toggle('fast'); 
            }
            else{                
                $('.question').removeClass("active");
                $(this).next().toggle('fast'); 
            }            
        });       
                
        $(".return").click(function(){   
            $('.questions-blocks').addClass('show');
            $('.questions-blocks').removeClass('hide');
            $('.questions').hide();
            $('.questions-details').show();
        });
    });
</script> 
        
<style>
    .questions-blocks.show{
        display: flex;
    }
    .questions-blocks.hide{
        display: none;
    }
    .questions{
        width: 100%;
    }
    .questions .return{
        text-align: center;
        display: block;
        color: #ccc;
        text-decoration: underline;
        margin: 20px 0 50px;
    }
    .questions .return img{
        margin-right: 7px;
    }
    .contact-banner img{
        width: 100%;
    }
    .questions-top{
        
    }
    .questions-top .sidemenu{
        margin-top: 25px;
    }
    
    .questions-menu{
        width: 19%;
    float: right;
    }
    .questions-blocks{
        display: flex;
        justify-content: space-between;
        padding-top: 100px;
        flex-wrap: wrap;
        width: 1090px;
    }
    .question-block{
        background: #f6eff0;
        width: 340px;
        height: 175px;
        text-align: center;
        padding-top: 25px;
        margin-bottom: 40px;
        cursor: pointer;
    }
    .question-image, .question-inside-image{
        width: 85px;
        text-align: center;
        height: 85px;
        margin: auto;
        background: #fff;
        border-radius: 50%;
        position: relative;
    }
    .question-image{
        background: #fff;
    }
    .question-inside-image{
        background: #f6eff0;
    }
    .question-image img, .question-inside-image img {
        width: 50%;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }
/*
    .question-image img{
        width: 25%;
    }
*/
    .question-text p{
        font-size: 14px;
    font-weight: bold;
    padding-top: 10px;
    }
    @media screen and (max-width: 768px){
        .questions-blocks {    
            width: auto;
            justify-content: center;
            padding-top: 30px;
        }
        .question-block {
            margin: 0 20px 40px;
        }
        .questions-menu {
            display: none;
        }
        .contact-icon {
            padding: 10px;
        }
        .contact-icon-image img {
            width: 60%;
        }
    }
    @media screen and (max-width: 480px){
        .contact-icon {
            width: 20%;
        }
        .contact-bottom-title {
            padding: 0 20%;
        }
        .question-block {
            margin: 0 0px 40px;
        }
        }
</style>
