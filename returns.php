<?php
/**
 * Template Name: Returns
 *  *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>
<div class="sidemenu-banner"><img src="<?php echo img('returns-banner.png'); ?>"/>
    <h1 class="page-title"><span class="returns-title"><?php echo the_title(); ?></span><br><span class="returns-subtitle"><?php echo get_field('subtitle'); ?></span></h1></div>
<div class="returns-container">
    <div class="returns-menu"><?php get_template_part('templates/sidemenu/sidemenu'); ?></div>
    <div class="returns-top">
        <?php if (!is_user_logged_in()) { ?>
            <div class="woocommerce-MyAccount-user">
                <img src="<?php echo img('person.png'); ?>">
                <h3>כניסה לחברות רשומות</h3>
            </div>
            <form class="woocommerce-EditAccountForm woocommerce-form woocommerce-form-login login" method="post">
                <?php do_action('woocommerce_login_form_start'); ?>
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" placeholder="כתובת מייל *"
                           value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>"/><?php // @codingStandardsIgnoreLine ?>
                </p>
                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide form-row-eye">
                    <input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password"
                           placeholder="סיסמא *"/>
                    <img class="closed-eye" src="<?php echo img('closed-eye.png'); ?>"><img class="opened-eye" src="<?php echo img('opened-eye.png'); ?>">
                </p>
                <?php do_action('woocommerce_login_form'); ?>
                <p class="form-row form-login-submit">
                    <?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
                    <button type="submit" class="woocommerce-Button button" name="login" value="<?php esc_attr_e('Log in', 'woocommerce'); ?>"><?php esc_html_e('כניסה',
                            'woocommerce'); ?></button>
                </p>
                <?php do_action('woocommerce_login_form_end'); ?>
            </form>
        <?php } else { ?>
            <h3 class="hi">היי, <?php echo wp_get_current_user()->first_name; ?></h3>
            <h4>החלפות והחזרות חינם תוך 30 יום! אנא בחרי:</h4>
            <div class="returns-buttons">
                <button type="" class="return" name="" value=""><?php esc_html_e('רוצה להחזיר', 'woocommerce'); ?></button>
                <button type="" class="replace" name="" value=""><?php esc_html_e('רוצה להחליף ', 'woocommerce'); ?></button>
            </div>
            <div class="orders-to-return">
            <?php
            $customer_orders = wc_get_orders(
                apply_filters(
                    'woocommerce_my_account_my_orders_query',
                    array(
                        'customer' => get_current_user_id(),
                        'paginate' => false,
                    )
                )
            );

            foreach ( $customer_orders as $customer_order ) :
                $order      = wc_get_order( $customer_order );
                $item_count = $order->get_item_count();
                ?>
                <div class="grey">
                    <div class="woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr( $order->get_status() ); ?> order tm-orders">
                        <?php foreach ( wc_get_account_orders_columns() as $column_id => $column_name ) : ?>
                            <?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
                                <?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

                            <?php elseif ( 'order-number' === $column_id ) : ?>

                                <div class="tm-order-item">
                                    <div class="tm-order-title">מספר הזמנה</div>
                                    <div class="tm-order-number"><?php echo $order->get_order_number(); ?></div>
                                    <div class="tm-order-status"><span>תאריך: </span><time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time>&nbsp;|&nbsp;
                                        <span>סטטוס: </span><?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?></div>
                                </div>

                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="tm-order-details">
                        <div colspan="5">
                            <?php do_action( 'woocommerce_view_order', $order->get_order_number() ); ?>
                        </div>
                        <p>
                            לאחר הגעת המוצר למשרדנו ובדיקתו ניצור עמכם קשר
                            ותישלח הודעה על הזיכוי לכתובת המייל המעודכנת בחשבונכם.
                        </p>
                        <a href="#" class="submit-return">
                            שליחה
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        <?php } ?>
    </div>
    <img src="<?php echo img('returns-thanks.png') ?>" alt="" class="returns-thanks">
    <?php
    get_template_part('templates/contact/address');
    ?>
</div>
<div style="clear:both"></div>
<?php get_footer(); ?>
<style>
    td.woocommerce-table__product-checkbox.product-checkbox {
        display: table-cell !important;
    }
    td.woocommerce-table__product-checkbox.product-checkbox input[type="checkbox"] {
        -webkit-appearance: checkbox;
    }
    .contact-banner img {
        width: 100%;
    }

    .sidemenu-banner h1 {
        line-height: 20px;
        text-align: center;
    }
    .orders-to-return
    {
        width: 60%;
        margin: auto;
        display: none;
    }
    .returns-thanks {
        margin: auto;
        text-align: center;
        display: none;
    }
    table.order-customer-details, .order-details-summary {
        display: none;
    }

    a.submit-return {
        background: black;
        color: white;
        padding: 15px 30px;
        display: inline-block;
        margin: 0 0 20px;
    }

    .resend-error {
        color: red;
    }
</style>
<script>
    jQuery(document).ready(function () {
        let $ = jQuery;
        $('.form-row-eye img').click(function () {
            if ($(this).prevAll("input.input-text").attr('type') == 'text') {
                $(this).prevAll("input.input-text").attr('type', 'password');
                $(this).css('display', 'none');
                $(this).prev().css('display', 'block');
            } else {
                $(this).prevAll("input.input-text").attr('type', 'text');
                $(this).css('display', 'none');
                $(this).next().css('display', 'block');
            }
        });

        $(".tm-orders").click(function(){
            $(this).next().toggleClass("active");
            return false;
        });
        $('button.replace').click(function () {
            debugger;
            $('.orders-to-return').hide();
            $('.orders-to-return').slideDown();
            $('.returns-toptext').text('אנא בחרי את המוצר אותו תרצי להחליף');
        });
        $('button.return').click(function () {
        debugger;
            $('.orders-to-return').hide();
            $('.orders-to-return').slideDown();
            $('.returns-toptext').text('אנא בחרי את המוצר אותו תרצי להחזיר');
        });

        $('a.submit-return').bind('click', function () {
            debugger;
            let orderItems = [];
            $(this).parent().find('input:checked').each(function () {
                orderItems.push($(this).data('order-item-id'));
            });

            $.ajax({
                type: 'post',
                url: ajax_url,
                data: {
                    action: 'returnItems',
                    orderItems
                },
                beforeSend: function () {
                },
                complete: function () {
                },
                success: function (response) {
                    if (response === 'success') {
                        $('.returns-top').slideUp();
                        $('.returns-thanks').css('display', 'block');
                    } else {
                        alert(response);
                    }
                },
            });
            return false;
        });
    });
</script>

