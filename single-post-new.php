<?php
/**
 * Template Name: Single post
 *
 * The template for displaying pages without the sidebar
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */

//get_header(); ?>
<div class="single-post">
<?php get_template_part('templates/header/head'); ?>
<?php get_template_part('templates/single/header'); ?>

<div class="post-container" id="primary">
    <div class="content">
        <?php
        if (have_posts()) {
            while (have_posts()) : the_post();
                ?>
                <div class="row margin-bottom">
                    
                    <div class="col-sm-12">
                        <div class="post-wrapper">
                            <h2><?php the_title(); ?></h2>
                            <h3><?php echo get_field('subtitle'); ?></h3>
                            <div class="post-date"><?php echo get_the_date('d.m.Y'); ?></div>
                            <div class="post-content"><?php the_content(); ?></div>
                        </div>
                        <?php comments_template(); ?>

                        <?php get_template_part('templates/single/post_text_icons'); ?>
                    </div>
                </div>
            <?php
            endwhile;
        }
        ?>
    </div>
</div>
<?php get_template_part('templates/single/footer'); ?>
</div>
<style>
    .single-post .container{
        max-width: 1371px;
        margin-left: 86px;
    }
.single-post .container .topicon.we-are-here{
        margin-top: 20px;
    }
    .topicon.location{
        display: none
    }
    .topbar {
        position: relative;
        background: #f4ebe5;
        height: 70px;
    }
    .post-container{
        margin: 75px 140px 0;
    }
    .post-container .content{
        padding: 27px 190px 0;
        background: #f8f8f8;
    }
    .post-container .content ul.wp-block-gallery{
        direction: ltr;
        margin-top: 40px;
    }
    .post-container .content h2{
        font-size: 17px;
    }
    .post-container .content h3{
        font-size: 14px;
        border-bottom: 1px solid #000;
        padding-bottom: 14px;
    }
    .post-container .content .post-date{
        font-size: 26px;
        line-height: 26px;
    }
    .post-container .content .post-content{
        margin-top: 35px;
    }
    .post-container .content .post-content p{        
        font-size: 13px;
        line-height: 16px;
    }
    .post-container .content .post-content img{
        margin-bottom: 16px;
    }
    .post-container .content .post-content ul{
        margin-bottom: 0;
    }
    .single-footer{
        display: flex;
        justify-content: center;
        flex-direction: row-reverse;
        padding: 25px 0 20px;
        background: #f4ebe5;
    }
    .single-footer-icon{
        padding: 0 15px;
    }
    .single-postfooter{
        text-align: center;
        padding: 20px 0;
        font-size: 13px;
        font-weight: 600;
    }
    .post_text{
        display: flex;
        justify-content: space-between;
        border-bottom: 1px solid #000;
        border-top: 1px solid #000;
        padding: 25px 0;
    }
    .post_text_link a, .post_text_icons{
        display: flex;
        flex-direction: row-reverse;
        align-items: center;
    }
    .post_text-icon{
        padding-right: 15px;
    }
    .post_text_link img{
        padding-left: 15px;
    }
</style>