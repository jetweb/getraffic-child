<?php
/**
 * Customer Reset Password email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s: Customer first name */ ?>
<p>היי,
<p>המייל הזה הגיע אלייך כי קיבלנו בקשה לאפס את הסיסמה עבור החשבון של <?php printf( esc_html__( '%s', 'woocommerce' ), esc_html( $user_login ) ); ?></p>
<p><?php esc_html_e( 'If you didn\'t make this request, just ignore this email. If you\'d like to proceed:', 'woocommerce' ); ?><a class="link" href="<?php echo esc_url( add_query_arg( array( 'key' => $reset_key, 'id' => $user_id ), wc_get_endpoint_url( 'lost-password', '', wc_get_page_permalink( 'myaccount' ) ) ) ); ?>"><?php // phpcs:ignore ?>
		כאן תוכלי ליצור סיסמה חדשה
	</a>
</p>
<p><strong>שימי לב – </strong>הלינק ליצירת סיסמה חדשה יהיה בתוקף במשך 24 שעות, כדי לשמור על אבטחת החשבון שלך.</p>
<p>נתראה באתר,</p>
<p>צוות בי יוניק</p>

<?php do_action( 'woocommerce_email_footer', $email ); ?>
