<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php 
	$user = get_user_by('login', $user_login );
?>
<p class="wellcome">היי <?php echo $user->first_name; ?>,</p>
<p>איזה כיף שהצטרפת לאתר בי יוניק! כבר הרבה יותר נחמד לנו כשאת פה.</p>
<p>אנחנו מאמינים שמי שאוהבת שנוח לה כבר ממש לא צריכה להתפשר על סטייל – בגלל זה הקמנו את בי יוניק, ומאז יותר ויותר נשים ישראליות הולכות על האיכות שלנו.</p>

<p>שם המשתמשת שלך הוא: <?php echo $user->user_email; ?> והוא ישמש אותך כדי להתחבר</p>
<p>עכשיו כשיש לך חשבון באתר, תוכלי:</p>
<ul>
<li>לבצע הזמנות בקלות ובמהירות</li>
<li>ליהנות מדגמים בלעדיים לאתר</li>
<li>לעקוב אחרי הזמנות קודמות או כאלה שעדיין בתהליך</li>
<li>לשמור פריטים אהובים לאורך זמן ולגשת אליהם מכל מקום</li>
</ul>
<a href="https://www.b-unique.co.il/my-account/">כאן תוכלי לצפות בפרטי החשבון שלך ולשנות פרטים אם תרצי</a>

<p style="margin: 16px 0 0;">נתראה באתר,</p>
<p>צוות בי יוניק</p>


<?php do_action( 'woocommerce_email_footer', $email );
