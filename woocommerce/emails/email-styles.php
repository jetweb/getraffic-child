<?php
/**
 * Email Styles
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-styles.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load colors.
$bg        = get_option( 'woocommerce_email_background_color' );
$body      = get_option( 'woocommerce_email_body_background_color' );
$base      = get_option( 'woocommerce_email_base_color' );
$base_text = wc_light_or_dark( $base, '#202020', '#ffffff' );
$text      = get_option( 'woocommerce_email_text_color' );

// Pick a contrasting color for links.
$link_color = wc_hex_is_light( $base ) ? $base : $base_text;

if ( wc_hex_is_light( $body ) ) {
	$link_color = wc_hex_is_light( $base ) ? $base_text : $base;
}

$bg_darker_10    = wc_hex_darker( $bg, 10 );
$body_darker_10  = wc_hex_darker( $body, 10 );
$base_lighter_20 = wc_hex_lighter( $base, 20 );
$base_lighter_40 = wc_hex_lighter( $base, 40 );
$text_lighter_20 = wc_hex_lighter( $text, 20 );

// !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
// body{padding: 0;} ensures proper scale/positioning of the email in the iOS native email app.
?>

body {
	padding: 0;
background: #f4f4f4;
direction: rtl;
}

body * {
font-family: 'Tahoma', 'Varela Round', sans-serif;
}
#wrapper {
	background-color: <?php echo esc_attr( $bg ); ?>;
	margin: 0;
	padding: 30px 0;
	-webkit-text-size-adjust: none !important;
	width: 100%;
}

#template_container {
    _border: 1px solid rgba(183, 159, 95, 1);
    _background: url(https://buntest.getraffic.dev/wp-content/themes/getraffic-child/assets/images/border-c.png) no-repeat -1px 0px;
}

table#template_container #template_header tr {
    _background: url(https://buntest.getraffic.dev/wp-content/themes/getraffic-child/assets/images/border-c.png) no-repeat;
    _background-position: center bottom;
}


#template_header {
	color: <?php echo esc_attr( $base_text ); ?>;
	border-bottom: 0;
	font-weight: bold;
	line-height: 100%;
	vertical-align: middle;
}

#template_header h1 {    
    max-width: 300px;
    position: relative;
    line-height: 125%;
    margin: 0 auto;
    
}

_#template_header h1:after {
    content: "";
    display: block;
    background: url(http://buntest.getraffic.dev/wp-content/themes/getraffic-child/assets/images/gold.png) no-repeat;
    width: 139px;
    height: 82px;
    position: absolute;
    left: -111px;
    top: -44px;
    background-size: contain;
    }
    

#template_header h1,
#template_header h1 a {
	color: #000;
}

#template_footer td {
	padding: 0;
	border-radius: 6px;
	color: #212529;
	font-size: 15px;
	text-align: right;
	height: 123px
}

#template_footer #credit {
	border: 0;
	line-height: 125%;
	text-align: right;
	padding: 0px;
	color: #ffffff;
    font-size: 15px;
}

#template_footer #credit div {
	background: #000;
	padding: 1%;
	width: 47%;
    height: 140px;
}

.social a {
    display: inline-block;
}

.social p {
    text-align: center;
}


#body_content {
}

#body_content table td {
	padding: 16px 0px 0;
}

div#body_content_inner {
	padding: 0 5% 5px;
	text-align: right;
}


#body_content table td td {
	padding: 12px;
}

#body_content table td th {
	padding: 12px;
}

#body_content td ul.wc-item-meta {
	font-size: small;
	margin: 1em 0 0;
	padding: 0;
	list-style: none;
}

#body_content td ul.wc-item-meta li {
	margin: 0.5em 0 0;
	padding: 0;
}

#body_content td ul.wc-item-meta li p {
	margin: 0;
}

#body_content p {
	margin: 0 0 16px;
	color: #000;
}

.gs li {
    margin-left: 15px;
    color: #000 !important;
}

#body_content a {
    color: #d3b4aa !important;
    font-weight: bold !important;
}

#body_content_inner {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	font-size: 14px;
	line-height: 150%;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

.td {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	border: none;
	vertical-align: middle;
}

.address {
	padding: 0 10px;
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	border: none;
	font-style: normal !important;
    line-height: 26px;
}

.text {
	color: <?php echo esc_attr( $text ); ?>;
}

.link {
	color: #000;
	text-decoration: underline;
}

#header_wrapper {
	padding: 16px;
	display: block;
	background: url(http://buntest.getraffic.dev/wp-content/themes/getraffic-child/assets/images/gold.png) no-repeat;
    background-size: 26% auto;
    padding-top: 25px;
    margin-top: -10px;
}

}

h1 {
	color: #000;
	font-size: 30px;
	font-weight: 300;
	line-height: 150%;
	margin: 0;
	text-align: center;
	text-shadow: 0 1px 0 <?php echo esc_attr( $base_lighter_20 ); ?>;
}

h2 {
	color: #212529;
	display: block;
	font-size: 16px;
	font-weight: bold;
	line-height: 30px;
	margin: 0 0 10px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

h2 a.link {
	color: #212529;
	font-weight: bold;
	}

h3 {
	color: #000;
	display: block;
	font-size: 16px;
	font-weight: bold;
	line-height: 130%;
	margin: 16px 0 8px;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
}

a {
	color: <?php echo esc_attr( $link_color ); ?>;
	font-weight: normal;
	text-decoration: underline;
}

img {
	border: none;
	display: inline-block;
	font-size: 14px;
	font-weight: bold;
	height: auto;
	outline: none;
	text-decoration: none;
	text-transform: capitalize;
	vertical-align: middle;
	margin-<?php echo is_rtl() ? 'left' : 'right'; ?>: 10px;
}

img.wp-post-image {
    width: 80px;
    height: 80px;
}

table.td tfoot {
    background: #fff;
    width: 100%;
}

div#wrapper {
    margin: 0 auto !important;
    float: none;
    display: block;
    background: #f4f4f4;
    width: 600px;
    max-width: 100%;
}


p.totals {
    text-align: center;
    margin: 0px 0px !important;
    font-weight: normal;
}

.td tfoot th.td {
    padding: 5px !important;
}

.td tfoot tr:first-child th.td {
    padding-top: 10px !important;
}

.td tfoot tr:last-child th.td {
    padding-bottom: 10px !important;
}

.td tfoot tr:last-child th.td p.totals {
    font-weight: bold;
}

.footer-icons {
display: block;
}


.footer-icons p img {
    margin-left:0;
}

.footer-menu-icons {
    text-align: left;
    margin: 0px 10px;
}

table#template_footer {
    background: none;
    padding: 0px;
}

.footer-icons p, .footer-icons a {
    color: #fff !important;
    text-align: right;
    margin-bottom: 0px;
}

.footer-icons p {
   float: right;
    margin-top: 0;
}

.footer-menu-icons a {
    margin: 0 5px;
}


#template_container, #template_header, #template_body, #template_footer {
    width: 100%;
    max-width: 600px;
}

img.bism {
    width: 150px;
    float: right;
    margin-bottom: 20px;
}

#template_footer a, #template_footer, #template_footer td {
	color: #fff;
}

ul.wc-item-meta {
    display: none;
}



<?php
