<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}
$currency_symbol = get_woocommerce_currency_symbol( get_woocommerce_currency() );

if (empty($product)) {
    return;
}


?>

<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'woocommerce-table__line-item order_item', $item, $order ) ); ?>">

    <td class="woocommerce-table__product-checkbox product-checkbox" style="display: none;">
        <input type="checkbox" class="selected-order-item" data-order-item-id="<?= $item_id ?>" />
    </td>
	<td class="woocommerce-table__product-name product-name">
		<?php
			$is_visible        = $product && $product->is_visible();
			$product_permalink = apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $product->get_permalink( $item ) : '', $item, $order );
            echo $product->get_image('thumbnail');
			
		?>
	</td>

	<td class="woocommerce-table__product-total product-total">
        <?php        

			do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order, false );

			wc_display_item_meta( $item );

			do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order, false );
        ?>
        <div class="product-order-details">
            <div><?php echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $product -> get_title() ) : $product -> get_title(), $item, $is_visible );?></div>

            <?php $quantity = apply_filters( 'woocommerce_order_item_quantity_html', ' <span class="product-quantity order-quantity">' . sprintf( 'כמות: %s', $item->get_quantity() ) . '</span>', $item );?>

            <div class="tm-order-variation">
            <?php
            if ($item->get_variation_id()) {
                $var = new WC_Product_Variation($item->get_variation_id());
                foreach ($var->get_attributes() as $attr => $value) {
                    $term = get_term_by('slug', $value, urldecode($attr));
                    ?>
                    <?php echo wc_attribute_label($attr); ?>:&nbsp;<?php echo urldecode($value); ?>&nbsp;|&nbsp;
                    <?php
                }   
                echo $quantity; ?>
<!--
                <div class="cart-item-atts returns-variations">
                    <?php 
                    foreach ($var->get_attributes() as $attr => $value) {
                        echo '<select>';
                        $attr  = urldecode($attr);
                        $realAttr      = str_replace('pa_', '', $attr);
                        $terms = wc_get_product_terms($var->get_parent_id(), $attr, ['fields' => 'all']);

                        foreach ($terms as $term) {
                            $realName = str_replace('-', ' ', urldecode($value));
                            $isSelected = false;
                            if ($term->name === $realName) {
                                $quickViewProductIds[] = [$var->get_parent_id(),$cart_item_key];
                                $attachment_id = absint(get_term_meta($term->term_id, 'product_attribute_image', true));
                                $image_size    = woo_variation_swatches()->get_option('attribute_image_size');
                                $image_url     = wp_get_attachment_image_url($attachment_id, apply_filters('wvs_product_attribute_image_size', $image_size));

                                $isSelected    = true;
                            }
                            echo '<option '. ($isSelected ? 'selected="selected"' : '') .'>'. $realAttr . ' | ' . $term->name . '</option>';
                        }
                        echo '</select>';
                        } ?>
                </div>
-->
           <?php }
            
            ?>
            </div>
        </div>
	</td>
    <td class="woocommerce-table__product-price product-price">
        <div>
            <?php echo $order->get_formatted_line_subtotal( $item ); ?>
            <?php //echo '<span class="order-regular-price">' . $currency_symbol . $product->get_regular_price() . '</span>'; ?>
            <?php //echo '<span class="order-price">' . $currency_symbol . $product->get_price() . '</span>'; ?>
        </div>
    </td>

</tr>

<?php if ( $show_purchase_note && $purchase_note ) : ?>

<tr class="woocommerce-table__product-purchase-note product-purchase-note">

	<td colspan="2"><?php echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); ?></td>

</tr>

<?php endif; ?>
