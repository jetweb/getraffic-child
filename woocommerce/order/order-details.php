<?php
/**
 * Order details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.5.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if ( ! $order = wc_get_order( $order_id ) ) {
	return;
}

$order_items           = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();
$show_downloads        = $order->has_downloadable_item() && $order->is_download_permitted();
$currency_symbol       = get_woocommerce_currency_symbol( get_woocommerce_currency() );

if ( $show_downloads ) {
	wc_get_template( 'order/order-downloads.php', array( 'downloads' => $downloads, 'show_title' => true ) );
}
?>
<section class="woocommerce-order-details">
	<?php do_action( 'woocommerce_order_details_before_order_table', $order ); ?>

	<h2 class="woocommerce-order-details__title"><?php _e( 'Order details', 'woocommerce' ); ?></h2>

	<table class="woocommerce-table woocommerce-table--order-details shop_table order_details">

		<thead>
			<tr>
				<th class="woocommerce-table__product-name product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
				<th class="woocommerce-table__product-table product-total"><?php _e( 'Total', 'woocommerce' ); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php
			do_action( 'woocommerce_order_details_before_order_table_items', $order );

			foreach ( $order_items as $item_id => $item ) {
				$product = $item->get_product();

				wc_get_template( 'order/order-details-item.php', array(
					'order'			     => $order,
					'item_id'		     => $item_id,
					'item'			     => $item,
					'show_purchase_note' => $show_purchase_note,
					'purchase_note'	     => $product ? $product->get_purchase_note() : '',
					'product'	         => $product,
				) );
			}

			do_action( 'woocommerce_order_details_after_order_table_items', $order );
			?>
            
		</tbody>

		<tfoot>                        
			<?php if ( $order->get_customer_note() ) : ?>
				<tr>
					<th><?php _e( 'Note:', 'woocommerce' ); ?></th>
					<td><?php echo wptexturize( $order->get_customer_note() ); ?></td>
				</tr>
			<?php endif; ?>
		</tfoot>
	</table>
    <table class="order-customer-details">
        <tr>
            <td class="order-customer-title">פרטי משלוח</td>
            <td class="order-customer-names">
                <div>שם:</div>
                <div>כתובת:</div>
                <div>עיר:</div>
                <div class="orders-padding">
                    <div>טלפון נייד:</div>
                    <div>סוג משלוח:</div>
                </div>
            </td>
            <td class="order-customer-values">
                <div class="woocommerce-customer-details--phone"><?php echo esc_html( $order->get_shipping_first_name() ); ?>&nbsp;<?php echo esc_html( $order->get_shipping_last_name() ); ?></div>
                <div class="woocommerce-customer-details--phone"><?php echo esc_html( $order->get_shipping_address_1() ); ?></div>
                <div class="woocommerce-customer-details--phone"><?php echo esc_html( $order->get_shipping_city() ); ?></div>
                <div class="orders-padding">
                    <div class="woocommerce-customer-details--phone"><?php echo esc_html( $order->get_billing_phone() ); ?></div>
                    <div class="woocommerce-customer-details--phone"><?php echo esc_html( $order->get_shipping_method() ); ?></div>
                </div>
            </td>
        </tr>
    </table>
    
	<?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?>
</section>
<div class="order-details-summary">
        <p>סיכום ביניים: <?php echo $currency_symbol . $order->get_subtotal(); ?></p> 
<!--        <p>הטבות מועדון / קוד קופון: <?php echo $currency_symbol . $order->get_discount_total(); ?></p>-->
<!--        <p>משלוח: <?php //echo $currency_symbol . $order->get_total_shipping(); ?></p>-->
        <p>משלוח: <?php echo ($order->get_total_shipping()>0 ? $currency_symbol . $order->get_total_shipping() : 'חינם'); ?></p>

        <p class="order-details-summary-total">סה״כ לתשלום: <?php echo $currency_symbol . $order->get_total(); ?></p>
    </div>
<?php
if ( $show_customer_details ) {
	wc_get_template( 'order/order-details-customer.php', array( 'order' => $order ) );
}
