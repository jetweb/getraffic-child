<?php

/**

 * Wishlist page template

 *

 * @author Your Inspiration Themes

 * @package YITH WooCommerce Wishlist

 * @version 2.0.12

 */



if ( ! defined( 'YITH_WCWL' ) ) {

	exit;

} // Exit if accessed directly

?>



<?php do_action( 'yith_wcwl_before_wishlist_form', $wishlist_meta ); ?>



    <?php wp_nonce_field( 'yith-wcwl-form', 'yith_wcwl_form_nonce' ) ?>



    <!-- TITLE -->

    <?php

    do_action( 'yith_wcwl_before_wishlist_title', $wishlist_meta );



    if( ! empty( $page_title ) ) :

    ?>

        <div class="wishlist-title <?php echo ( $is_custom_list ) ? 'wishlist-title-with-form' : ''?>">

            <?php echo apply_filters( 'yith_wcwl_wishlist_title', '<h2>' . $page_title . '</h2>' ); ?>

            <?php if( $is_custom_list ): ?>

                <a class="btn button show-title-form">

                    <?php echo apply_filters( 'yith_wcwl_edit_title_icon', '<i class="fa fa-pencil"></i>' )?>

                    <?php _e( 'Edit title', 'yith-woocommerce-wishlist' ) ?>

                </a>

            <?php endif; ?>

        </div>

        <?php if( $is_custom_list ): ?>

            <div class="hidden-title-form">

                <input type="text" value="<?php echo $page_title ?>" name="wishlist_name"/>

                <button>

                    <?php echo apply_filters( 'yith_wcwl_save_wishlist_title_icon', '<i class="fa fa-check"></i>' )?>

                    <?php _e( 'Save', 'yith-woocommerce-wishlist' )?>

                </button>

                <a class="hide-title-form btn button">

                    <?php echo apply_filters( 'yith_wcwl_cancel_wishlist_title_icon', '<i class="fa fa-remove"></i>' )?>

                    <?php _e( 'Cancel', 'yith-woocommerce-wishlist' )?>

                </a>

            </div>

        <?php endif; ?>

    <?php

    endif;



     do_action( 'yith_wcwl_before_wishlist', $wishlist_meta ); ?>



    

            



            

        <?php

        if( count( $wishlist_items ) > 0 ) :

            ?><div class="products woocommerce-wishlist-content"><?php

            $added_items = array();

            foreach( $wishlist_items as $item ) :

                global $product;

            

	            $item['prod_id'] = yit_wpml_object_id ( $item['prod_id'], 'product', true );



	            if( in_array( $item['prod_id'], $added_items ) ){

		            continue;

	            }



	            $added_items[] = $item['prod_id'];

	            $product = wc_get_product( $item['prod_id'] );

	            $availability = $product->get_availability();

	            $stock_status = isset( $availability['class'] ) ? $availability['class'] : false;

                $quickproduct = get_query_var('productToRender');



                if( $product && $product->exists() ) {

                    GT::renderProduct($product, $showControls = true, $template = 'wishlist-product', $spreadVariations = false);

                    //GT::renderProduct($product);

                }

            endforeach;

            ?></div><?php

        else: ?>

            <div class="wishlist-empty full-width align-center">

                <img src="<?php echo gt_get_field('image'); ?>" alt=""><br />

                <p style="display: none;"><?php echo gt_get_field('text'); ?></p>

		<p><?php echo gt_get_field('text'); ?></p>

                <a href="<?php echo gt_get_field('button_link'); ?>" class="wishlist-back"><?php echo gt_get_field('button_text'); ?></a>

            </div>

            <?php $bestSellers = gt_get_field('homepage_best_sellers', 25); ?>

            <div class="container best-sellers-container wishlist-empty-container">

                <h1 class="nothing-found-text">you may like</h1>

                <?php

                $products = [];

                foreach ($bestSellers['best_sellers'] as $post) {

                    $products[] = wc_get_product($post->ID);

                }

                GT::renderCrossSellProducts(

                    '', '', $products

                );

                ?>

            </div>

        <?php

        endif;?>





<?php

$posts = gt_get_field('wishlist_xsell', 'option');

if ($posts) {

    $products = [];

    foreach ($posts as $post) {

        $products[] = wc_get_product($post->ID);

    }

    GT::renderCrossSellProducts('!WANT IT? GET IT', 'הוסיפי את המוצרים ל-WISHLIST שלך', $products);

}

?>