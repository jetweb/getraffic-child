<?php
defined('ABSPATH') || exit;
do_action('woocommerce_before_single_product');
GT::enqueueAsset('product-js', '/assets/src/js/product.js?v=1.0.0', ['jquery'], true, true, '202006141356');
if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.

    return;
}
global $gtStoredProductID;
$gtStoredProductID = get_queried_object_id();
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
    <div class="container">
        <div class="row">
            <?php get_template_part('templates/product/content/details'); ?>
            <?php get_template_part('templates/product/content/images/index'); ?>
        </div>
    </div>
    <?php
    global $product;
    $args     = [
        'post_type'      => 'product',
        'post_status'    => 'publish',
        'posts_per_page' => '5',
        'post__not_in'   => [$product->get_id()],
        'tax_query'      => [
            [
                'taxonomy' => 'product_cat',
                'field'    => 'term_id',
                'terms'    => $product->get_category_ids(),
                'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
            ],
            [
                'taxonomy' => 'product_visibility',
                'field'    => 'slug',
                'terms'    => 'exclude-from-catalog', // Possibly 'exclude-from-search' too
                'operator' => 'NOT IN',
            ],
        ],
    ];
    $query    = new WP_Query($args);
    $products = [];
    foreach ($query->get_posts() as $post) {
        $products[] = wc_get_product($post->ID);
    }
    //$xsellProducts = array_filter(array_map('wc_get_product', $product->get_cross_sell_ids()), 'wc_products_array_filter_visible');
    $xsellProducts  = $products;
    $recentProducts = GT::getRecentlyViewedProducts();
	if (!is_array($recentProducts)) {
		if (stristr($recentProducts,'you')) {
			$recentProducts=array();
		} 
	}
    $productsToShow = (count($xsellProducts) > count($recentProducts)) ? 'xsell' : 'recents';
    wp_reset_query();
    ?>
    <div class="xsell-selector">
        <div class="recents-title xsell-selector-item <?php echo $productsToShow == 'recents' ? 'active' : ''; ?>" onclick="toggleXsellItem('recents');">
            <h1 aria-level="2" class="best-match">מוצרים שראית לאחרונה</h1>
            <!--            <h2 class="best-match">מוצרים שראית לאחרונה</h2>-->
        </div>
        <img src="<?php echo img('list-sep.png'); ?>" alt="">
        <div class="xsell-title xsell-selector-item <?php echo $productsToShow == 'xsell' ? 'active' : ''; ?>" onclick="toggleXsellItem('xsell');">
            <h1 aria-level="2" class="best-match">אולי יעניינו אותך...</h1>
            <!--            <h2 class="best-match">אולי תאהבו</h2>-->
        </div>
    </div>
    <div class="xsell-content toggle-content <?php echo $productsToShow == 'xsell' ? '' : 'hidden'; ?>">
        <?php GT::renderCrossSellProducts('', '', $xsellProducts, 'thumbnail', false, 'slick-xsell'); ?>
    </div>
    <div class="recents-content toggle-content <?php echo $productsToShow == 'recents' ? '' : 'hidden'; ?>">
        <?php GT::renderCrossSellProducts('', '', $recentProducts, 'thumbnail', false, 'slick-recents');; ?>
    </div>
    <?php get_template_part('templates/product/content/reviews'); ?>
</div>
<?php do_action('woocommerce_after_single_product'); ?>
<input id="altCandidate" type="hidden" value="<?= $product->get_title(); ?>">
<script>
    let lastSelectedSize = null;

    function toggleXsellItem(contentName, elem) {
        jQuery('.toggle-content').hide();
        jQuery('.' + contentName + '-content').show();
        jQuery('.xsell-selector-item').removeClass('active');
        jQuery('.' + contentName + '-title').addClass('active');
        jQuery('#slick-' + contentName).slick('unslick');
        slickInitializers['slick-' + contentName]();
    }

    jQuery(document).ready(function () {
        let urlParams = new URLSearchParams(window.location.search);
        let selectedColor = urlParams.get('attribute_pa_color');
        let selectedSize = urlParams.get('attribute_pa_size');
        // $('select#pa_color').val(selectedColor);
        // $('select#pa_size').val(selectedSize);
        if (!selectedSize && $('select#pa_size').val()) {
            selectedSize = $('select#pa_size').val();
        }
        if (!selectedColor && $('select#pa_color').val()) {
            selectedColor = $('select#pa_color').val();
        }
        setTimeout(function () {
            try {
                if (selectedColor) {
                    $('li[data-value="' + selectedColor + '"]').trigger('click');
                } else {
                    $('ul[data-attribute_name=attribute_pa_color] li:first').trigger('click');
                }
                if (selectedSize) {
                    $('li[data-value=' + selectedSize + ']').trigger('click');
                } else {
                    $('ul[data-attribute_name=attribute_pa_color] li:first').trigger('click');
                }
                window.scrollTo(0, 0);
                console.log(selectedColor, selectedSize);
            } catch (Err) {
            }
            //$('.product-gallery, .mobile-gallery').show();
            resetWishlsit();
            jQuery(window).resize();
            jQuery(window).trigger('resize');
            setTimeout(function () {
                jQuery('.product-gallery, .mobile-gallery').show();
                jQuery(window).resize();
                jQuery(window).trigger('resize');
                jQuery(document).trigger('resize');
            }, 400);
        }, 1800);
        jQuery(document).on('wvs-selected-item', function (e, value, select, element) {
            jQuery('li').removeClass('oos');
            let variations = jQuery('.product form.variations_form').data('product_variations');
            if (jQuery('ul.variable-items-wrapper').length === 1) {
                for (let variation of variations) {
                    let attrName = jQuery('ul.variable-items-wrapper').data('attribute_name');
                    let value = variation['attributes'][attrName];
                    let html = variation['availability_html'];
                    if (html.indexOf('out-of-stock') !== -1) {
                        jQuery('ul[data-attribute_name=' + attrName + '] li[data-value=' + value + ']').addClass('oos');
                    }
                }
            } else {
                setTimeout(function () {
                    let selectedSize = jQuery('ul[data-attribute_name=attribute_pa_color] li.selected').data('value');
                    if (selectedSize) {
                        for (let variation of variations) {
                            if (variation.attributes.attribute_pa_color == selectedSize) {
                                let html = variation['availability_html'];
                                let colorValue = variation['attributes']['attribute_pa_size'];
                                if (html.indexOf('out-of-stock') !== -1) {
                                    jQuery('ul[data-attribute_name=attribute_pa_size] li[data-value="' + colorValue + '"]').addClass('oos');
                                }
                            }
                        }
                    }
                    // Reselect last size if color changed
                    if (!Number.isInteger(value) && lastSelectedSize) {
                        lastSelectedSize.trigger('click');
                    }
                    // Store newly selected size
                    if (Number.isInteger(value)) {
                        lastSelectedSize = jQuery('ul[data-attribute_name=attribute_pa_size] li[data-value=' + value + ']');
                    }
                }, 50);
            }
            fixBuggyZoomPlugin();
            //        .flex-viewport figure div img {
            //            margin-left: 200px;
            //            width: 680px !important;
            //        }

                //jQuery('.flex-viewport figure div.flex-active-slide').css('paddingLeft', jQuery('ol.flex-control-thumbs').width());
        }); // Custom Event for li

        pushAltCandidate();
        $(document).on('wc_additional_variation_images_frontend_image_swap_callback', pushAltCandidate);
    });
    jQuery('form.variations_form').on('wc_additional_variation_images_frontend_image_swap_done_callback', resetWishlsit);
    jQuery(window).click(fixBuggyZoomPlugin);


    function resetWishlsit() {
        if (jQuery(window).width() > 991) {
            setTimeout(function () {
                jQuery('.woocommerce-product-gallery .whishlist').appendTo('.flex-viewport');
                jQuery('.woocommerce-product-gallery .whishlist').css('opacity', '1');
                jQuery('.woocommerce-product-gallery .whishlist').css('z-index', '1000');
            }, 1000);
            if(navigator.userAgent.search("Firefox")) {
                if (jQuery(window).width() > 768) {
                    jQuery('.flex-viewport figure div img').width(jQuery('.flex-viewport').width()).css('marginLeft', '200px');
                }
            }
        }
    }

    function fixBuggyZoomPlugin() {
        jQuery('.zoomWindowContainer').remove();
        jQuery('.tintContainer').remove();
        setTimeout(function () {
            let image = jQuery('div.flex-active-slide img');
            if (typeof (IZ) !== 'undefined') {
                image.image_zoom(IZ.options);
            }
        }, 800);
    }

    function pushAltCandidate() {
        setTimeout(function() {
            let j = jQuery;
            let altCandidate = j('#altCandidate').val();
            j('figure img').attr('alt', altCandidate);
            j('figure img').attr('title', altCandidate);
            j('.iz_watermark').attr('title', altCandidate);
            j('.zoomWindowContainer').attr('title', altCandidate);

        }, 2000);

    }
</script>
<?php
if (true) { ?>
    <style>
        .product-gallery, .mobile-gallery {
            display: none;
        }
    </style>
<?php }

?>
<?php if(false): ?>
<!--script type="application/ld+json">

{

  "@context": "https://schema.org/",

  "@type": "Product",

  "name": "<?php the_title(); ?>",

  "image": ["<?php echo wp_get_attachment_url($product->get_image_id()); ?>",    "<?php
global $product;
$attachment_ids = $product->get_gallery_attachment_ids();
foreach ($attachment_ids as $attachment_id) {
    echo $image_link = wp_get_attachment_url($attachment_id) . ', ';
} ?>"],

  "description": "<?php echo apply_filters('the_excerpt', $product->post->post_excerpt); ?>>",

  "sku": "<?php echo $product->get_sku(); ?>",

  "mpn": "<?php echo $product->get_sku(); ?>",

  "brand": "name": "B.unique Shoes",

  "offers": {

    "@type": "Offer",

    "url": "<?php echo get_permalink(); ?>",

    "priceCurrency": "ILS",

    "price": "<?php echo $product->get_price(); ?>",

    "priceValidUntil": "<?php echo $product->get_date_on_sale_to(); ?>",

    "itemCondition": "https://schema.org/UsedCondition",

    "availability": "https://schema.org/InStock",

    "seller": {

      "@type": "Organization",

      "name": "B.unique Shoes"

    }

  }

}

</script--!>
<?php endif; ?>
<style>
    .oos {
        background: /*linear-gradient(to top left,*/ /*rgba(0,0,0,0) 0%,*/ /*rgba(0,0,0,0) calc(50% - 0.8px),*/ /*rgba(0,0,0,1) 50%,*/ /*rgba(0,0,0,0) calc(50% + 0.8px),*/ /*rgba(0,0,0,0) 100%),*/ linear-gradient(to top right,
        rgba(100, 100, 100, 0) 0%,
        rgba(100, 100, 100, 0) calc(50% - 0.8px),
        rgba(100, 100, 100, 1) 50%,
        rgba(100, 100, 100, 0) calc(50% + 0.8px),
        rgba(100, 100, 100, 0) 100%);
    }

    .image-variable-item {
        padding: 3px !important;
    }

    @media screen and (min-width: 769px) {
        .woocommerce-product-gallery .flex-viewport {
            /*width: 680px !important;*/
        }
        /*.flex-viewport figure div img { width: 680px !important; marginLeft: 200px !important; }*/
    }
</style>

