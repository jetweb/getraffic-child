<?php

/**

 * The Template for displaying product archives, including the main shop page which is a post type archive

 *

 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.

 *

 * HOWEVER, on occasion WooCommerce will need to update template files and you

 * (the theme developer) will need to copy the new files to your theme to

 * maintain compatibility. We try to do this as little as possible, but it does

 * happen. When this occurs the version of the template file will be bumped and

 * the readme will list any important changes.

 *

 * @see     https://docs.woocommerce.com/document/template-structure/

 * @package WooCommerce/Templates

 * @version 3.4.0

 */

defined('ABSPATH') || exit;

get_header('shop'); ?>



<?php 

$term = get_queried_object();

?>



<div class="mobile_banner_image"><a href="<?php echo get_field('mobile_category_banner', $term)['mobile_category_banner_link']; ?>"><img src="<?php echo get_field('mobile_category_banner', $term)['mobile_category_banner_image']; ?>"></a><h1 class="bold"><?php echo get_field('mobile_category_banner', $term)['mobile_category_banner_title']; ?></h1><p class="mobile_category_description"><?php echo get_field('mobile_category_banner', $term)['mobile_category_banner_text']; ?></p></div>

    <div class="container category">

        <?php

            get_template_part('templates/category/header');

            get_template_part('templates/category/search-bar');

            get_template_part('templates/category/loop');

            get_template_part('templates/category/upsell');



            do_action('woocommerce_after_shop_loop');

            do_action('woocommerce_after_main_content');

        ?>

    </div>

<?php

get_template_part('templates/category/script/index');

get_template_part('templates/category/script/additional');

do_action('woocommerce_sidebar');

get_footer('shop');



