<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
    <?php $dashboard = get_field('dashboard', 'option'); ?>    
	<div class="woocommerce-MyAccount-main dashboard">        
		<img src="<?php echo img('myaccount-frame.png'); ?>">
		<div class="myaccount-text"><p><?php echo $dashboard['text']; ?></p></div>
		<div class="myaccount-subtext"><p><?php echo $dashboard['subtext']; ?></p></div>
	</div>

<style>
    div.dashboard {
        background: url(<?php echo $dashboard['image']; ?>);
    }
</style>