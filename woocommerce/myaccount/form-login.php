<?php
/**
 * Template Name: Login
 *  *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
GT::enqueueAsset('account-css', '/assets/src/scss/account.css', []);
GT::enqueueAsset('account-js', '/assets/src/js/account.js', ['jquery'], true, true);

$loginFields = gt_get_field('my_account_login', 'option');

do_action( 'woocommerce_before_customer_login_form' );
?>
<div class="login-container">
        <div class="woocommerce-MyAccount-user">            
                <img src="<?php echo $loginFields['icon']?>">
                <h3><?php echo $loginFields['login_text']?></h3>
        </div>
        
        <form class="woocommerce-EditAccountForm woocommerce-form woocommerce-form-login login" method="post">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

            <?php
                if (isset($_REQUEST['return_url'])) {
                    ?>
                    <input type="hidden" name="return_url" value="<?=$_REQUEST['return_url'];?>">
                    <?php
                }
            ?>

			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide validate validate-required validate-email " invalid-email-message="<?= $loginFields['invalid_email_text'] ?>">
                <label for="username" class="username">כתובת מייל *</label>
				<input type="text" dir="ltr" style="text-align: left;direction: ltr" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" autocomplete="username" placeholder="כתובת מייל *" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
			</p>
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide form-row-eye validate validate-required">
                <label for="password" class="password">סיסמא *</label>
				<input class="validate required woocommerce-Input woocommerce-Input--text input-text" style="visibility:visible!important;" type="password" name="password" id="password" autocomplete="current-password" placeholder="סיסמא *"/>
                <img class="closed-eye" src="<?php echo img('closed-eye.png'); ?>"><img class="opened-eye" src="<?php echo img('opened-eye.png'); ?>">
			</p>

			<?php do_action( 'woocommerce_login_form' ); ?>
            <p class="woocommerce-LostPassword lost_password">
                <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php echo $loginFields['forgot_pass']; ?></a>
            </p>
			<p class="form-row form-login-submit">
				<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
				<button type="submit" class="woocommerce-Button button" name="login"><?php echo $loginFields['button_text']?></button>
			</p>

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>
</div>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
