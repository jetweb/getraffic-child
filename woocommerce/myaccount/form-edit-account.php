<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

$fields = gt_get_field('my_account_edit_account', 'option');
do_action( 'woocommerce_before_edit_account_form' ); ?>
<div class="getraffic-edit-account">
    <div class="woocommerce-MyAccount-user">
        <img src="<?php echo $fields['icon']; ?>">
        <h3><?php echo $fields['title']; ?></h3>
    </div>
    <form class="woocommerce-EditAccountForm edit-account" action="" method="post" <?php do_action('woocommerce_edit_account_form_tag'); ?> >
        <?php do_action('woocommerce_edit_account_form_start'); ?>
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first validate validate-required validate-no-numbers">
            <label for="account_first_name"><?php esc_html_e('שם פרטי', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
            <input type="text" empty-message="אין אפשרות לעדכן את החשבון בלי שם פרטי" invalid-numbers-message="אפשר להשתמש פה רק באותיות בעברית או באנגלית" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" autocomplete="given-name"
                   value="<?php echo esc_attr($user->first_name); ?>"/>
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last validate validate-required validate-no-numbers">
            <label for="account_last_name"><?php esc_html_e('שם משפחה', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
            <input type="text" empty-message="אין אפשרות לעדכן את החשבון בלי שם משפחה" invalid-numbers-message="אפשר להשתמש פה רק באותיות בעברית או באנגלית" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" autocomplete="family-name"
                   value="<?php echo esc_attr($user->last_name); ?>"/>
        </p>
        <div class="clear"></div>

        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide validate validate-required validate-email">
            <label for="account_email"><?php esc_html_e('כתובת מייל', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
            <input type="email" empty-message="אין אפשרות לעדכן את כתובת המייל לכתובת ריקה" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email"
                   value="<?php echo esc_attr($user->user_email); ?>"/>
        </p>
        <!--	<fieldset>-->
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide current_password form-row-eye">
            <label for="password_current"><?php esc_html_e('סיסמא', 'woocommerce'); ?></label>
            <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" autocomplete="off"/>
            <img class="closed-eye" src="<?php echo img('closed-eye.png'); ?>"><img class="opened-eye" src="<?php echo img('opened-eye.png'); ?>">
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first new_password form-row-eye validate-length">
            <label for="password_1"><?php esc_html_e('סיסמא חדשה', 'woocommerce'); ?></label>
            <input type="password" min-length="6" invalid-length-message="הסיסמה צריכה להיות מורכבת מ-6 תווים לפחות"  class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" autocomplete="off"/>
            <img class="closed-eye" src="<?php echo img('closed-eye.png'); ?>"><img class="opened-eye" src="<?php echo img('opened-eye.png'); ?>">
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last confirm_new_password form-row-eye">
            <label for="password_2"><?php esc_html_e('אימות סיסמא חדשה', 'woocommerce'); ?></label>
            <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" autocomplete="off"/>
            <img class="closed-eye" src="<?php echo img('closed-eye.png'); ?>"><img class="opened-eye" src="<?php echo img('opened-eye.png'); ?>">
        </p>
        <!--	</fieldset>-->
        <div class="clear"></div>
        <?php do_action('woocommerce_edit_account_form'); ?>
        <p class="save_address">
            <?php wp_nonce_field('save_account_details', 'save-account-details-nonce'); ?>
            <button type="submit" class="woocommerce-Button button" name="save_account_details"
                    value="<?php esc_attr_e('Save changes', 'woocommerce'); ?>"><?php echo $fields['button_text']; ?></button>
            <input type="hidden" name="action" value="save_account_details"/>
        </p>
        <?php do_action('woocommerce_edit_account_form_end'); ?>
    </form>


<?php do_action( 'woocommerce_after_edit_account_form' ); ?>

</div>
<script>
    jQuery(document).ready(function($) {
        $('select.country_select').val('IL');

        $('button[name=save_account_details]').click(function() {
            let valid = true;
            $('p.form-row').each(function(){
                let current = validateFormElement($(this).find('input'));
                valid = valid && current;
            });

            if ($('#password_1').val().length && $('#password_2').val().length) {
                if ($('#password_current').val().length === 0) {
                    $('#password_current').closest('.form-row').removeClass('woocommerce-validated').addClass('woocommerce-invalid');
                    $('#password_current').closest('.form-row').attr('error-message', 'יש להזין את הסיסמא הנוכחית');
                    valid = false;
                }

                if ($('#password_1').val() !== $('#password_2').val()) {
                    $('#password_2').closest('.form-row').removeClass('woocommerce-validated').addClass('woocommerce-invalid');
                    $('#password_2').closest('.form-row').attr('error-message', 'הסיסמה לא תואמת לסיסמה בשדה הקודם, בבקשה נסי שוב');
                    valid = false;
                }
            } else if ($('#password_1').val() && $('#password_2').val().length === 0) {
                $('#password_2').closest('.form-row').removeClass('woocommerce-validated').addClass('woocommerce-invalid');
                $('#password_2').closest('.form-row').attr('error-message', 'רק עוד פעם אחת בשביל לוודא... תודה :)');
                valid = false;
            }

            if (!valid) {
                return false;
            }
        });
    })
</script>
