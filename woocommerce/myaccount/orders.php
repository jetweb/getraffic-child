<?php

/**

 * Orders

 *

 * Shows orders on the account page.

 *

 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.

 *

 * HOWEVER, on occasion WooCommerce will need to update template files and you

 * (the theme developer) will need to copy the new files to your theme to

 * maintain compatibility. We try to do this as little as possible, but it does

 * happen. When this occurs the version of the template file will be bumped and

 * the readme will list any important changes.

 *

 * @see     https://docs.woocommerce.com/document/template-structure/

 * @author  WooThemes

 * @package WooCommerce/Templates

 * @version 3.2.0

 */

if (!defined('ABSPATH')) {

    exit;

}

$fields = gt_get_field('my_account_orders', 'option');



do_action('woocommerce_before_account_orders', $has_orders); ?>

    <div class="woocommerce-MyAccount-orders">

        <img src="<?php echo $fields['icon']; ?>">

        <h3><?php echo $fields['title']; ?></h3>

    </div>

    <?php if ($has_orders) : ?>

        <?php

        $customer_orders = wc_get_orders(

            apply_filters(

                'woocommerce_my_account_my_orders_query',

                array(

                    'customer' => get_current_user_id(),

                    'paginate' => false,

                )

            )

        );

        //var_dump($customer_orders);

        foreach ($customer_orders as $customer_order) :

            $order = wc_get_order($customer_order);

            $item_count = $order->get_item_count();

            ?>

            <div class="woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr($order->get_status()); ?> order tm-orders">

                <?php foreach (wc_get_account_orders_columns() as $column_id => $column_name) : ?>

                    <?php if (has_action('woocommerce_my_account_my_orders_column_' . $column_id)) : ?>

                        <?php do_action('woocommerce_my_account_my_orders_column_' . $column_id, $order); ?>

                    <?php elseif ('order-number' === $column_id) : ?>

                        <div class="tm-order-item">

                            <div class="tm-order-title">מספר הזמנה</div>

                            <div class="tm-order-number"><?php echo $order->get_order_number(); ?></div>

                            <div class="tm-order-status"><span>תאריך: </span>

                                <time datetime="<?php echo esc_attr($order->get_date_created()->date('c')); ?>"><?php echo esc_html(wc_format_datetime($order->get_date_created())); ?></time>&nbsp;|&nbsp;

                                <span>סטטוס: </span><?php echo esc_html(wc_get_order_status_name($order->get_status())); ?></div>

                        </div>

                    <?php endif; ?>

                <?php endforeach; ?>

            </div>

            <div class="tm-order-details">

                <div colspan="5">

                    <?php do_action('woocommerce_view_order', $order->get_id()); ?>

                </div>

            </div>

        <?php endforeach; ?>

        <img src="<?php echo img('border-c.png') ?>">

        

        <div class="myaccount-orders-link">

            <span>

                <?php echo $fields['returns_text']; ?>

            </span>

            <a href="<?php echo wc_get_account_endpoint_url('returns'); ?>"><?php echo $fields['click_here']; ?></a>

        </div>



        <?php do_action('woocommerce_before_account_orders_pagination'); ?>

        <?php if (1 < $customer_orders->max_num_pages) : ?>

            <div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination">

                <?php if (1 !== $current_page) : ?>

                    <a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button"

                       href="<?php echo esc_url(wc_get_endpoint_url('orders', $current_page - 1)); ?>"><?php _e('Previous', 'woocommerce'); ?></a>

                <?php endif; ?>



                <?php if (intval($customer_orders->max_num_pages) !== $current_page) : ?>

                    <a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button"

                       href="<?php echo esc_url(wc_get_endpoint_url('orders', $current_page + 1)); ?>"><?php _e('Next', 'woocommerce'); ?></a>

                <?php endif; ?>

            </div>

        <?php endif; ?>

    <?php else : ?>

       <div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info no-orders-yet">
		<p>אין פה הזמנות עדיין</p>
		<p>מחפשת רעיונות?<br>
		 בואי לראות <a href="/category/new-arrival/">מה חדש באתר</a>

	</div>
<?php endif; ?>



    <?php do_action('woocommerce_after_account_orders', $has_orders); ?>

