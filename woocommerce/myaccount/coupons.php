<?php
// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}
$fields = gt_get_field('my_account_coupons', 'option');
?>
<div class="getraffic-edit-account">
    <div class="account-coupons">
        <div class="account-coupons-top">
            <img src="<?php echo $fields['icon']; ?>">
            <h3><?php echo $fields['title']; ?></h3>
        </div>
        <div class="account-coupons-content">
            <?php get_template_part('templates/account/coupons'); ?>
        </div>
    </div>
</div>
