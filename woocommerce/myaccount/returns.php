<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */

$fields = gt_get_field('my_account_returns', 'option');
?>
<div class="returns-container">
    <div class="returns-top">

            <h3 class="hi"><?= str_replace('[username]', wp_get_current_user()->first_name, $fields['hi']);  ?></h3>
            <h4><?= $fields['returns_30_day_text'] ?></h4>
            <div class="returns-buttons">
                <button class="return" name="" value=""><?php echo $fields['want_to_return']; ?></button>
                <button class="replace" name="" value=""><?php echo $fields['want_to_replace']; ?></button>
            </div>
            <div class="orders-to-return">
                <div class="returns-toptext"><?php echo $fields['choose_product']; ?></div>
            <?php
            $customer_orders = wc_get_orders(
                apply_filters(
                    'woocommerce_my_account_my_orders_query',
                    array(
                        'customer' => get_current_user_id(),
                        'paginate' => false,
                    )
                )
            );

            foreach ( $customer_orders as $customer_order ) :
                $order      = wc_get_order( $customer_order );
				$order_status = $order->get_status();
				//print_r($order_status);
				if ($order_status == 'completed' || $order_status == 'service-completed') {
                $item_count = $order->get_item_count();
                ?>
                <div class="grey">
                    <div class="woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr( $order->get_status() ); ?> order tm-orders">
                        <?php foreach ( wc_get_account_orders_columns() as $column_id => $column_name ) : ?>
                            <?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
                                <?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

                            <?php elseif ( 'order-number' === $column_id ) : ?>

                                <div class="tm-order-item">
                                    <div class="tm-order-title">מספר הזמנה</div>
                                    <div class="tm-order-number"><?php echo $order->get_order_number(); ?></div>
                                    <div class="tm-order-status"><span>תאריך: </span><time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time>&nbsp;|&nbsp;
                                        <span>סטטוס: </span><?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?></div>
                                </div>

                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="tm-order-details">
                        <div colspan="5">
                            <?php do_action( 'woocommerce_view_order', $order->get_id() ); ?>
                        </div>
                        <p>
                            <?php echo $fields['after_arrival']; ?>
                        </p>
                        <a href="#" class="submit-return">
                            <?php echo $fields['send']; ?>
                        </a>
                    </div>
                </div>
								<?php } ?>
            <?php endforeach; ?>
            </div>
        
    </div>
    <div class="returns-thanks">
        <div class="contact-thankyou-imagetop"><img src="<?php echo img('contactus-topimage.png') ?>"></div>
        <p style="text-align:center" class="contact-thankyou-texttop">תודה על שיצרת איתנו קשר</p>
        <p style="text-align:center" class="contact-thankyou-textbottom"><?php echo $fields['we_will_contact_you']; ?></p>
        <div class="contact-thankyou-button"><a href="/"><?php echo $fields['keep_shopping']; ?></a></div>
        <div class="contact-thankyou-imagebottom"><img src="<?php echo img('contactus-bottomimage.png') ?>"></div>
    </div>
<!--    <img src="<?php //echo img('returns-thanks.png') ?>" alt="" class="returns-thanks">-->
    <?php
    get_template_part('templates/contact/address');
    ?>
</div>