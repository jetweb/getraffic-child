<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.2.0
 */
if (!defined('ABSPATH')) {
    exit;
}
$currency_symbol = get_woocommerce_currency_symbol(get_woocommerce_currency());
GT::enqueueAsset('checkout', '/assets/src/scss/checkout.css', [], false, true);
\Getraffic\GTM::pushOrderCompleted($order);



?>
<div style="display: none;">
    <?php
    do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() );
    do_action( 'woocommerce_thankyou', $order->get_id() );
    ?>
</div>
    <div class="contact-banner">
        <?php if (wp_is_mobile()) { ?>
            <img src="<?php the_field('top_image_thankyou_mob', 'option'); ?>"/>
        <?php } else { ?>
            <img src="<?php the_field('top_image_thankyou', 'option'); ?>"/>
        <?php } ?>
    </div>
    <div class="order-container ">

<?php if ($order->has_status(['failed'])) : ?>
    <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.',
            'woocommerce'); ?></p>
    <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
        <a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>" class="button pay"><?php _e('Pay', 'woocommerce') ?></a>
        <?php if (is_user_logged_in()) : ?>
            <a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>" class="button pay"><?php _e('My account', 'woocommerce'); ?></a>
        <?php endif; ?>
    </p>
<?php else : ?>
    <div class="tnx-text col-md-8 col-md-offset-2 col-sm 12">
        <h1>
            היי
            <span><?php echo $order->get_billing_first_name(); ?></span>
        </h1>
        <p>
            הזמנתך התקבלה
        </p>
        <div class="tnx-order-num">
            מספר הזמנה:
            <span><?php echo $order->get_order_number(); ?></span>
        </div>
        <p class="tnx-mail-sent">
            <strong>
                אישור הזמנה נשלח למייל:
                <span>

                    <?php echo $order->get_billing_email(); ?>

                </span>
            </strong>
        </p>
    </div>
    </div>
    <div style="clear:both"></div>
    <div class="order-container orders">
        <div class="tnx-order col-md-8 col-md-offset-2 col-sm 12">
            <?php
            $order_items = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));
            foreach ($order_items as $item_id => $item) {
                echo '<div class="tnx-item">';
                $product = $item->get_product();
                GT::getProductMainImageHTML($product);
                echo '<div class="blocks">';
                wc_get_template('order/order-details-item.php', [
                    'order' => $order,
                    'item_id' => $item_id,
                    'item' => $item,
                    'show_purchase_note' => false,
                    'purchase_note' => $product ? $product->get_purchase_note() : '',
                    'product' => $product,
                ]);
                echo '</div>';
                echo '</div>';
            }
            ?>
        </div>
    </div>
    <div style="clear:both"></div>
    <?php $user_street = $order->get_meta('_billing_street'); ?>
    <?php $user_house = $order->get_meta('_billing_house'); ?>
    <?php $user_appartment = $order->get_meta('_billing_appartment'); ?>
<?php $user_floor = $order->get_meta('_billing_floor'); ?>
    <div class="order-container delivery">
        <div class="tnx-section col-md-9 col-md-offset-3 col-sm-12">
            <h1 class="tnx-order-title col-md-2">
                פרטי משלוח
            </h1>
            <div class="tnx-section-right shipping-labels col-md-10">
                <div class="tnx-shipping-item">
                    <div class="col-md-3 col-xs-4">שם :</div>
                    <div class="col-md-9 col-xs-8"><?php echo $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(); ?></div>
                </div>
                <?php if ($user_street != 'no' && $order->get_shipping_method() !== 'איסוף עצמי מחנויות הרשת') { ?>
                    <div class="tnx-shipping-item">
                        <div class="col-md-3 col-xs-4">כתובת :</div>
                        <div class="col-md-9 col-xs-8"><?php
                            echo $user_street;
                            if (!empty($user_house) && $user_house != 'no') {
                                echo ' ' . $user_house;
                            }
                            if (!empty($user_appartment) && $user_appartment != 'no') {
                                echo ', דירה ' . $user_appartment;
                            }
                            if (!empty($user_floor) && $user_floor != 'no') {
                                echo ', קומה ' . $user_floor;
                            }
                            ?></div>
                    </div>
                <?php } ?>

                <?php if ($order->get_billing_city()) { ?>
                    <div class="tnx-shipping-item">
                        <div class="col-md-3 col-xs-4">עיר :</div>
                        <div class="col-md-9 col-xs-8"><?php echo $order->get_billing_city(); ?></div>
                    </div>
                <?php } ?>
                <div class="orders-padding">
                    <div class="tnx-shipping-item">
                        <div class="col-md-3 col-xs-4">טלפון נייד :</div>
                        <div class="col-md-9 col-xs-8"><?php echo $order->get_billing_phone(); ?></div>
                    </div>
                    <div class="tnx-shipping-item">
                        <div class="col-md-3 col-xs-4">סוג משלוח :</div>
                        <div class="col-md-9 col-xs-8"><?php echo $order->get_shipping_method(); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="clear:both"></div>
    <div class="order-container summary-total">
        <div class="tnx-section col-md-6 col-md-offset-3 col-sm 12">
            <div class="order-details-summary">
                <p>סיכום ביניים: <?php echo  wc_price($order->get_subtotal()); ?></p>
                <?php
                if ($order->get_discount_total() > 0) {
					$discounts=$order->get_discount_total();
					$smart_coupon = get_post_meta($order->get_id(),'smart_coupons_contribution','true');
					if (is_array($smart_coupon)) {
						foreach ($smart_coupon as $key => $coupon) {
						$discounts= $discounts + $coupon;
						}
					}
                    ?>
                    <p>הנחות וקופונים: <?php echo  wc_price($discounts); ?></p>
                    <?php
                }
                ?>
                <p>משלוח: <?php echo $order->get_shipping_method(); ?>&nbsp;|&nbsp;<?php echo wc_price($order->get_total_shipping()); ?></p>
                <p class="order-details-summary-total">סה״כ לתשלום: <?php echo wc_price($order->get_total()); ?></p>
            </div>
        </div>
    </div>
    <div style="clear:both"></div>
    <div class="order-container">
        <div class="tnx-section-contact col-md-6 col-md-offset-3 col-sm 12">
            <div class="tnx-section-orders-questions">
                <a href=/questions/">
                    <img src="<?php echo img('q-icon.png') ?>">
                    <p>שאלות ותשובות</p>
                </a>
            </div>
            <div class="tnx-section-orders-exchange">
                <a href="/returns-2/">
                    <img src="<?php echo img('r-icon.png') ?>">
                    <p>מדיניות החזרות והחלפות</p>
                </a>
            </div>
        </div>
    </div>
    <div style="clear: both"></div>
<?php endif; ?>

<div class="mailchimp-response" style="display: none;">
<?php
var_dump($order->get_meta('mailchimp_response'));
//var_dump((new \Getraffic\MailChimp())->subscribe($order->get_billing_email(), $order->get_billing_first_name(), $order->get_billing_last_name(), '', true));
?>
</div
