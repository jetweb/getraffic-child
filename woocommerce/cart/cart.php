<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */
defined('ABSPATH') || exit;
wc_print_notices();
GT::enqueueAsset('cart-js', '/assets/src/js/cart.js', ['jquery'], true, true);
do_action('woocommerce_before_cart');
?>

<div class="row cart-container">
    <div class="col-lg-7">
        <?php get_template_part('templates/cart/upsell-top'); ?>
        <form class="woocommerce-cart-form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
            <?php do_action('woocommerce_before_cart_table'); ?>
            <div class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
                <?php do_action('woocommerce_before_cart_contents'); ?>
                <?php get_template_part('templates/cart/cart-items-content'); ?>
                <?php do_action('woocommerce_cart_contents'); ?>

                <div colspan="6" class="actions">
                    <button type="submit" class="button" name="update_cart" style="display:none;" value="<?php esc_attr_e('Update cart', 'woocommerce'); ?>"><?php esc_html_e('Update cart', 'woocommerce'); ?></button>
                    <?php do_action('woocommerce_cart_actions'); ?>
                    <?php wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce'); ?>
                </div>
                <?php do_action('woocommerce_after_cart_contents'); ?>
            </div>
            <?php do_action('woocommerce_after_cart_table'); ?>
        </form>
    </div>
    <div class="col-lg-5">
        <div class="cart-collaterals"><?php do_action('woocommerce_cart_collaterals'); ?></div>
        <div class="benefits-icons"><img src="<?php echo img('new-benefits-icons1.png') ?>" /><p class="to-terms">*בכפוף לתקנון</p></div>
    </div>
</div>

<?php get_template_part('templates/cart/upsell-bottom'); ?>

<?php
// Render quickviews if necessary for the attribute update option
global $quickViewProductIds;
if (!empty($quickViewProductIds)) {
    foreach ($quickViewProductIds as $quickViewProduct) {
        query_posts(['p' => $quickViewProduct[0], 'post_type' => 'product']);
        while (have_posts()) : the_post();
            set_query_var('cartItemKey', $quickViewProduct[1]);
            set_query_var('qvProduct', wc_get_product($quickViewProduct[0]));
            get_template_part('templates/popups/quick-view');
        endwhile;
        wp_reset_query();
    }
}
?>

<?php do_action('woocommerce_after_cart'); ?>

<?php $textDisabled    = gt_get_field('cart_attr_disabled_text', 'option'); ?>
<script>
    const oosText = ' - <?= $textDisabled ?>';

    // $(document).on('added_to_cart', function(e) {
    //     $('.sample-cart a.added').text('נוסף לעגלה');
    //     setTimeout(function() {
    //         $('.samples').removeClass('active');
    //     }, 1500)
    // });
</script>
