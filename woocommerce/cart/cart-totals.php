<?php

/**

 * Cart totals

 *

 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-totals.php.

 *

 * HOWEVER, on occasion WooCommerce will need to update template files and you

 * (the theme developer) will need to copy the new files to your theme to

 * maintain compatibility. We try to do this as little as possible, but it does

 * happen. When this occurs the version of the template file will be bumped and

 * the readme will list any important changes.

 *

 * @see 	    https://docs.woocommerce.com/document/template-structure/

 * @author 		WooThemes

 * @package 	WooCommerce/Templates

 * @version     2.3.6

 */



if ( ! defined( 'ABSPATH' ) ) {

	exit;

}



?>

<div class="cart_totals <?php echo ( WC()->customer->has_calculated_shipping() ) ? 'calculated_shipping' : ''; ?>">

    <div class="car"><img src="<?php echo img('car.png') ?>" />

        <?php

        $shippingFrom = gt_get_field('free_shipping_from', 'option');

        $subTotal = WC()->cart->get_subtotal();

        if (($shippingFrom - $subTotal) > 0) { ?>

            הוסיפי עוד ₪<?php echo ($shippingFrom - $subTotal); ?> למשלוח חינם!

        <?php } else { ?>

            איזה כיף! קיבלת משלוח חינם

        <?php } ?>

    </div>

	<?php do_action( 'woocommerce_before_cart_totals' ); ?>



    <h2 class="order-totals">סיכום ההזמנה שלך</h2>



	<table cellspacing="0" class="shop_table shop_table_responsive">

	    <tr class="order-next-stage">
                <th style="font-size: 16px;font-weight: 900 !important;">הזנת קופונים תתאפשר בשלב הבא</th>
            </tr>

		<tr class="cart-subtotal">

			<th><?php _e( 'Subtotal', 'woocommerce' ); ?>: <?php wc_cart_totals_subtotal_html(); ?>
			<?php 
		    $subtotal = WC()->cart->get_subtotal();
		    if ( $subtotal < 260 ) {
			echo '<p class="shipp">*עלות המשלוח תתווסף בשלב הבא לאחר בחירת שיטת המשלוח</p>';
			}
			?>
			</th>
		</tr>



        <tr class="shipping-method">

            <th>

                אפשרויות משלוח

                <?php

                $allRates = [];

                $packages = WC()->shipping()->get_packages();

                foreach ($packages as $package) {

                    //$package['rates']['flat_rate:1']->get_cost()

                    foreach ($package['rates'] as $key => $rate) {

                        $allRates[$key] = $rate;

                    }

                }



                foreach ($allRates as $rate) {

                ?>

                <div class="method">

                    <?php echo $rate->get_label(); ?> | <?php echo $rate->get_cost(); ?>

                </div>

                <?php

                }

                ?>

            </th>

        </tr>



		<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>

			<tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">

				<th><?php wc_cart_totals_coupon_label( $coupon ); ?>: <?php wc_cart_totals_coupon_html( $coupon ); ?></th>

			</tr>

		<?php endforeach; ?>





		<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>

			<tr class="fee">

				<th><?php echo esc_html( $fee->name ); ?></th>

				<td data-title="<?php echo esc_attr( $fee->name ); ?>"><?php wc_cart_totals_fee_html( $fee ); ?></td>

			</tr>

		<?php endforeach; ?>



		<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) :

			$taxable_address = WC()->customer->get_taxable_address();

			$estimated_text  = WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping()

					? sprintf( ' <small>' . __( '(estimated for %s)', 'woocommerce' ) . '</small>', WC()->countries->estimated_for_prefix( $taxable_address[0] ) . WC()->countries->countries[ $taxable_address[0] ] )

					: '';



			if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>

				<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>

					<tr class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">

						<th><?php echo esc_html( $tax->label ) . $estimated_text; ?></th>

						<td data-title="<?php echo esc_attr( $tax->label ); ?>"><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>

					</tr>

				<?php endforeach; ?>

			<?php else : ?>

				<tr class="tax-total">

					<th><?php echo esc_html( WC()->countries->tax_or_vat() ) . $estimated_text; ?>: <?php wc_cart_totals_taxes_total_html(); ?></th>

				</tr>

			<?php endif; ?>

		<?php endif; ?>



		<?php do_action( 'woocommerce_cart_totals_before_order_total' ); ?>

            


		<tr class="order-total" style="display: none;">

			<th>סך הכל לתשלום: <?php wc_cart_totals_order_total_html(); ?></th>

		</tr>



		<?php do_action( 'woocommerce_cart_totals_after_order_total' ); ?>



	</table>



	<div class="wc-proceed-to-checkout">

		<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>

	</div>



	<?php do_action( 'woocommerce_after_cart_totals' ); ?>



</div>

