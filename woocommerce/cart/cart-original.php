<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

wc_print_notices();

do_action('woocommerce_before_cart'); ?>

<div class="row cart-container">
    <div class="col-lg-7">
        <div class="samples">הכיף ממשיך! עוד מוצרים במחירים שווים<span>+</span></div>
        <div class="product-samples">
        <?php $query = new WP_Query( array(
            'post_type' => 'product',
            'posts_per_page' => 3,
            'order_by' => 'date',
            'product_cat' => 'איפור',
            ));
            
            while ( $query->have_posts() ) {
			$query->the_post();
            ?>
                <div class="col-sm-4">
                    <?php
                        global $product;
                        GT::renderProduct($product, false, 'sample');
                    ?>
                </div>
            <?php }
        wp_reset_postdata();
        ?>
            <div class="clearfix"></div>
        </div>
        <form class="woocommerce-cart-form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
            <?php do_action('woocommerce_before_cart_table'); ?>

            <div class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
                <?php do_action('woocommerce_before_cart_contents'); ?>

                <?php
                $quickViewProductIds = [];
                foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                    $_product   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                    $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
                    $prod_category = get_the_terms($cart_item['product_id'], 'product_cat')[0] -> name;
                    if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                        $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item,
                            $cart_item_key);
                        ?>

                        <div class="woocommerce-cart-form__cart-item <?php echo esc_attr(apply_filters('woocommerce_cart_item_class', 'cart_item', $cart_item,
                            $cart_item_key)); ?>">

                            <div class="product-remove">
                                <?php
                                // @codingStandardsIgnoreLine
                                echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
                                    '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><img src="' . img('del.png') . '" /></a>',
                                    esc_url(wc_get_cart_remove_url($cart_item_key)),
                                    __('Remove this item', 'woocommerce'),
                                    esc_attr($product_id),
                                    esc_attr($_product->get_sku())
                                ), $cart_item_key);
                                ?>
                            </div>

                            <div class="product-thumbnail">
                                <?php
                                $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

                                if (!$product_permalink) {
                                    echo wp_kses_post($thumbnail);
                                } else {
                                    printf('<a href="%s">%s</a>', esc_url($product_permalink), wp_kses_post($thumbnail));
                                }
                                ?>
                            </div>

                            <div class="cart-item-info">
                                <div class="product-name" data-title="<?php esc_attr_e('Product', 'woocommerce'); ?>">
                                    <?php
                                    if (!$product_permalink) {
                                        echo wp_kses_post(apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;');
                                    } else {
                                        echo wp_kses_post(apply_filters('woocommerce_cart_item_name',
                                            sprintf('<a href="%s">%s</a>', esc_url($product_permalink), $_product->get_title()), $cart_item, $cart_item_key));
                                        echo ' | ' . $prod_category;
                                    }

                                    do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);

                                    // Meta data.
                                    echo wc_get_formatted_cart_item_data($cart_item); // PHPCS: XSS ok.

                                    // Backorder notification.
                                    if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                                        echo wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification',
                                            '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>'));
                                    }
                                    ?>
                                </div>
                                <div class="product-price" data-title="<?php esc_attr_e('Price', 'woocommerce'); ?>">
                                    <?php
                                    echo apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); // PHPCS: XSS ok.
                                    ?>
                                </div>
                                <div class="product-quantity" data-title="<?php esc_attr_e('Quantity', 'woocommerce'); ?>">
                                    <?php
                                    if ($_product->is_sold_individually()) {
                                        $product_quantity = sprintf('1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key);
                                    } else {
                                        $product_quantity = woocommerce_quantity_input([
                                            'input_name'   => "cart[{$cart_item_key}][qty]",
                                            'input_value'  => $cart_item['quantity'],
                                            'max_value'    => $_product->get_max_purchase_quantity(),
                                            'min_value'    => '0',
                                            'product_name' => $_product->get_name(),
                                        ], $_product, false);
                                    }
                                    echo '<div class="q_wrapper">';
                                    echo sprintf('<a class="q_arrow_up %s">+</a>', $cart_item['quantity'] == $_product->get_max_purchase_quantity() ? 'disabled' : '');
                                    echo sprintf('<span>%s</span>', $cart_item['quantity']);
                                    echo sprintf('<a class="q_arrow_down %s">-</a>', $cart_item['quantity'] == 0 ? 'disabled' : '');
                                    echo sprintf('<input class="quantityHidden" type="hidden" name="cart[%s][qty]" value="%s" />', $cart_item_key, $cart_item['quantity']);
                                    echo '</div>';
                                    //echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.

                                    foreach ($_product->get_attributes() as $attribute => $attributeName) {
                                        $attr  = urldecode($attribute);
                                        $terms = wc_get_product_terms($_product->get_parent_id(), $attr, ['fields' => 'all']);
                                        foreach ($terms as $term) {
                                            $realName = str_replace('-', ' ', urldecode($attributeName));
                                            if ($term->name === $realName) {
                                                $quickViewProductIds[] = [$_product->get_parent_id(),$cart_item_key];
                                                $attachment_id = absint(get_term_meta($term->term_id, 'product_attribute_image', true));
                                                $image_size    = woo_variation_swatches()->get_option('attribute_image_size');
                                                $image_url     = wp_get_attachment_image_url($attachment_id, apply_filters('wvs_product_attribute_image_size', $image_size));
                                                $realAttr      = str_replace('pa_', '', $attr);
                                                echo "<img class='attribute-img' src='$image_url' />";
                                                echo "<span class='attribute-name'>$realAttr | $realName</span>";
                                                echo "<a href='#' class='attribute-edit-link' 
                                                onclick=\"openQuickView('". $cart_item_key ."');\">עריכה</a>";
                                            }
                                        }
                                    }
                                    ?>


                                </div>
                                <div class="product-subtotal" data-title="<?php esc_attr_e('Total', 'woocommerce'); ?>">
                                    סה״כ |
                                    <?php
                                    echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item,
                                        $cart_item_key); // PHPCS: XSS ok.
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>

                <?php do_action('woocommerce_cart_contents'); ?>

                <div style="margin-top: 40px;">
                    <div colspan="6" class="actions">

                        <button type="submit" class="button" name="update_cart" style="display:none;"
                                value="<?php esc_attr_e('Update cart', 'woocommerce'); ?>"><?php esc_html_e('Update cart', 'woocommerce'); ?></button>

                        <?php do_action('woocommerce_cart_actions'); ?>

                        <?php wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce'); ?>
                    </div>
                </div>

                <?php do_action('woocommerce_after_cart_contents'); ?>
            </div>
            <?php do_action('woocommerce_after_cart_table'); ?>
        </form>
    </div>
    <div class="col-lg-4 col-lg-offset-1">
        <div class="cart-collaterals">
            <?php
            /**
             * Cart collaterals hook.
             *
             * @hooked woocommerce_cross_sell_display
             * @hooked woocommerce_cart_totals - 10
             */
            do_action('woocommerce_cart_collaterals');
            ?>
        </div>

        <?php
        $field = gt_get_field('cart_page', 'option');
        if ($field['banner_image']) {
            ?>
            <div class="cart-banner">
                <img src="<?php echo $field['banner_image']; ?>"/>
            </div>
        <?php } ?>
    </div>
</div>


<div class="container best-price-list">
    <?php
        $posts = gt_get_field('best_price', 'option');
        $products = [];
        foreach ($posts as $post) {
            $products[] = wc_get_product($post->ID);
        }

        GT::renderCrossSellProducts('MUST HAVE', 'במיוחד בשבילך', $products, 'sample');
    ?>
</div>

<?php
foreach ($quickViewProductIds as $quickViewProduct) {
    query_posts(['p' => $quickViewProduct[0], 'post_type' => 'product']);
    while (have_posts()) : the_post();
        set_query_var('cartItemKey', $quickViewProduct[1]);
        get_template_part('templates/popups/quick-view');
    endwhile;
    wp_reset_query();
}

?>

<?php do_action('woocommerce_after_cart'); ?>

<script type="text/javascript">
    jQuery(document).ready(function () {
        let $ = jQuery;

        bindQuantityButtons();
        $(document).on('wc_fragments_refreshed', bindQuantityButtons);

        let cartButton = $('button[name=update_cart]');
        cartButton.hide();
    });

    function bindQuantityButtons() {
        $('.q_arrow_up').bind('click', function () {
            let closestQuantity = $(this).parent().find('input.quantityHidden');
            let newValue = parseInt(closestQuantity.val()) + 1;
            changeQuantity(newValue, closestQuantity);
        });

        $('.q_arrow_down').bind('click', function () {
            let closestQuantity = $(this).parent().find('input.quantityHidden');
            let newValue = parseInt(closestQuantity.val()) - 1;
            if (newValue > 0) {
                changeQuantity(newValue, closestQuantity);
            }
        });
    }

    function changeQuantity(newQuantity, closestQuantity) {
        let $ = jQuery;
        closestQuantity.val(newQuantity);
        $('button[name=update_cart]').removeAttr('disabled');
        $('button[name=update_cart]').trigger('click');
    }
    jQuery(document).ready(function () {
        let $ = jQuery;
        $(".samples").click(function(){
           $(".product-samples").slideToggle("fast");
            $(this).toggleClass("active"); return false;
        });
    });
</script>
