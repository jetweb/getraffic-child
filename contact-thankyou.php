<?php
/**
 * Template Name: Contact Us thankyou
 *  *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>
<div class="container contactus">
<?php get_template_part('templates/contact/form'); ?>
</div>
<div style="clear:both"></div>
<?php get_footer(); ?>
