<?php
/**
 * Template Name: Search Results Index
 *
 * The template for displaying pages without the sidebar
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
global $wp_query;
get_header(); ?>
<div class="container" id="primary">
    <div class="search-result">
        <div class="search-result-wrap">
            <div class="search-result-input">
                <input type="text" name="searchItem" id="seacrhResult" value="" class="search-value" title="search" placeholder="חיפוש"/>
            </div>
        </div>
    </div>
    <?php
    if (!$wp_query->found_posts) {
        ?>
        <div class="container nothing-found">
            <div class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 dir="rtl">לא מצאנו את הביטוי ״<?php the_search_query() ?>״</h2>
                    </div>
                </div>
            </div>
        </div>
        <?php
        //TODO: remove! use `renderCrossSellProducts` instead!
        $bestSellers = gt_get_field('homepage_best_sellers', 25); ?>
        <div class="container best-sellers-container">
            <h1 class="nothing-found-text">...maybe you’re looking for</h1>
            <?php
            $products = [];
            foreach ($bestSellers['best_sellers'] as $post) {
                $products[] = wc_get_product($post->ID);
            }
            GT::renderCrossSellProducts(
                '', '', $products
            );
            ?>
        </div>
        <?php get_template_part('templates/contact/address'); ?>
    <?php } else {
        ?>
        <div class="search-word"><?php echo $_REQUEST['s']; ?></div>
        <p class="results-found"> נמצאו <?= $wp_query->found_posts; ?> תוצאות חיפוש</p>
        <div class="products">
            <?php
            if (have_posts()) {
                while (have_posts()) : the_post();
                    if (isset($product) && $product->exists()) {
                        GT::renderProduct($product);
                    } else {
                        ?>
                        <div class="product-wrapper">
                            <div class="thumb-wrapper">
                                <a href="<?php the_permalink(); ?>">
                                    <img src="<?php the_post_thumbnail_url(); ?>" alt="" class="thumb">
                                </a>
                            </div>
                            <div class="product-info">
                                <a href="<?php the_permalink(); ?>"><h1 class="title"><?php the_title(); ?></h1></a>
                                <h2 class="description"><?php echo wp_trim_words(get_the_excerpt(), 10); ?></h2>
                            </div>
                        </div>
                    <?php } endwhile;
            } ?>
        </div>
    <?php } ?>
</div>

<?php get_footer(); ?>
