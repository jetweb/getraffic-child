<?php
/**
 * Template Name: Checkout Page
 *
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */

if (strpos($_SERVER['REQUEST_URI'], 'order-received') !== false) {
    get_template_part('templates/checkout/thank-you');
    exit;
} ?>
<div class="checkout">
<?php get_template_part('templates/header/head');
wp_admin_bar_render();

?>
<div class="only-mobile"><?php get_template_part('templates/header/topbar/minicart');?></div>
<div class="only-mobile"><?php get_template_part('templates/header/menu/mobile');?></div>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . '/assets/css/checkout.css'; ?>">
<div class="woocommerce">
<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">
    <div class="container-fluid">
        <div class="row full-height">
            <div class="col-md-7 col-md-offset-1 checkout-wizard-container">
                <?php if (have_posts()) {
                    while (have_posts()) : the_post();
                        the_content(); endwhile;
                } ?>
            </div>
            <div class="col-md-4 checkout-cart-container">
                    <?php do_action('woocommerce_checkout_order_review'); ?>
            </div>
        </div>
    </div>
</form>
</div>



<form name="checkout_redeeming" class="checkout_redeeming hidden" method="post">
    <?php //RSFunctionForCart::reward_system_add_message_after_cart_table(); ?>
</form>
    <style>
        tr.order-total th.ship {
            visibility: hidden;
        }
    </style>
</div>
<?php get_template_part('templates/footer/ajax-url'); ?>
<script>
    jQuery(document).ready(function($) {

        let firstLoad = true;
        $( document.body ).on( 'updated_checkout', function(a, b, c, d) {
            console.log('updated_checkout', firstLoad, a, b, c, d);
            if (firstLoad) {
                if (!$('ul#shipping_method li').first().find('label input').prop('checked')) {
                    $('ul#shipping_method li').first().find('label input').prop('checked', 'checked');
                    $( document.body ).trigger( 'update_checkout', { update_shipping_method: true } );
                } else {
                    $('ul#shipping_method li').first().find('label input').prop('checked', null);
                    $('.checkout-shipping-fields').hide();
                }
                firstLoad = false;
            } else {
                $('tr.order-total th.ship').css("visibility", "visible");
            }
            $('ul#shipping_method').prepend($('ul#shipping_method li label[for=shipping_method_0_local_pickup_plus]').parent());

            // console.log('firstload', firstLoad);
            // if (firstLoad) {
            //     $('select.pickup_location').val(0).trigger('change');
            //     $('ul#shipping_method li').first().find('label input').trigger('click');
            //     setTimeout(function() {
            //         $('ul#shipping_method li').first().find('label input').trigger('click');
            //     }, 1000);
            // }
        });

        $(document).on('showing_checkout_step', function (e, a) {
            console.log('showing_checkout_step', e, a);
            if (a === 'delivery') {
                $('tr.order-total th.ship').css("visibility", "visible");
                firstLoad = false;
                if (typeof __ZA !== 'undefined') { __ZA.simulateNewPage(true);}
            }

            if (a === 'coupons') {
                $([document.documentElement, document.body]).animate({
                    scrollTop: 60
                }, 400);
            }
        });
    });
</script>

<?php
if (strpos($_SERVER['REQUEST_URI'], 'order-pay') === false) {
    \Getraffic\GTM::pushCheckoutStep(1);
}
?>

<?php wp_footer(); ?>

