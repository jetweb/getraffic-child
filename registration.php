<?php
/**
 * Template Name: עמוד הצטרפות למודעון
 *
 * The home page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>
<div class="register-page" id="registerPage">
    <div class="register-top">
        <?php get_template_part('templates/login/registration'); ?>
    </div>
    
</div>
<?php get_template_part('templates/contact/address'); ?>
<div style="clear:both"></div>

<?php get_footer(); ?>

<script>
    jQuery(document).ready(function($) {
        $('.register-top .register-button').bind('click', function() {
            let valid = true;
            //debug
            $('.register-top input.validate').each(function () {
                if ($(this).val() == '') {
                    valid = false;
                    $(this).next().css('color', '#ff0000').html($(this).data('empty-message'));
                    $(this).css('border', '1px solid #ff0000');
                }
                else if ($(this).parent().hasClass('popup-phone') && !validatePhone($(this).val())) {
                    valid = false;
                    $(this).next().css('color', '#ff0000').html('יש להזין 10 ספרות ללא מקף');
                    $(this).css('border', '1px solid #ff0000');
                }
                else if ($(this).hasClass('popup-mail') && !validateEmail($(this).val())) {
                    valid = false;
                    $(this).next().css('color', '#ff0000').html('כתובת המייל אינה תקינה');
                    $(this).css('border', '1px solid #ff0000');
                }
                else if ($(this).hasClass('validate-only-letters') && hasNumberOrSpecialChar($(this).val())) {
                    valid = false;
                    $(this).next().css('color', '#ff0000').html($(this).data('error-letters'));
                    $(this).css('border', '1px solid #ff0000');
                }
                else if ($(this).hasClass('validate-password') && $(this).val().length < 6) {
                    valid = false;
                    $(this).next().css('color', '#ff0000').html('הסיסמה צריכה להיות מורכבת מ-6 תווים לפחות');
                    $(this).css('border', '1px solid #ff0000');
                }
                else{
                    $(this).next().html('');
                    $(this).css('border', '1px solid black');
                }

            });


            if (!$('.register-top  #accept_terms').prop('checked')) {
                valid = false;
                $('.error_terms').css('color', '#ff0000').html('יש לאשר את תנאי השימוש באתר');
            }else{
                $('.error_terms').html('');
            }

            debugger;
            var password = $('.register-top .new_password').find('.validate').val();
            var confirm_password = $('.register-top .confirm_new_password').find('.validate').val();

            if (password && confirm_password && password !== confirm_password) {
                valid = false;
                $('.error_confirm_password').html('הסיסמה לא תואמת לסיסמה בשדה הקודם, בבקשה נסי שוב');
            }

            if (!valid) {
                return false;
            } else {
                $.ajax({
                    type: 'post',
                    url: ajax_url,
                    data: $('form#registrationForm').serialize(),
                    beforeSend: function() {

                    },
                    complete: function(response) {

                    },
                    failure: function(response) {

                    },
                    success: function(response) {
                        response = JSON.parse(response);
                        if (response.error) {
                            $('p.validation').html(response.error);
                        } else if (response.redirect) {
                            location.href = response.redirect;
                        }
                    },
                });
            }

            return false;
        });
    });


    $('.form-row-eye img').click(function() {

        if ($(this).prevAll(".form-row-eye input.validate").attr('type') == 'text') {
            $(this).prevAll(".form-row-eye input.validate").attr('type', 'password');
            $(this).css('display', 'none');
            $(this).prev().css('display', 'block');
        } else {
            $(this).prevAll(".form-row-eye input.validate").attr('type', 'text');
            $(this).css('display', 'none');
            $(this).next().css('display', 'block');

        }
    });


    $(".popup-mail").focusin(function() {
        $('.tooltip-mail').show();
    });
    $(".popup-mail").focusout(function() {
        $('.tooltip-mail').hide();
    });
    $(".popup-phone").focusin(function() {
        $('.tooltip-phone').show();
    });
    $(".popup-phone").focusout(function() {
        $('.tooltip-phone').hide();
    });

</script>
