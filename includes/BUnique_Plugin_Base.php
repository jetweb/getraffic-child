<?php

require_once(get_template_directory() . '/includes/PluginBase.php');
require_once 'BUnique_Scripts_To_Footer.php';

use Getraffic\PluginBase;
use Getraffic\CustomPostType;
use Getraffic\CustomMetaField;
use Getraffic\CustomMetaBox;
use Getraffic\CustomTaxonomy;
use Getraffic\Smoove;

/**
 * Class TonyMoly
 */
class BUnique_Plugin_Base extends PluginBase
{
    public function __construct()
    {
        parent::__construct();

        $this->registerMenus();
        $this->registerFrontendCall('join-newsletter', 'joinNewsletter');
        $this->registerFrontendCall('returnItems', 'returnItems');
        $this->registerFrontendCall('replaceItems', 'replaceItems');
        $this->registerFrontendCall('check_sku', 'check_sku');
    }

    /**
     *
     */
		 public static function check_sku() {
	$args = array(
		'post_type' => 'product',
	   'meta_query' => array(
		   array(
			   'key' => '_sku',
			   'value' => $_REQUEST['sku'],
			   'compare' => '=',
		   )
	   )
	);
	$query = new WP_Query($args);
	if ($query->found_posts) {
	$posts = $query->posts;
	$return = array(
    'type' => 'success',
    'url'      => get_site_url() .'/shop/?s='. urlencode(stristr($posts[0]->post_title,':',true))
	);
	wp_send_json_success( $return );
	}
	$return = array(
    'type' => 'error',
	);

	wp_send_json_error( $return );

}


    public static function returnItems() {
        $orderItems = [];
        foreach ($_POST['orderItems'] as $item_id) {
            $orderItems[] = new WC_Order_Item_Product($item_id);
        }

        $order = new WC_Order($orderItems[0]->get_order_id());
        $subject = 'בקשה להחזרת מוצרים בהזמנה מס. ' . $order->get_order_number();

        ob_start();
        set_query_var('order', $order);
        set_query_var('orderItems', $orderItems);
        set_query_var('emailSubject', $subject);
        get_template_part('templates/emails/request-return');
        $html = ob_get_contents();
        ob_end_clean();
        $headers = ['Content-Type: text/html; charset=UTF-8'];
        $wcMail = new \WC_Email();
        $message = apply_filters('woocommerce_mail_content', $wcMail->style_inline($html));
        wp_mail('info@bunique.co.il, yaniv@getraffic.co.il', $subject, $message, $headers);

        echo 'success';
        exit;
    }

    public static function replaceItems() {
        $orderItems = [];
        foreach ($_POST['orderItems'] as $item_id) {
            $orderItems[] = new WC_Order_Item_Product($item_id);
        }

        $order = new WC_Order($orderItems[0]->get_order_id());
        $subject = 'בקשה להחלפת מוצרים בהזמנה מס. ' . $order->get_order_number();

        ob_start();
        set_query_var('order', $order);
        set_query_var('orderItems', $orderItems);
        set_query_var('emailSubject', $subject);
        get_template_part('templates/emails/request-replace');
        $html = ob_get_contents();
        ob_end_clean();
        $headers = ['Content-Type: text/html; charset=UTF-8'];
        $wcMail = new \WC_Email();
        $message = apply_filters('woocommerce_mail_content', $wcMail->style_inline($html));
        wp_mail('info@bunique.co.il, yaniv@getraffic.co.il', $subject, $message, $headers);

        echo 'success';
        exit;
    }


    public static function joinNewsletter() {
        echo (new \Getraffic\MailChimp())->subscribe($_REQUEST['email']);
        exit;
    }

    /** Navigation */
    function registerMenus()
    {
        register_nav_menu('mobile-menu-bottom', __('Mobile Menu Bottom'));
    }

    function getTextDomain()
    {
        // TODO: Implement getTextDomain() method.
    }

    /**
     * Has to be an associative array with the key as the name of the post type!
     *
     * @return array
     */
    function getCustomPostTypes()
    {
        return [];
    }
}


