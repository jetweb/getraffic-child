<?php
final class BUnique_Scripts_To_Footer {

    /**
     * An array of script slugs that should remain in the header.
     *
     * @var array
     */
    protected $header_scripts;

    // Hold the class instance.
    private static $instance = null;

    /**
     * Construct.
     *
     * Registers our activation hook and init hook.
     */
    protected function __construct() {

        add_action( 'admin_init', array( $this, 'check_version' ) );

        // Don't run anything else in the plugin, if we're on an incompatible
        if ( ! self::compatible_version() || is_admin() ) {
            return;
        }

        // Make it so
        add_action( 'init', array( $this, 'init' ) );

    }

    // The object is created from within the class itself
    // only if the class has no instance.
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }


    /**
     * The backup sanity check.
     *
     * This is just in case the plugin is activated in a weird way,
     * or the versions change after activation.
     *
     * @since 0.6.0
     */
    function check_version() {}

    /**
     * Display notice on deactivation.
     *
     * @since 0.6.0
     */
    function disabled_notice() {

        echo '<strong>Requires WordPress 3.1.0 or higher.</strong>';

    }

    /**
     * Check everything is compatible.
     *
     * @since 0.6.0
     *
     * @return boolean
     */
    static function compatible_version() {

        if ( version_compare( $GLOBALS['wp_version'], '3.1.0', '<' ) ) {
            return false;
        }

        // Add sanity checks for other version requirements here

        return true;

    }

    /**
     * Plugin Init.
     *
     * Makes some checks and runs the filter, sets up the admin settings page
     * and plugin links.
     *
     * @since 0.2.0
     * @since 0.6.3 moved 'set_header_scripts' into 'wp_head' action.
     */
    function init() {

        // Run the plugin
        add_action( 'wp_enqueue_scripts', array( $this, 'clean_head' ) );
        add_filter( 'stf_include', array( $this, 'stf_includes' ) );

        // Set the header scripts to be forced into the header
        add_action( 'wp_head', array( $this, 'set_header_scripts' ), 1 );

        // Add select scripts into the header
        add_action( 'wp_head', array( $this, 'print_head_scripts' ), 10 );

    }

    /**
     * Set the scripts to be printed in the header, based on options and filter.
     *
     * @since 0.6.0
     */
    public function set_header_scripts() {

        $this->header_scripts = apply_filters( 'stf_exclude_scripts', array( 'jquery' ) );

    }

    /**
     * The holy grail: print select scripts in the header!
     *
     * @since 0.6.0
     */
    function print_head_scripts() {

        if( ! isset( $this->header_scripts ) || empty( $this->header_scripts ) || ! is_array( $this->header_scripts ) )
            return;

        // The main filter, true inacts the plugin, false does not (excludes the page)
        if( $this->is_included() ) {
            foreach( $this->header_scripts as $script ) {
                if( ! is_string( $script ) )
                    continue;

                // If the script is enqueued for the page, print it
                if( wp_script_is( $script ) )
                    wp_print_scripts( $script );
            }
        }

    }

    /**
     * Remove scripts from header, forces them to the footer.
     *
     * Checks the singular post/page first, then other common pages
     * and compares against any global settings and filters.
     *
     * @since 0.1.0
     **/
    function clean_head() {

        // Bail if we're in the admin area
        if( is_admin() )
            return;

        // The main filter, true inacts the plugin, false does not (excludes the page)
        $include = $this->is_included();

        // If this isn't set, then we're missing something
        if( ! isset( $include ) ) {
            $this->log_me( 'Something went wrong with the $include variable' );
            return;
        }

        // Either it's turned off site wide and included for this post/page, or turned on site wide and not excluded for this post/page - also not admin
        if( true === $include ) {
            remove_action( 'wp_head', 'wp_print_scripts' );
            remove_action( 'wp_head', 'wp_print_head_scripts', 9 );
            remove_action( 'wp_head', 'wp_enqueue_scripts', 1 );
        }

    }

    /**
     * Determing if the current page is included, via a filter.
     *
     * @since 0.6.0
     *
     * @return boolean Default is true.
     */
    public function is_included() {

        $include = apply_filters( 'stf_include', true );

        if( true === $include ) {
            return true;
        } elseif( false === $include ) {
            return false;
        } else {
            $this->log_me( 'Non-boolean value in the stf_include filter' );
            return true;
        }

    }

    /**
     * Runs the various template checks and returns true/false.
     *
     * @since 0.6.0
     *
     * @return boolean
     */
    function stf_includes() {

        // Collect the information
        if( is_singular() || is_page() ) {

            // Make sure we can grab the page/post id
            $queried_object_id = get_queried_object_id();

            // verify we got a good result
            if( absint( $queried_object_id ) && ( $post_type = get_post_type( $queried_object_id ) ) ) {

                // See if post type is supported, if not bail
                if( false === $this->post_type_supported( $post_type ) )
                    return false;

                // Get the exclude post meta value
                $exclude_page = get_post_meta( $queried_object_id, 'stf_exclude', true );

                // Support for older versions that use 'on' as well as newer versions with boolean
                if( 'on' === $exclude_page || true == $exclude_page )
                    return false;

                // Older override check, depreciated
                $excluded_override = apply_filters( 'scripts_to_footer_exclude_page', null, $queried_object_id );
                if( 'on' == $excluded_override || true == $excluded_override ) {
                    $this->log_me( 'The \'scripts_to_footer_exclude_page\' is depreciated, please use \'stf_{$post_type}\' returning false to exclude the page.');
                    return false;
                }

                // Allow override
                return apply_filters( "stf_{$post_type}", true, $queried_object_id );

            } else {
                return false;
            }

            // Home (blog) page
        } elseif( is_home() ) {

            // Grab global setting
            $type = 'home';

            // Search Result Page
        } elseif( is_search() ) {

            $type = 'search';

            // 404 Pages
        } elseif( is_404() ) {

            $type = '404';

            // Author Archive
        } elseif( is_author() ) {

            $type = 'author_archive';

            // Category Archive
        } elseif( is_category() ) {

            if( $this->tax_supported( 'category' ) ) {
                $type = "category_archive";
            } else {
                return false;
            }

            // Tag Archive
        } elseif( is_tag() ) {

            if( $this->tax_supported( 'post_tag' ) ) {
                $type = "post_tag_archive";
            } else {
                return false;
            }

            // Taxonomy Archive
        } elseif( is_tax() ) {

            $taxonomy = get_query_var( 'taxonomy' );
            if( !$taxonomy ) {
                return false;
            }
            $tax = get_taxonomy( $taxonomy );
            if( isset( $tax->name ) && $this->tax_supported( $tax->name ) ) {
                $type = "{$tax->name}_archive";
            } else {
                return false;
            }

            // Post Type Archive
        } elseif( is_post_type_archive() ) {

            $post_type = get_post_type();
            if( $this->post_type_supported( $post_type ) ) {
                $type = "{$post_type}_archive";
            } else {
                return false;
            }

            // Generic archives (date, author, etc)
        } elseif( is_archive() ) {

            $type = 'archive';

            // if all else fails return false
        } else {

            return false;

        }

        // Get the option and return the result with a filter to override
        if( isset( $type ) && is_string( $type ) ) {

            // Filter to *exclude* a type of page, return the opposite
            /* $exclude = stf_get_option( "stf_exclude_{$type}", false );
             if( $exclude ) {
                 $include = false;
             } else {
                 $include = true;
             }

             return apply_filters( "stf_{$type}", $include );*/

            return false;

        } else {

            $this->log_me( 'invalid $type element' );
            return false;

        }

    }

    /**
     * Check for post type support, via the filter. Default support for page and post.
     *
     * @since 0.6.0
     *
     * @return array Supported posts
     */
    public function post_types_supported() {

        $post_types = apply_filters( 'scripts_to_footer_post_types', array( 'page', 'post' ) );
        if( is_array( $post_types ) ) {
            return $post_types;
        } else {
            return false;
        }

    }

    /**
     * Check if a post type is supported.
     *
     * @since 0.6.0
     *
     * @return array Supported posts
     */
    public function post_type_supported( $post_type ) {

        $post_types = $this->post_types_supported();
        if( is_array( $post_types ) && is_string( $post_type ) && in_array( $post_type, $post_types ) ) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Check for custom taxonomy support, via the filter.
     *
     * @since 0.6.0
     *
     * @return array Supported posts
     */
    public function taxonomies_supported() {

        $taxes = apply_filters( 'scripts_to_footer_taxonomies', array( 'category', 'post_tag' ) );
        if( is_array( $taxes ) ) {
            return $taxes;
        } else {
            return false;
        }

    }

    /**
     * Check if a post type is supported.
     *
     * @since 0.6.0
     *
     * @return array Supported posts
     */
    public function tax_supported( $taxonomy ) {

        $taxes = $this->taxonomies_supported();
        if( is_array( $taxes ) && is_string( $taxonomy ) && in_array( $taxonomy, $taxes ) ) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Log any errors, if debug mode is on.
     *
     * @since 0.6.0
     *
     * @param string $message
     */
    public function log_me( $message ) {

        if ( $this->debug() ) {
            if ( is_array( $message ) || is_object( $message ) ) {
                error_log( 'Scripts-to-Footer Plugin Error: ' . print_r( $message, true ) );
            } else {
                error_log( 'Scripts-to-Footer Plugin Error: ' . $message );
            }
        }

    }

    /**
     * Check to see if we're in a debug mode.
     *
     * @since 0.6.5
     *
     * @return bool
     */
    private function debug() {

        return defined( 'WP_DEBUG' ) && true === WP_DEBUG
            && defined( 'STF_DEBUG' ) && true === STF_DEBUG;

    }
}