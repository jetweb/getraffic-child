<?php

/**

 * Template Name: Stores

 *  *

 * This is the template that displays all pages by default.

 * Please note that this is the WordPress construct of pages and that

 * other "pages" on your WordPress site will use a different template.

 *

 * @package    WordPress

 * @subpackage Blank

 */

get_header();

?>

<div class="container stores">

    <div class="content">

        <div class="row">

            <div class="stores-right">

                <?php $stores = gt_get_field('store'); ?>

                <div class="ourstory-content">

                    <div class="search-store">

                        <div class="search-store-wrap">

                            <div class="search-store-input">

                                <input type="text" name="searchItem" id="seacrhStore" value="" class="search-store" title="search" placeholder="חיפוש סניף"/>

                            </div>

                        </div>

                    </div>

                    <p class="stores-title">חפשי לפי שם הקניון (למשל: שבעת הכוכבים) או לפי שם היישוב בו נמצא הסניף</p>

                    <p class="stores-nothingFound">לשם עוד לא הגענו, אולי בעתיד :) <br>כדאי לוודא שלא נפלו במקרה טעויות בהקלדה, <br>או לחפש לפי יישוב אחר </p>

                    <?php

                    foreach ($stores as $store) {

                        if (!empty($store)) {

                            ?>

                            <div class="store">

                                

                                <div class="store-title"><?php echo $store['city']; ?><?= (!empty($store['place'])) ? ' | ' . $store['place'] : ''; ?></div>

                                <div class="store-text"><?php echo $store['address']; ?></div>

                                <div class="store-text desktop">טלפון <?php echo $store['phone']; ?></div>

                                <div class="store-text mobile-only">טלפון <a href="tel:<?php echo $store['phone']; ?>"><?php echo $store['phone']; ?></a></div>


                                <div class="opening-hours">שעות פתיחה</div>

                                <div class="store-text">א׳-ה׳ <?php echo $store['hours']; ?></div>

                                <div class="store-text">ו׳ <?php echo $store['friday_hours']; ?></div>

                                <div class="store-text"><?php echo $store['information']; ?></div>

                                <div class="store-text"><?php echo $store['comment']; ?></div>

                            </div>

                            <?php

                        }

                    }

                    ?>

                </div>

            </div>

            <div class="stores-left">

                <div class="stores-left-title">

                    <?php $left_stores = gt_get_field('store_left_side'); ?>

                    <?php //print_r($left_stores); ?>

                    <h1><?php echo $left_stores['title']; ?></h1>

                    <p><?php echo $left_stores['text']; ?></p>

                </div>

            </div>

        </div>

    </div>

</div>

<?php get_footer(); ?>

<script type="text/javascript">

    jQuery(document).ready(function () {

        let $ = jQuery;

        $('input.search-store').bind('change keyup keydown', function () {

            let searchVal = $(this).val();

            $('.store').each(function () {

                if ($(this).text().indexOf(searchVal) !== -1) {

                    $(this).show();

                } else {

                    $(this).hide();

                }

            });

        });

    });

</script>



<style>

    .stores-left {

        background: url(<?php echo $left_stores['image']; ?>);

        background-repeat: no-repeat;

        background-size: 100%;

    }

    .stores-nothingFound{

        display: none;

    }

</style>