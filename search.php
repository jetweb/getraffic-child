<?php
/**
 * Template Name: Search Results Index
 *
 * The template for displaying pages without the sidebar
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
global $wp_query;
$search_wp_query = new WP_Query( "s=$s&post_type='product'" );
get_header(); ?>
<div class="container" id="primary">
    <?php
    woocommerce_catalog_ordering();
    if ($search_wp_query->found_posts) { ?>
        <p class="results-found"> נמצאו <?= $search_wp_query->found_posts; ?> תוצאות חיפוש</p>
    <?php } ?>
    <form class="search-form" action="/">
    <div class="search-result">
        <div class="search-result-wrap">
            <div class="search-result-input">
                <input type="text" name="s" id="searchResult" value="<?= $s ?>" class="search-value" title="search" placeholder="חיפוש"/>
            </div>
        </div>
    </div>
    </form>
    <?php
    
    if ($search_wp_query->found_posts) { ?>
        <div class="products">
        <?php if (have_posts()) {
            while (have_posts()) : the_post();                    
                if (isset($product) && $product->exists()) {
                    GT::renderProduct($product);
                }
            endwhile;
        } ?>
    </div> 
    <?php } else {
    ?>
        <div class="container nothing-found">
            <div class="content">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 dir="rtl">לא מצאנו את הביטוי ״<?php the_search_query() ?>״</h2>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $bestSellers = gt_get_field('homepage_best_sellers', 25); ?>
        <div class="container best-sellers-container">
            <h1 class="nothing-found-text">אולי אחת מהתוצאות האלה תוכל להתאים?</h1>
            <?php
            $products = [];
            foreach ($bestSellers['best_sellers'] as $post) {
                $products[] = wc_get_product($post->ID);
            }
            GT::renderCrossSellProducts(
                '', '', $products
            );
            ?>
        </div>
        <?php get_template_part('templates/contact/address'); ?>
   <?php } ?> 
</div>

<?php get_footer(); ?>
<style>
    .search-popup-wrap {
        display: none !important;
    }
</style>